# FROM --platform=linux/arm64 node:16-alpine
FROM --platform=linux node:18
WORKDIR /app
COPY package*.json ./
COPY .env .env
RUN mkdir -p /usr/local/sap
# RUN apt-get update && \
#     apt-get install -y unzip
COPY libs/nwrfcsdk /usr/local/sap/nwrfcsdk
# COPY libs/nwrfc750P_12-70002752.zip /usr/local/sap
# RUN unzip /usr/local/sap/nwrfc750P_12-70002752.zip
ENV SAPNWRFC_HOME /usr/local/sap/nwrfcsdk
ENV SAPNWRFC_LIB_DIR $SAPNWRFC_HOME/lib
ENV LD_LIBRARY_PATH $SAPNWRFC_LIB_DIR:$LD_LIBRARY_PATH
RUN mkdir -p /etc/ld.so.conf.d
RUN echo '/usr/local/sap/nwrfcsdk/lib' > /etc/ld.so.conf.d/nwrfcsdk.conf
RUN ldconfig && ldconfig -p | grep sap
RUN npm install
copy . .
RUN npm run build
expose 3300
CMD ["node", "dist/main"]