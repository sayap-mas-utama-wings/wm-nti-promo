echo  "masukkan nama branch (default branch master jika kosong) :"

read branch

if [ -z  "$branch" ]
then
  git checkout master
  git pull origin master
else
  git checkout $branch
  git pull origin $branch
fi

echo "ingin jalankan npm install ? (ya/tidak) :"

read npmInstall
var1="ya"

if [ "$npmInstall"="$var1" ]
then
  npm install
fi

npm run build
tar -czvf wm-nti-promo.tar.gz dist node_modules
rm -r /var/www/html/wm-nti-promo/node_modules
rm -r /var/www/html/wm-nti-promo/dist
rm /var/www/html/wm-nti-promo/.env
mv ./wm-nti-promo.tar.gz /var/www/html/wm-nti-promo/
cp ./.env /var/www/html/wm-nti-promo/
cd /var/www/html/wm-nti-promo/
tar -xzvf /var/www/html/wm-nti-promo/wm-nti-promo.tar.gz
rm /var/www/html/wm-nti-promo/wm-nti-promo.tar.gz
pm2 stop wm-nti-promo
pm2 start wm-nti-promo