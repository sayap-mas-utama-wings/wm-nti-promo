import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response2<T> {
  data: T;
}

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response2<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response2<T>> {
    // const ctx = context.switchToHttp().getResponse();
    // console.log('cek ctx', ctx.req.res.req);

    const result = next.handle().pipe(
      map((data) => {
        // console.log('response', data);
        if (data) {
          let message: string;
          if (data?.message) {
            message = data?.message;
            delete data['message'];
          } else {
            message = '';
          }

          if (data?.status == 'error') {
            context.switchToHttp().getResponse().status(400);
          }

          return {
            message,
            data,
          };
        }
      }),
      // catchError((err) => throwError(() => new BadGatewayException())),
      // tap((data) => {
      //   console.log('masok');
      // }),
    );

    return result;
  }
}
