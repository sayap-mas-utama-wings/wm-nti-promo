import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ParamZfnCpChkShipmentExist extends SapRfcObject {
  readonly I_TKNUM?: string;
}
