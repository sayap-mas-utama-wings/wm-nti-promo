import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ReturnZfnCmGetPoVendorInterfaceSap extends SapRfcObject {
  readonly PV_EBELN?: string;
  readonly PV_AEDAT?: string;
  readonly PV_VSART?: string;
  readonly PV_WERKS?: string;
  readonly PV_LGORT?: string;
  readonly PV_FRGKE?: string;
  readonly PV_FLAG_GUDANG?: string;
  readonly PV_MESSAGE?: string;
  readonly E_BONGKAR_MIM? :string;
}
