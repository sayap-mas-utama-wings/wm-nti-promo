import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ReturnZfnCpCheckPrintSjInterfaceSap extends SapRfcObject {
  readonly E_EXIST?: string;
}
