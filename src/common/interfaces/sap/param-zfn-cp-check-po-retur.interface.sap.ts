import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ParamZfnCpCheckPoReturInterfaceSap extends SapRfcObject {
  readonly I_EBELN?: string;
}
