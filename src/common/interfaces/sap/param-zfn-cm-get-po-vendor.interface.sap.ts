import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ParamZfnCmGetPoVendorInterfaceSap extends SapRfcObject {
  readonly PV_PO_VENDOR?: string;
  readonly PV_PO_PLANT?: string;
  readonly PV_FLAG_CROSS_PO?: string;
  readonly PV_FLAG_ECP?: string;
}
