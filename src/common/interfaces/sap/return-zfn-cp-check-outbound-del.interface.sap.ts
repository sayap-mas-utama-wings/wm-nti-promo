import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ReturnZfnCpCheckOutboundDelInterfaceSap extends SapRfcObject {
  readonly E_ERROR?: string;
  readonly E_MESSAGE?: string;
}
