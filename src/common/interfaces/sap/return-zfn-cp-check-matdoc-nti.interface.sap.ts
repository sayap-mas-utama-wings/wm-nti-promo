import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ReturnZfnCpCheckMatdocNti extends SapRfcObject {
  readonly E_ISERROR?: string;
  readonly E_MESSAGE?: string;
}
