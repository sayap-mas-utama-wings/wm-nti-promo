import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ParamZfnCpCheckOutboundDelInterfaceSap extends SapRfcObject {
  readonly I_VBELN?: string;
  readonly I_WERKS?: string;
}
