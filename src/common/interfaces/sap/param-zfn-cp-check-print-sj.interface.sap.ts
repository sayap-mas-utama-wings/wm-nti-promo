import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ParamZfnCpCheckPrintSjInterfaceSap extends SapRfcObject {
  readonly I_TKNUM?: string;
}
