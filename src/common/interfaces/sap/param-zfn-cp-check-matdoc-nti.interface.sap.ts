import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ParamZfnCpCheckMatdocNti extends SapRfcObject {
  readonly I_MBLNR?: string;
  readonly I_MJAHR?: string;
}
