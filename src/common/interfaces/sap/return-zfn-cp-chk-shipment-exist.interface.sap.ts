import { SapRfcObject } from 'nestjs-sap-rfc';

export interface ReturnZfnCpChkShipmentExist extends SapRfcObject {
  readonly E_ISERROR?: string;
  readonly E_MESSAGE?: string;
}
