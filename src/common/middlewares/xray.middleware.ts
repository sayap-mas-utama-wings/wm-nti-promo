import { NestMiddleware, Injectable, Logger } from '@nestjs/common';
import { Request, Response } from 'express';
import { AWSXRay } from 'src/config/xray/xray';

@Injectable()
export class XRayMiddleware implements NestMiddleware {
  private readonly logger = new Logger(XRayMiddleware.name);

  use(req: Request, res: Response, next: Function) {
    AWSXRay.captureHTTPsGlobal(require('http'));
    AWSXRay.captureHTTPsGlobal(require('https'));

    // Create a custom segment to capture request data
    const segment = AWSXRay.getSegment();
    segment.addMetadata('Request Body', JSON.stringify(req.body));

    // Capture response data
    const originalSend = res.send;
    res.send = function (body) {
      segment.addMetadata('Response Body', JSON.stringify(body));
      return originalSend.apply(res, arguments);
    };

    next();
  }
}
