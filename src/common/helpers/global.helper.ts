import { In, ILike, Between, Raw } from 'typeorm';
import { formatYearMonthDate, validationDate } from './date.helpers';

export const addWhereClause = (param: string) => {
  return param.includes(',') || param.includes("'")
    ? In(param.split(','))
    : ILike('%' + param + '%');
};

export const addWhereClauseMust = (param: string) => {
  console.log('🚀 ~ addWhereClauseMust ~ param:', param);
  return param.includes(',') || param.includes("'")
    ? In(param.split(','))
    : param;
};

export const generateWhereClauseByAttr = (search: any, objectAttr: any) => {
  // console.log(`search1: ${JSON.stringify(search)}`);
  // check if search.genSearch is not empty
  const search2 = {};
  if (search?.genSearch && search?.genSearch != '') {
    for (const key of objectAttr.string) {
      search[key] = search?.genSearch ?? search?.[key] ?? '';
    }
    if (objectAttr?.stringMust) {
      for (const key of objectAttr.stringMust) {
        search[key] = search?.genSearch ?? search?.[key] ?? '';
      }
    }

    Object.assign(search2, search);
    if (objectAttr?.number) {
      for (const key of objectAttr.number) {
        search2[key] = search?.genSearch ?? search2?.[key] ?? '';
      }
    }
  }
  console.log('🚀 ~ generateWhereClauseByAttr ~ search2:', search2);

  // assign value genSearch to all column number in search
  if (
    search?.genSearch &&
    !isNaN(search?.genSearch) &&
    objectAttr.hasOwnProperty('number')
  ) {
    for (const key of objectAttr['number']) {
      search2[key] = search?.genSearch ?? search2?.[key];
    }
  }
  // console.log(`search2: ${JSON.stringify(search)}`);

  let whereClause: any;
  if (!search?.genSearch) {
    // advanced search from datatable using where and
    whereClause = {};
    if (objectAttr?.stringMust) {
      for (const key of objectAttr.stringMust) {
        if (search?.[key] && search[key] != '') {
          Object.assign(whereClause, {
            [key]: addWhereClauseMust(search[key]),
          });
        }
      }
    }

    if (objectAttr?.string) {
      for (const key of objectAttr.string) {
        if (search?.[key] && search[key] != '') {
          Object.assign(whereClause, {
            [key]: addWhereClause(search[key]),
          });
        }
      }
    }

    if (objectAttr?.number) {
      for (const key of objectAttr.number) {
        if (
          search?.[key] &&
          search[key] != '' &&
          typeof search[key] == 'number'
        ) {
          Object.assign(whereClause, {
            [key]: search[key],
          });
        }
      }
    }

    if (objectAttr?.date) {
      for (const key of objectAttr.date) {
        if (search?.[key] && search[key] != '' && validationDate(search[key])) {
          Object.assign(whereClause, {
            [key]: Between(
              `${formatYearMonthDate(search[key])} 00:00:00`,
              `${formatYearMonthDate(search[key])} 23:59:59`,
            ),
          });
        }
      }
    }
  } else {
    // general search from datatable using where or

    whereClause = [];
    if (objectAttr?.stringMust) {
      for (const key of objectAttr.stringMust) {
        if (search?.[key] && search[key] != '') {
          whereClause.push({
            [key]: addWhereClauseMust(search[key]),
          });
        }
      }
    }

    if (objectAttr?.string) {
      for (const key of objectAttr.string) {
        if (search?.[key] && search[key] != '') {
          whereClause.push({
            [key]: addWhereClause(search[key]),
          });
        }
      }
    }

    if (objectAttr?.number) {
      console.log(
        '🚀 ~ generateWhereClauseByAttr ~ objectAttr?.number:',
        objectAttr?.number,
      );
      for (const key of objectAttr.number) {
        if (
          search2?.[key] &&
          search2[key] != ''
          // &&
          // typeof search[key] == 'number'
        ) {
          // whereClause.push({
          //   [key]: search[key],
          // });
          whereClause.push({
            [key]: Raw(
              (alias) => `cast(${alias} as text) like '%${search2[key]}%'`,
            ),
          });
        }
      }
    }

    if (objectAttr?.date) {
      for (const key of objectAttr.date) {
        if (search?.[key] && search[key] != '' && validationDate(search[key])) {
          whereClause.push({
            [key]: Between(
              `${formatYearMonthDate(search[key])} 00:00:00`,
              `${formatYearMonthDate(search[key])} 23:59:59`,
            ), //search[key],
          });
        }
      }
    }
  }

  return whereClause;
};

export const numberGenerateDigit = (angka: number) => {
  const lengthAngka = `${angka}`.length;
  let result = '';

  switch (lengthAngka) {
    case 1:
      result = `000` + angka;
      break;
    case 2:
      result = `00` + angka;
      break;
    case 3:
      result = `0` + angka;
      break;
    case 4:
      result = `${angka}`;
      break;
  }

  return result;
};
