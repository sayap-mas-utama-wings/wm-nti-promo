import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './configuration';
import * as Joi from '@hapi/joi';
import { PgWmConfigService } from './config.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        PG_WM_HOST: Joi.string(),
        PG_WM_PORT: Joi.number(),
        PG_WM_USERNAME: Joi.string(),
        PG_WM_PASSWORD: Joi.string(),
        PG_WM_DATABASE: Joi.string(),
        LOG_QUERY: Joi.string(),
      }),
    }),
  ],
  providers: [ConfigService, PgWmConfigService],
  exports: [ConfigService, PgWmConfigService],
})
export class PgWmConfigModule {}
