import { registerAs } from '@nestjs/config';

export default registerAs('pgwm', () => ({
  host: process.env.PG_NTI_PROMO_HOST,
  port: process.env.PG_NTI_PROMO_PORT,
  username: process.env.PG_NTI_PROMO_USERNAME,
  password: process.env.PG_NTI_PROMO_PASSWORD,
  database: process.env.PG_NTI_PROMO_DATABASE,
  log_query: process.env.LOG_QUERY,
}));
