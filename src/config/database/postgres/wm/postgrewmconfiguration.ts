import { registerAs } from '@nestjs/config';

export default registerAs('postgreWm', () => ({
  host: process.env.PG_WM_HOST,
  port: process.env.PG_WM_PORT,
  username: process.env.PG_WM_USERNAME,
  password: process.env.PG_WM_PASSWORD,
  database: process.env.PG_WM_DATABASE,
}));
