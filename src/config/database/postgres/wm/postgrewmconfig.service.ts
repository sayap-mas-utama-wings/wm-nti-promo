import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class PostgreWmConfigService {
  constructor(private configService: ConfigService) {}

  get host(): string {
    return this.configService.get<string>('postgreWm.host');
  }

  get port(): number {
    return Number(this.configService.get<number>('postgreWm.port'));
  }

  get username(): string {
    return this.configService.get<string>('postgreWm.username');
  }

  get password(): string {
    return this.configService.get<string>('postgreWm.password');
  }

  get database(): string {
    return this.configService.get<string>('postgreWm.database');
  }
}
