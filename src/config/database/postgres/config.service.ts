import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class PgWmConfigService {
  constructor(private configService: ConfigService) {}

  get host(): string {
    return this.configService.get<string>('pgwm.host');
  }

  get port(): number {
    return Number(this.configService.get<number>('pgwm.port'));
  }

  get username(): string {
    return this.configService.get<string>('pgwm.username');
  }

  get password(): string {
    return this.configService.get<string>('pgwm.password');
  }

  get database(): string {
    return this.configService.get<string>('pgwm.database');
  }

  get log_query(): string {
    return this.configService.get<string>('pgcp.log_query');
  }
}
