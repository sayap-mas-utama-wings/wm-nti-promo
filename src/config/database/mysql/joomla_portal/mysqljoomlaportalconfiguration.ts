import { registerAs } from '@nestjs/config';

export default registerAs('mysqlJoomlaPortal', () => ({
  host: process.env.MYSQL_JOOMLA_PORTAL_HOST,
  port: process.env.MYSQL_JOOMLA_PORTAL_PORT,
  username: process.env.MYSQL_JOOMLA_PORTAL_USERNAME,
  password: process.env.MYSQL_JOOMLA_PORTAL_PASSWORD,
  database: process.env.MYSQL_JOOMLA_PORTAL_DATABASE,
}));
