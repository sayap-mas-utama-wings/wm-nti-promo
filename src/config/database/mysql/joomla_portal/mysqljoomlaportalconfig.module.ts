import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './mysqljoomlaportalconfiguration';
import * as Joi from '@hapi/joi';
import { MysqlJoomlaPortalConfigService } from './mysqljoomlaportalconfig.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        MYSQL_JOOMLA_PORTAL_HOST: Joi.string(),
        MYSQL_JOOMLA_PORTAL_PORT: Joi.number(),
        MYSQL_JOOMLA_PORTAL_USERNAME: Joi.string(),
        MYSQL_JOOMLA_PORTAL_PASSWORD: Joi.string(),
        MYSQL_JOOMLA_PORTAL_DATABASE: Joi.string(),
      }),
    }),
  ],
  providers: [ConfigService, MysqlJoomlaPortalConfigService],
  exports: [ConfigService, MysqlJoomlaPortalConfigService],
})
export class MysqlJoomlaPortalConfigModule {}
