import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MysqlJoomlaPortalConfigService {
  constructor(private configService: ConfigService) {}

  get host(): string {
    return this.configService.get<string>('mysqlJoomlaPortal.host');
  }

  get port(): number {
    return Number(this.configService.get<number>('mysqlJoomlaPortal.port'));
  }

  get username(): string {
    return this.configService.get<string>('mysqlJoomlaPortal.username');
  }

  get password(): string {
    return this.configService.get<string>('mysqlJoomlaPortal.password');
  }

  get database(): string {
    return this.configService.get<string>('mysqlJoomlaPortal.database');
  }
}
