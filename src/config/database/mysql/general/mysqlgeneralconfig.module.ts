import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './mysqlgeneralconfiguration';
import * as Joi from '@hapi/joi';
import { MysqlGeneralConfigService } from './mysqlgeneralconfig.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        MYSQL_GENERAL_HOST: Joi.string(),
        MYSQL_GENERAL_PORT: Joi.number(),
        MYSQL_GENERAL_USERNAME: Joi.string(),
        MYSQL_GENERAL_PASSWORD: Joi.string(),
        MYSQL_GENERAL_DATABASE: Joi.string(),
      }),
    }),
  ],
  providers: [ConfigService, MysqlGeneralConfigService],
  exports: [ConfigService, MysqlGeneralConfigService],
})
export class MysqlGeneralConfigModule {}
