import { registerAs } from '@nestjs/config';

export default registerAs('mysqlGeneral', () => ({
  host: process.env.MYSQL_GENERAL_HOST,
  port: process.env.MYSQL_GENERAL_PORT,
  username: process.env.MYSQL_GENERAL_USERNAME,
  password: process.env.MYSQL_GENERAL_PASSWORD,
  database: process.env.MYSQL_GENERAL_DATABASE,
}));
