import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MysqlGeneralConfigService {
  constructor(private configService: ConfigService) {}

  get host(): string {
    return this.configService.get<string>('mysqlGeneral.host');
  }

  get port(): number {
    return Number(this.configService.get<number>('mysqlGeneral.port'));
  }

  get username(): string {
    return this.configService.get<string>('mysqlGeneral.username');
  }

  get password(): string {
    return this.configService.get<string>('mysqlGeneral.password');
  }

  get database(): string {
    return this.configService.get<string>('mysqlGeneral.database');
  }
}
