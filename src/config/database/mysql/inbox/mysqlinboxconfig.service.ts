import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MysqlInboxConfigService {
  constructor(private configService: ConfigService) {}

  get host(): string {
    return this.configService.get<string>('mysqlInbox.host');
  }

  get port(): number {
    return Number(this.configService.get<number>('mysqlInbox.port'));
  }

  get username(): string {
    return this.configService.get<string>('mysqlInbox.username');
  }

  get password(): string {
    return this.configService.get<string>('mysqlInbox.password');
  }

  get database(): string {
    return this.configService.get<string>('mysqlInbox.database');
  }
}
