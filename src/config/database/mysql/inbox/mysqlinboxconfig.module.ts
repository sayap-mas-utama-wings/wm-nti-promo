import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MysqlInboxConfigService } from './mysqlinboxconfig.service';
import * as Joi from '@hapi/joi';
import configuration from './mysqlinboxconfiguration';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        MYSQL_INBOX_HOST: Joi.string(),
        MYSQL_INBOX_PORT: Joi.number(),
        MYSQL_INBOX_USERNAME: Joi.string(),
        MYSQL_INBOX_PASSWORD: Joi.string(),
        MYSQL_INBOX_DATABASE: Joi.string(),
      }),
    }),
  ],
  providers: [ConfigService, MysqlInboxConfigService],
  exports: [ConfigService, MysqlInboxConfigService],
})
export class MysqlInboxConfigModule {}
