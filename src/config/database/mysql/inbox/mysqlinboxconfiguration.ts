import { registerAs } from '@nestjs/config';

export default registerAs('mysqlInbox', () => ({
  host: process.env.MYSQL_INBOX_HOST,
  port: process.env.MYSQL_INBOX_PORT,
  username: process.env.MYSQL_INBOX_USERNAME,
  password: process.env.MYSQL_INBOX_PASSWORD,
  database: process.env.MYSQL_INBOX_DATABASE,
}));
