import { Module } from '@nestjs/common';
import { DynamicDbService } from './dynamic-db.service';
import { MysqlGeneralConfigModule } from 'src/config/database/mysql/general/mysqlgeneralconfig.module';
import { MysqlJoomlaPortalConfigModule } from 'src/config/database/mysql/joomla_portal/mysqljoomlaportalconfig.module';
import { MysqlInboxConfigModule } from 'src/config/database/mysql/inbox/mysqlinboxconfig.module';
import { PostgreWmConfigModule } from 'src/config/database/postgres/wm/postgrewmconfig.module';

@Module({
  imports: [
    MysqlGeneralConfigModule,
    MysqlJoomlaPortalConfigModule,
    MysqlInboxConfigModule,
    PostgreWmConfigModule,
  ],
  providers: [DynamicDbService],
  exports: [DynamicDbService],
})
export class DynamicDbModule {}
