import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { PgWmConfigModule } from 'src/config/database/postgres/config.module';
import { PgWmConfigService } from 'src/config/database/postgres/config.service';
import { DatabaseType } from 'typeorm';
import { MNtiUser } from 'src/models/mntiuser/entities/mntiuser';
import { MNtiReason } from 'src/models/mntireason/entities/mntireason.entity';
import { MPicmarketing } from 'src/models/mpicmarketing/entities/mpicmarketing';
import { MSetting } from 'src/models/msetting/entities/msetting';
import { TReqNtiHdr } from 'src/models/treqntihdr/entities/treqntihdr.entity';
import { TReqNtiDtl } from 'src/models/treqntidtl/entities/treqntidtl.entity';
import { TLogNti } from 'src/models/tlognti/entities/tlognti';
import { TReqNtiWorkflow } from 'src/models/treqntiworkflow/entities/treqntiworkflow';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [PgWmConfigModule],
      useFactory: async (pgWmConfigService: PgWmConfigService) => ({
        type: 'postgres' as DatabaseType,
        host: pgWmConfigService.host,
        port: pgWmConfigService.port,
        username: pgWmConfigService.username,
        password: pgWmConfigService.password,
        database: pgWmConfigService.database,
        entities: [
          MNtiUser,
          MNtiReason,
          MPicmarketing,
          MSetting,
          TReqNtiHdr,
          TReqNtiDtl,
          TLogNti,
          TReqNtiWorkflow,
        ],
        // synchronize: true,
        // logging: pgWmConfigService.log_query === 'true' ? true : false,
        logging: true,
        // ssl: true,
        // extra: {
        //   ssl: {
        //     rejectUnauthorized: false,
        //   },
        // },
      }),
      inject: [PgWmConfigService],
    } as TypeOrmModuleAsyncOptions),
  ],
})
export class PgWmDatabaseProviderModule {}
