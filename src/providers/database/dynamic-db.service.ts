import { Injectable } from '@nestjs/common';
import { MysqlGeneralConfigService } from 'src/config/database/mysql/general/mysqlgeneralconfig.service';
import { MysqlInboxConfigService } from 'src/config/database/mysql/inbox/mysqlinboxconfig.service';
import { MysqlJoomlaPortalConfigService } from 'src/config/database/mysql/joomla_portal/mysqljoomlaportalconfig.service';
import { PostgreWmConfigService } from 'src/config/database/postgres/wm/postgrewmconfig.service';
import { connections } from 'src/main';
import { MCostCenter } from 'src/models/general/mcostcenter/entities/mcostcenter';
import { MFtpS3 } from 'src/models/general/mftps3/entities/mftps3.entity';
import { MMaterial } from 'src/models/general/mmaterial/entities/mmaterial.entity';
import { MPh3Cc } from 'src/models/general/mph3cc/entities/mph3cc.entity';
import { MPlant } from 'src/models/general/mplant/entities/mplant';
import { MPlantSalesOffice } from 'src/models/general/mplantsalesoffice/entities/mplantsalesoffice';
import { MPlantTransplan } from 'src/models/general/mplanttransplan/entities/mplanttransplan.entity';
import { MRfcSalesoffice } from 'src/models/general/mrfcsalesoffice/entities/mrfcsalesoffice';
import { MSalesOffice } from 'src/models/general/msalesoffice/entities/msalesoffice';
import { MSloc } from 'src/models/general/msloc/entities/msloc';
import { MTransplantPoint } from 'src/models/general/mtransplantpoint/entities/mtransplantpoint.entity';
import { TInboxV2 } from 'src/models/inbox/tinboxv2/entities/tinboxv2';
import { SmuUsers } from 'src/models/joomlaportal/smuusers/entities/smuusers.entity';
import { MSlocHo } from 'src/models/mslocho/entities/mslocho.entity';
import { MSetting } from 'src/models/msetting/entities/msetting';
import { DataSource, DataSourceOptions } from 'typeorm';
import { MPltGsberSlsoff } from 'src/models/general/mpltgsberslsoff/entities/mpltgsberslsoff';

@Injectable()
export class DynamicDbService {
  constructor(
    private mysqlGeneralConfigService: MysqlGeneralConfigService,
    private mysqlJoomlaPortalConfigService: MysqlJoomlaPortalConfigService,
    private mysqlInboxConfigService: MysqlInboxConfigService,
    private postgreWmConfigService: PostgreWmConfigService,
  ) {}

  async createDynamicConnectionWm(): Promise<DataSource> {
    try {
      const codename = `${this.postgreWmConfigService.database}DB`;
      const dso: DataSourceOptions = {
        name: codename,
        type: 'postgres',
        host: this.postgreWmConfigService.host,
        port: this.postgreWmConfigService.port,
        username: this.postgreWmConfigService.username,
        password: this.postgreWmConfigService.password,
        database: this.postgreWmConfigService.database,
        entities: [MSetting],
        synchronize: false,
        logging: true,
      };
      return await this.createOrGetConnection(codename, dso);
    } catch (error) {
      throw error;
    }
  }

  async createDynamicConnectionGeneral(): Promise<DataSource> {
    try {
      const codename = `${this.mysqlGeneralConfigService.database}DB`;
      const dso: DataSourceOptions = {
        name: codename,
        type: 'mysql',
        host: this.mysqlGeneralConfigService.host,
        port: this.mysqlGeneralConfigService.port,
        username: this.mysqlGeneralConfigService.username,
        password: this.mysqlGeneralConfigService.password,
        database: this.mysqlGeneralConfigService.database,
        entities: [
          MSlocHo,
          MPlantTransplan,
          MFtpS3,
          MTransplantPoint,
          MPh3Cc,
          MMaterial,
          MPlantSalesOffice,
          MCostCenter,
          MPlant,
          MSloc,
          MSalesOffice,
          MRfcSalesoffice,
          MPltGsberSlsoff,
        ],
        synchronize: false,
        logging: true,
      };
      return await this.createOrGetConnection(codename, dso);
    } catch (error) {
      throw error;
    }
  }

  async createDynamicConnectionJoomlaPortal(): Promise<DataSource> {
    try {
      const codename = `${this.mysqlJoomlaPortalConfigService.database}DB`;
      const dso: DataSourceOptions = {
        name: codename,
        type: 'mysql',
        host: this.mysqlJoomlaPortalConfigService.host,
        port: this.mysqlJoomlaPortalConfigService.port,
        username: this.mysqlJoomlaPortalConfigService.username,
        password: this.mysqlJoomlaPortalConfigService.password,
        database: this.mysqlJoomlaPortalConfigService.database,
        entities: [SmuUsers],
        synchronize: false,
        logging: true,
      };
      return await this.createOrGetConnection(codename, dso);
    } catch (error) {
      throw error;
    }
  }

  async createDynamicConnectionInbox(): Promise<DataSource> {
    try {
      const codename = `${this.mysqlInboxConfigService.database}DB`;
      const dso: DataSourceOptions = {
        name: codename,
        type: 'mysql',
        host: this.mysqlInboxConfigService.host,
        port: this.mysqlInboxConfigService.port,
        username: this.mysqlInboxConfigService.username,
        password: this.mysqlInboxConfigService.password,
        database: this.mysqlInboxConfigService.database,
        entities: [TInboxV2],
        synchronize: false,
        logging: true,
      };
      return await this.createOrGetConnection(codename, dso);
    } catch (error) {
      throw error;
    }
  }

  async createOrGetConnection(codename: string, dso: DataSourceOptions = null) {
    try {
      let conn: DataSource = connections.get(codename);
      if (!conn) {
        console.log('init?', codename);
        if (!dso) throw new Error(`Invalid DSO ${codename}`);
        const newConn = await new DataSource(dso).initialize();
        connections.set(newConn.name, newConn);
        conn = newConn;
      }
      return conn;
    } catch (error) {
      console.log('Err-createOrGetConnection', error);
      throw new Error(error);
    }
  }
}
