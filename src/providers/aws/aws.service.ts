import { Injectable } from '@nestjs/common';
import {
  GetObjectCommand,
  GetObjectCommandInput,
  GetObjectCommandOutput,
  PutObjectCommand,
  PutObjectCommandInput,
  S3Client,
  ServiceOutputTypes,
} from '@aws-sdk/client-s3';
import { Readable } from 'stream';

@Injectable()
export class AwsService {
  async uploadS3FileB64(
    bucket: string,
    accessKeyId: string,
    secretAccessKey: string,
    region: string,
    file: string,
    fileName: string,
    mimeType: string,
  ): Promise<ServiceOutputTypes> {
    const buff: Buffer = Buffer.from(file, 'base64');
    return await this.uploadToS3(
      bucket,
      accessKeyId,
      secretAccessKey,
      region,
      buff,
      fileName,
      mimeType,
      'base64',
    );
  }

  async readStringFromS3(
    bucket: string,
    accessKeyId: string,
    secretAccessKey: string,
    region: string,
    fileName: string,
    stringEncoding: BufferEncoding,
  ): Promise<string> {
    try {
      const s3Client: S3Client = new S3Client({
        credentials: {
          accessKeyId: accessKeyId,
          secretAccessKey: secretAccessKey,
        },
        region: region,
      });

      const params: GetObjectCommandInput = {
        Bucket: bucket,
        Key: String(fileName),
      };

      const output: GetObjectCommandOutput = await s3Client.send(
        new GetObjectCommand(params),
      );

      const bodyContents: string = await this.streamToString(
        output.Body as Readable,
        stringEncoding,
      );

      return bodyContents;
    } catch (error) {
      throw error;
    }
  }

  private async streamToString(
    stream: Readable,
    encoding: BufferEncoding,
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      const chunks = [];
      stream.on('data', (chunk) => chunks.push(chunk));
      stream.on('error', reject);
      stream.on('end', () => resolve(Buffer.concat(chunks).toString(encoding)));
    });
  }

  private async uploadToS3(
    bucket: string,
    accessKeyId: string,
    secretAccessKey: string,
    region: string,
    file: any,
    fileName: string,
    mimeType: string,
    contentEncoding: string,
  ): Promise<ServiceOutputTypes> {
    try {
      const s3Client: S3Client = new S3Client({
        credentials: {
          accessKeyId: accessKeyId,
          secretAccessKey: secretAccessKey,
        },
        region: region,
      });

      const params: PutObjectCommandInput = {
        Bucket: bucket,
        Key: String(fileName),
        Body: file,
        // ACL: 'public-read',
        ContentType: mimeType,
        ContentEncoding: contentEncoding,
        // ContentDisposition: 'inline',
        // CreateBucketConfiguration: {
        //   LocationConstraint: 'ap-southeast-1',
        // },
      };

      const output: ServiceOutputTypes = await s3Client.send(
        new PutObjectCommand(params),
      );
      return output;
    } catch (error) {
      throw error;
    }
  }
}
