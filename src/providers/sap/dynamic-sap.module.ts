import { Module } from '@nestjs/common';
import { DynamicSapService } from './dynamic-sap.service';
// import { SapModule } from 'nestjs-sap-rfc/sap.module';

@Module({
  providers: [DynamicSapService],
  exports: [DynamicSapService],
})
export class DynamicSapModule {}
