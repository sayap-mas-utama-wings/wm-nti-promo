import { Injectable } from '@nestjs/common';
import { isBase64 } from 'class-validator';
import { SapClientService, SapService } from 'nestjs-sap-rfc';
import {
  Client,
  RfcClientBinding,
  RfcConnectionParameters,
  RfcClientOptions,
} from 'node-rfc';
import { MConfigsap } from 'src/models/mconfigsap/entities/mconfigsap.entity';

@Injectable()
export class DynamicSapService {
  async createDynamicConnection(
    arg1: RfcClientBinding | RfcConnectionParameters,
    clientOptions?: RfcClientOptions,
  ): Promise<SapService> {
    try {
      return new SapClientService(new Client(arg1, clientOptions));
    } catch (error) {
      throw error;
    }
  }

  async createDynamicSapConnection(config: MConfigsap): Promise<SapService> {
    try {
      if (isBase64(config.password)) {
        config.password = Buffer.from(config.password, 'base64').toString(
          'ascii',
        );
      }
      return this.createDynamicConnection({
        ashost: config.msHost ? '' : config.host,
        client: config.client,
        user: config.username,
        passwd: config.password,
        sysid: config.systemId,
        sysnr: config.systemNumber,
        lang: config.lang,
        group: config.group,
        mshost: config.msHost,
      });
    } catch (error) {
      throw error;
    }
  }
}
