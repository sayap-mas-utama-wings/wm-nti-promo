import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { json } from 'express';
import { DataSource } from 'typeorm';
import { AWSXRay } from './config/xray/xray';
import { TransformInterceptor } from './common/interceptors/transform-interceptor';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Use X-Ray middleware
  app.use(AWSXRay.express.openSegment('checkpointmanuf'));

  // app.use('/securityin/uploadBase64', json({ limit: '3mb' }));
  app.use('/securityin/confirmIn', json({ limit: '3mb' }));
  app.use('/ftpcpmanuf/uploadS3FileB64', json({ limit: '3mb' }));
  app.use(json({ limit: '100kb' }));
  app.useGlobalInterceptors(new TransformInterceptor());

  // Close the AWS X-Ray segment
  app.use(AWSXRay.express.closeSegment());

  await app.listen(process.env.PORT || 3000);
}
bootstrap();

export const connections: Map<string, DataSource> = new Map();
