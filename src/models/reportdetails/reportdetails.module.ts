import { Module } from '@nestjs/common';
import { ReportDetailsService } from './reportdetails.service';
import { ReportDetailsController } from './reportdetails.controller';
import { TLogNtiModule } from '../tlognti/tlognti.module';

@Module({
  imports: [TLogNtiModule],
  providers: [ReportDetailsService],
  controllers: [ReportDetailsController],
})
export class ReportDetailsModule {}
