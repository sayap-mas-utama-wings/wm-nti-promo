import { Controller, Get, HttpStatus, Req } from '@nestjs/common';
import { ReportDetailsService } from './reportdetails.service';
import { responseError, responsePage } from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';

@Controller('reportdetails')
export class ReportDetailsController {
  constructor(private readonly reportDetailsService: ReportDetailsService) {}

  @Get()
  async listDetail(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.reportDetailsService.listReport(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('listRequestId')
  async listRequestId(@Req() req): Promise<any> {
    try {
      return await this.reportDetailsService.getlistIdRequest(req?.query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('listStatus')
  async listStatus(): Promise<any> {
    try {
      return await this.reportDetailsService.getlistStatus();
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
