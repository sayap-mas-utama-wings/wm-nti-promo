import { Injectable } from '@nestjs/common';
import { TLogNtiService } from '../tlognti/tlognti.service';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';

@Injectable()
export class ReportDetailsService {
  constructor(private readonly tLogNtiService: TLogNtiService) {}

  async getlistIdRequest(search?: any): Promise<any> {
    try {
      let whereClause: any;
      if (search) {
        const objSearchAttr = {
          string: ['requestId'],
          number: [],
          date: [],
        };
        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
      }
      const result = await this.tLogNtiService.findAll(
        ['tLogNti.requestId as "requestId"'],
        'tLogNti.requestId',
        whereClause,
      );

      return result;
    } catch (error) {
      throw error;
    }
  }

  async getlistStatus(): Promise<any> {
    try {
      const result = await this.tLogNtiService.findAll(
        ['tLogNti.status as "status"'],
        'tLogNti.status',
        {},
      );
      console.log(
        '🚀 ~ ReportDetailsService ~ getlistStatus ~ result:',
        result,
      );

      if (result) {
        return result;
      }

      return ['kosong'];
    } catch (error) {
      throw error;
    }
  }

  async listReport(pagination: PaginationOptions, search?: any): Promise<any> {
    try {
      const result = await this.tLogNtiService.findAndPagination(
        pagination,
        search,
        [
          'tLogNtiRepo.requestId',
          'tLogNtiRepo.plant',
          'tLogNtiRepo.plantDescription',
          'tLogNtiRepo.createdDate',
          'tLogNtiRepo.createdBy',
          'tLogNtiRepo.createdName',
          'tLogNtiRepo.status',
          'tLogNtiRepo.materialId',
          'tLogNtiRepo.materialDescription',
          'tLogNtiRepo.submittedQty',
          'tLogNtiRepo.remainingQty',
          'tLogNtiRepo.confirmQty',
          'tLogNtiRepo.uom',
          'tLogNtiRepo.ntiReason',
          'tLogNtiRepo.scrapReason',
          'tLogNtiRepo.category',
          'tLogNtiRepo.costCenter',
          'tLogNtiRepo.costCenterDescription',
          'tLogNtiRepo.matDoc',
          'tLogNtiRepo.reasonReject',
          'tLogNtiRepo.reasonComplete',
        ],
        [
          'tLogNtiRepo.requestId',
          // 'tLogNtiRepo.plant',
          // 'tLogNtiRepo.plantDescription',
          // 'tLogNtiRepo.createdDate',
          'tLogNtiRepo.createdBy',
          // 'tLogNtiRepo.status',
          // 'tLogNtiRepo.materialId',
          // 'tLogNtiRepo.submittedQty',
          // 'tLogNtiRepo.remainingQty',
          // 'tLogNtiRepo.confirmQty',
          // 'tLogNtiRepo.uom',
          // 'tLogNtiRepo.ntiReason',
          // 'tLogNtiRepo.scrapReason',
          // 'tLogNtiRepo.category',
          // 'tLogNtiRepo.costCenter',
          // 'tLogNtiRepo.costCenterDescription',
          // 'tLogNtiRepo.matDoc',
          // 'tLogNtiRepo.reasonReject',
          // 'tLogNtiRepo.reasonComplete',
        ],
      );
      return result;
    } catch (error) {
      throw error;
    }
  }
}
