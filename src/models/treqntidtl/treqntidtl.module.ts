import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TReqNtiDtl } from './entities/treqntidtl.entity';
import { TReqNtiDtlService } from './treqntidtl.service';
import { TReqNtiDtlController } from './treqntidtl.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TReqNtiDtl])],
  controllers: [TReqNtiDtlController],
  providers: [TReqNtiDtlService],
  exports: [TReqNtiDtlService],
})
export class TReqntiDtlModule {}
