import { Controller, Get, HttpStatus, Req } from '@nestjs/common';
import { TReqNtiDtlService } from './treqntidtl.service';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';

@Controller('treqntidtl')
export class TReqNtiDtlController {
  constructor(private readonly tReqNtiDtlService: TReqNtiDtlService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.tReqNtiDtlService.findAndPaginate(
        pagination,
        req?.query,
        'TReqNtiDtl.ntiId, TReqNtiDtl.materialId, TReqNtiDtl.ntiReason, TReqNtiDtl.scrapReason',
        [
          'TReqNtiDtl.ntiId',
          'TReqNtiDtl.requestId',
          'TReqNtiDtl.materialId',
          'TReqNtiDtl.materialDescription',
          'TReqNtiDtl.submittedQty',
          'TReqNtiDtl.uom',
          'TReqNtiDtl.remainingQty',
          'TReqNtiDtl.category',
          'TReqNtiDtl.ntiReason',
          'TReqNtiDtl.scrapReason',
        ],
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
