import { TReqNtiHdr } from 'src/models/treqntihdr/entities/treqntihdr.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('t_req_nti_dtl')
export class TReqNtiDtl {
  @PrimaryColumn({
    name: 'nti_id',
    type: 'integer',
    select: false,
  })
  ntiId: number;

  @Column({
    name: 'request_id',
    type: 'varchar',
    length: 12,
  })
  requestId: string;

  @Column({
    name: 'line_item',
    type: 'int4',
    default: 0,
  })
  lineItem: number;

  @Column({
    name: 'material_id',
    type: 'bigint',
    default: 0,
  })
  materialId: number;

  @Column({
    name: 'material_description',
    type: 'varchar',
    length: 50,
  })
  materialDescription: string;

  @Column({
    name: 'submitted_qty',
    type: 'float8',
    default: 0,
  })
  submittedQty: number;

  @Column({
    name: 'remaining_qty',
    type: 'float8',
    default: 0,
  })
  remainingQty: number;

  @Column({
    name: 'confirm_qty',
    type: 'float8',
    default: 0,
  })
  confirmQty: number;

  @Column({
    name: 'confirm_input_qty',
    type: 'float8',
    default: 0,
  })
  confirmInputQty: number;

  @Column({
    name: 'uom',
    type: 'varchar',
    length: 5,
    default: '',
  })
  uom: string;

  @Column({
    name: 'nti_reason',
    type: 'varchar',
    length: 100,
    default: '',
  })
  ntiReason: string;

  @Column({
    name: 'reason_id',
    type: 'int8',
  })
  reasonId: number;

  @Column({
    name: 'scrap_reason',
    type: 'varchar',
    length: 100,
    default: '',
  })
  scrapReason: string;

  @Column({
    name: 'category',
    type: 'varchar',
    length: 40,
    default: '',
  })
  category: string;

  @Column({
    name: 'cost_center',
    type: 'varchar',
    length: 10,
    default: '',
  })
  costCenter: string;

  @Column({
    name: 'cost_center_description',
    type: 'varchar',
    length: 50,
    default: '',
  })
  costCenterDescription: string;

  @Column({
    name: 'sloc',
    type: 'varchar',
    length: 50,
    default: '',
  })
  sloc: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 10,
    default: '',
  })
  status: string;

  @Column({
    name: 'mat_doc',
    type: 'varchar',
    length: 20,
    default: '',
  })
  matDoc: string;

  @Column({
    name: 'mat_doc_year',
    type: 'varchar',
    length: 10,
    default: '',
  })
  matDocYear: string;

  @Column({
    name: 'posting_date',
    type: 'date',
    default: '',
  })
  postingDate: string;

  @CreateDateColumn({
    name: 'created_date',
    type: 'date',
    default: () => 'NOW()::date',
  })
  public createdDate: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @Column({
    name: 'created_name',
    type: 'varchar',
    length: 50,
    default: '',
    nullable: false,
  })
  public createdName: string;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'date',
    onUpdate: 'NOW()',
  })
  public updatedDate: Date;

  @Column({
    name: 'updated_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public updatedBy: string;

  @Column({
    name: 'updated_name',
    type: 'varchar',
    length: 50,
    default: '',
    nullable: false,
  })
  public updatedName: string;

  @Column({
    name: 'reason_complete',
    type: 'varchar',
    length: 100,
  })
  reasonComplete: string;

  @Column({
    name: 'status_request',
    type: 'varchar',
    length: 20,
  })
  statusRequest: string;

  @Column({
    name: 'plant',
    type: 'varchar',
    length: 4,
  })
  plant: string;

  @ManyToOne(() => TReqNtiHdr, (tReqNti) => tReqNti.tReqNtiDtl)
  @JoinColumn({ name: 'request_id', referencedColumnName: 'requestId' })
  tReqNtiHdr: TReqNtiHdr;
}
