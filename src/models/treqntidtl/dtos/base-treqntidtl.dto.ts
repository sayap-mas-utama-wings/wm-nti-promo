import { IsNumber, IsString } from 'class-validator';

export class BaseTNtiDtlDto {
  @IsNumber()
  ntiId: number;

  @IsNumber()
  requestId: string;

  @IsNumber()
  lineItem: number;

  @IsNumber()
  materialId: number;

  @IsString()
  materialDescription: string;

  @IsNumber()
  submittedQty: number;

  @IsNumber()
  remainingQty: number;

  @IsNumber()
  confirmQty: number;

  @IsNumber()
  confirmInputQty: number;

  @IsString()
  uom: string;

  @IsString()
  ntiReason: string;

  @IsNumber()
  reasonId: number;

  @IsString()
  scrapReason: string;

  @IsString()
  category: string;

  @IsString()
  costCenter: string;

  @IsString()
  costCenterDescription: string;

  @IsString()
  sloc: string;

  @IsString()
  status: string;

  @IsString()
  matDoc: string;

  @IsString()
  matDocYear: string;

  @IsString()
  postingDate: string;

  @IsString()
  reasonComplete: string;

  @IsString()
  statusRequest: string;

  @IsString()
  plant: string;
}
