import { IsDateString, IsString } from 'class-validator';
import { BaseTNtiDtlDto } from './base-treqntidtl.dto';

export class CreateTNtiDtlDto extends BaseTNtiDtlDto {
  @IsDateString()
  startDate: string;

  @IsString()
  startTime: string;

  @IsString()
  startBy: string;

  @IsString()
  createdBy: string;

  @IsString()
  createdName: string;

  @IsString()
  createdDate: string;
}
