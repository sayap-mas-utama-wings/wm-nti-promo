import { IsDateString, IsString } from 'class-validator';
import { BaseTNtiDtlDto } from './base-treqntidtl.dto';

export class UpdateTNtiDtlDto extends BaseTNtiDtlDto {
  @IsDateString()
  updatedBy: string;

  @IsString()
  updatedName: string;

  @IsString()
  updatedDate: string;
}
