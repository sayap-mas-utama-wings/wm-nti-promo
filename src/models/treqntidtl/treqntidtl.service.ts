import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TReqNtiDtl } from './entities/treqntidtl.entity';
import {
  EntityManager,
  FindOptionsOrder,
  FindOptionsWhere,
  In,
  InsertResult,
  Like,
  Raw,
  Repository,
} from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { CreateTNtiDtlDto } from './dtos/create-treqntidtl.dto';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { UpdateTNtiDtlDto } from './dtos/update-treqntidtl.dto';

@Injectable()
export class TReqNtiDtlService {
  constructor(
    @InjectRepository(TReqNtiDtl) private tNtidtlRepo: Repository<TReqNtiDtl>,
  ) {}

  async create(
    createDto: QueryDeepPartialEntity<CreateTNtiDtlDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }
      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(TReqNtiDtl)
          .values(createDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.tNtidtlRepo
          .createQueryBuilder()
          .insert()
          .values(createDto)
          .returning(returning)
          .execute();
      }

      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(
    updateDto: QueryDeepPartialEntity<UpdateTNtiDtlDto>,
    whereCondition: FindOptionsWhere<TReqNtiDtl>,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      const repositoryUpdate = transactionManager
        ? transactionManager
        : this.tNtidtlRepo;

      const updated = await repositoryUpdate
        .createQueryBuilder()
        .update(TReqNtiDtl)
        .set({ ...updateDto })
        .where(whereCondition)
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async findOne(whereCondition: FindOptionsWhere<TReqNtiDtl>): Promise<any> {
    try {
      const data = await this.tNtidtlRepo.findOne({
        where: whereCondition,
      });

      if (!data) {
        throw new NotFoundException('Data not found');
      }

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    whereCondition: FindOptionsWhere<TReqNtiDtl>,
    orderCondition?: FindOptionsOrder<TReqNtiDtl>,
  ): Promise<any> {
    try {
      const data = await this.tNtidtlRepo.find({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) {
        throw new NotFoundException('Data t_nti_dtl not found');
      }

      return data;
    } catch (error) {
      throw error;
    }
  }

  async totalExecuted(
    whereCondition: FindOptionsWhere<TReqNtiDtl>,
  ): Promise<any> {
    try {
      const data = await this.tNtidtlRepo
        .createQueryBuilder('TReqNtiDtl')
        .select('SUM(remaining_qty) as total')
        .where(whereCondition)
        // .orderBy(orderCondition)
        .getRawOne();

      if (!data) {
        throw new NotFoundException('Data t_nti_dtl not found');
      }

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
    groupBy?: string,
    select?: string[],
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: [
            'materialDescription',
            'submitedQty',
            'remainingQty',
            'uom',
            'ntiReason',
            'scrapReason',
            'category',
            'requestId',
          ],
          number: ['ntiId', 'materialId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = [
          'material_id',
          'material_description',
          'submitted_qty',
          'remaining_qty',
          'uom',
          'nti_reason',
          'scrap_reason',
          'category',
        ];
        const columnIndex = search?.columnIndex ? search.columnIndex : 0;
        const sortOrder = search?.sortOrder ? search.sortOrder : 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }
      const result = await this.tNtidtlRepo
        .createQueryBuilder('TReqNtiDtl')
        .select(select)
        .where(whereClause)
        .orderBy(orderClause)
        .groupBy(groupBy)
        .skip((pagination.page - 1) * pagination.limit)
        .take(pagination.limit)
        .getManyAndCount();

      return result;
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginateExecute(
    pagination: PaginationOptions,
    search?: any,
    groupBy?: string,
    select?: string[],
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: [
            'materialDescription',
            'submitedQty',
            'remainingQty',
            'uom',
            'ntiReason',
            'scrapReason',
            'category',
          ],
          number: ['ntiId', 'materialId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = [
          'material_id',
          'material_description',
          'submitted_qty',
          'remaining_qty',
          'uom',
          'nti_reason',
          'scrap_reason',
          'category',
        ];
        const columnIndex = search?.columnIndex ? search.columnIndex : 0;
        const sortOrder = search?.sortOrder ? search.sortOrder : 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }
      const result = await this.tNtidtlRepo
        .createQueryBuilder('TReqNtiDtl')
        .select(select)
        // .select(['TReqNtiDtl.materialId'])
        .addSelect((db) => {
          const hasil = db
            .subQuery()
            .select('sum(trndtl.submitted_qty) as total')
            .from(TReqNtiDtl, 'trndtl')
            .where('trndtl.materialId = TReqNtiDtl.materialId')
            .andWhere('trndtl.ntiReason = TReqNtiDtl.ntiReason')
            .andWhere('trndtl.scrapReason = TReqNtiDtl.scrap_reason');

          return hasil;
        }, 'submitted_qty')
        .where(whereClause)
        .orderBy(orderClause)
        .groupBy(groupBy)
        .skip((pagination.page - 1) * pagination.limit)
        .take(pagination.limit)
        .getManyAndCount();
      console.log(result);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginateExecuteManual(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let sql = '';
      let wherePlant = '';
      if (search?.plant) {
        wherePlant = `AND trnd2.plant = '${search?.plant}' `;
      }
      sql = `select
      (
        select trnd2.nti_id as "ntiId"
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.category = trnd.category
        and trnd2.uom = trnd.uom
        AND trnd2.status_request != 'Done Execution'
        AND trnd2.status_request != 'Done Complete'
        ${wherePlant}
        ORDER BY trnd2.request_id DESC
        limit 1
      ) as "ntiId",
      trnd.material_id as "materialId",
      trnd.material_description as "materialDescription",
      trnd.nti_reason as "ntiReason",
      trnd.scrap_reason as "scrapReason",
      trnd.category,
      trnd.uom,
      (
        select sum(trnd2.submitted_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
        --AND (
        --  select initial_status from t_req_nti_hdr trndhdr
        --  where trndhdr.request_id = trnd2.request_id
        -- ) != 'D'
        AND trnd2.status != 'Reject'
        AND trnd2.status_request != 'Done Execution'
        AND trnd2.status_request != 'Done Complete'
        ${wherePlant}
       ) as "submittedQty",
       (
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
        AND trnd2.status != 'Reject'
        AND trnd2.status_request != 'Done Execution'
        AND trnd2.status_request != 'Done Complete'
        ${wherePlant}
       ) as "remainingQty",
       trnd.reason_id as "reasonId"
       from t_req_nti_dtl trnd
       `;
      const sqlWhere = `(
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
        ${wherePlant}
       ) != 0 
       OR 
       (
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
        ${wherePlant}
       ) != NULL `;
      sql += `WHERE ${sqlWhere} `;
      if (search?.genSearch) {
        sql += `AND trnd.material_id = '${search.genSearch}' `;
        sql += `OR ${sqlWhere} AND trnd.material_description like '%${search.genSearch}%' `;
        sql += `OR ${sqlWhere} AND trnd.nti_reason like '%${search.genSearch}%' `;
        sql += `OR ${sqlWhere} AND trnd.scrap_reason like '%${search.genSearch}%' `;
        sql += `OR ${sqlWhere} AND trnd.category like '%${search.genSearch}%' `;
        sql += `OR ${sqlWhere} AND trnd.uom like '%${search.genSearch}%' `;
        sql += `OR ${sqlWhere} AND (
          select sum(trnd2.submitted_qty) as total
          from t_req_nti_dtl trnd2 
          where trnd2.material_id = trnd.material_id 
          and trnd2.nti_reason = trnd.nti_reason
          and trnd2.scrap_reason = trnd.scrap_reason
         ) like '%${search.genSearch}%' `;
        sql += `OR ${sqlWhere} AND (
          select sum(trnd2.remaining_qty) as total
          from t_req_nti_dtl trnd2 
          where trnd2.material_id = trnd.material_id 
          and trnd2.nti_reason = trnd.nti_reason
          and trnd2.scrap_reason = trnd.scrap_reason
         ) like '%${search.genSearch}%' `;
      }
      if (search?.materialId) {
        sql += `AND trnd.material_id = '${search.materialId}' `;
      }
      if (search?.materialDescription) {
        sql += `AND trnd.material_description like '%${search.materialDescription}%' `;
      }
      if (search?.ntiReason) {
        sql += `AND trnd.nti_reason like '%${search.ntiReason}%' `;
      }
      if (search?.scrapReason) {
        sql += `AND trnd.scrap_reason like '%${search.scrapReason}%' `;
      }
      if (search?.category) {
        sql += `AND trnd.category like '%${search.category}%' `;
      }
      if (search?.uom) {
        sql += `AND trnd.uom like '%${search.uom}%' `;
      }
      if (search?.submitedQty) {
        sql += `OR ${sqlWhere} AND (
          select sum(trnd2.submitted_qty) as total
          from t_req_nti_dtl trnd2 
          where trnd2.material_id = trnd.material_id 
          and trnd2.nti_reason = trnd.nti_reason
          and trnd2.scrap_reason = trnd.scrap_reason
          ${wherePlant}
         ) like '%${search.submitedQty}%' `;
      }
      if (search?.remainingQty) {
        sql += `OR ${sqlWhere} AND (
          select sum(trnd2.remaining_qty) as total
          from t_req_nti_dtl trnd2 
          where trnd2.material_id = trnd.material_id 
          and trnd2.nti_reason = trnd.nti_reason
          and trnd2.scrap_reason = trnd.scrap_reason
          ${wherePlant}
         ) like '%${search.remainingQty}%' `;
      }
      sql += `group by (trnd.material_id,
        trnd.material_description,
        trnd.nti_reason,
        trnd.reason_id,
        trnd.scrap_reason,
        trnd.category,
        trnd.uom
        )`;

      const columnIndex = search?.columnIndex ? search.columnIndex : 0;
      const sortOrder = search?.sortOrder ? search.sortOrder : 'ASC';
      const columnNames = [
        'trnd.material_id',
        'trnd.material_description',
        'submittedQty',
        'remainingQty',
        'trnd.nti_reason',
        'uom',
        'nti_reason',
        'scrap_reason',
        'category',
      ];
      sql += `ORDER BY ${[columnNames[columnIndex]]} ${sortOrder} `;
      if (pagination.limit) {
        sql += `LIMIT ${pagination.limit} `;
      }
      if (pagination.page > 1) {
        sql += `OFFSET ${(pagination.page - 1) * pagination.limit} `;
      }
      const result = await this.tNtidtlRepo.query(sql);

      const total = await this.tNtidtlRepo.query(
        `SELECT
        trnd.material_id as "materialId",
        trnd.material_description as "materialDescription",
        trnd.nti_reason as "ntiReason",
        trnd.scrap_reason as "scrapReason",
        trnd.category 
        FROM "t_req_nti_dtl" "trnd" WHERE ${sqlWhere} 
        group by (trnd.material_id,
          trnd.material_description,
          trnd.nti_reason,
          trnd.scrap_reason,
          trnd.category,
          trnd.uom
          ) `,
      );

      return [result, total.length];
    } catch (error) {
      throw error;
    }
  }

  async findOneExecuteManual(search?: any): Promise<any> {
    try {
      let sql = '';
      sql = `select
      (
        select trnd2.nti_id as "ntiId"
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.category = trnd.category
        and trnd2.uom = trnd.uom
        limit 1
      ) as "ntiId",
      trnd.material_id as "materialId",
      trnd.material_description as "materialDescription",
      trnd.nti_reason as "ntiReason",
      trnd.scrap_reason as "scrapReason",
      trnd.category,
      trnd.uom,
      (
        select sum(trnd2.submitted_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
       ) as "submittedQty",
       (
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
       ) as "remainingQty",
       trnd.reason_id as "reasonId"
       from t_req_nti_dtl trnd
       `;
      const sqlWhere = `(
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
       ) != 0 
       OR 
       (
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.status in ('Approved', 'Partial')
       ) != NULL `;
      sql += `WHERE ${sqlWhere} `;
      if (search?.materialId) {
        sql += `AND trnd.material_id = '${search.materialId}' `;
      }
      if (search?.materialDescription) {
        sql += `AND trnd.material_description like '%${search.materialDescription}%' `;
      }
      if (search?.ntiReason) {
        sql += `AND trnd.nti_reason like '%${search.ntiReason}%' `;
      }
      if (search?.scrapReason) {
        sql += `AND trnd.scrap_reason like '%${search.scrapReason}%' `;
      }
      if (search?.category) {
        sql += `AND trnd.category like '%${search.category}%' `;
      }
      if (search?.uom) {
        sql += `AND trnd.uom like '%${search.uom}%' `;
      }
      if (search?.submitedQty) {
        sql += `OR ${sqlWhere} AND (
          select sum(trnd2.submitted_qty) as total
          from t_req_nti_dtl trnd2 
          where trnd2.material_id = trnd.material_id 
          and trnd2.nti_reason = trnd.nti_reason
          and trnd2.scrap_reason = trnd.scrap_reason
         ) like '%${search.submitedQty}%' `;
      }
      if (search?.remainingQty) {
        sql += `OR ${sqlWhere} AND (
          select sum(trnd2.remaining_qty) as total
          from t_req_nti_dtl trnd2 
          where trnd2.material_id = trnd.material_id 
          and trnd2.nti_reason = trnd.nti_reason
          and trnd2.scrap_reason = trnd.scrap_reason
         ) like '%${search.remainingQty}%' `;
      }
      sql += `group by (trnd.material_id,
        trnd.material_description,
        trnd.nti_reason,
        trnd.reason_id,
        trnd.scrap_reason,
        trnd.category,
        trnd.uom
        )`;
      sql += `LIMIT 1 `;
      const result = await this.tNtidtlRepo.query(sql);

      return result;
    } catch (error) {
      throw error;
    }
  }

  async totalAfterUpdate(search?: any): Promise<any> {
    try {
      let sql = '';
      sql = `select 
      (
        select trnd2.nti_id as "ntiId"
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
        and trnd2.category = trnd.category
        and trnd2.uom = trnd.uom
        limit 1
      ) as "ntiId",
      trnd.material_id as "materialId",
      trnd.material_description as "materialDescription",
      trnd.nti_reason as "ntiReason",
      trnd.scrap_reason as "scrapReason",
      trnd.category,
      trnd.uom,
      (
        select sum(trnd2.submitted_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
       ) as "submittedQty",
       (
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
       ) as "remainingQty"
       from t_req_nti_dtl trnd
       `;
      const sqlWhere = `(
        select sum(trnd2.remaining_qty) as total
        from t_req_nti_dtl trnd2 
        where trnd2.material_id = trnd.material_id 
        and trnd2.nti_reason = trnd.nti_reason
        and trnd2.scrap_reason = trnd.scrap_reason
       ) != 0`;
      sql += `WHERE ${sqlWhere} `;
      if (search?.materialId) {
        sql += `AND trnd.material_id = '${search.materialId}' `;
      }
      if (search?.materialDescription) {
        sql += `AND trnd.material_description = '${search.materialDescription}' `;
      }
      if (search?.ntiReason) {
        sql += `AND trnd.nti_reason = '${search.ntiReason}' `;
      }
      if (search?.scrapReason) {
        sql += `AND trnd.scrap_reason = '${search.scrapReason}' `;
      }
      if (search?.category) {
        sql += `AND trnd.category = '${search.category}' `;
      }
      if (search?.uom) {
        sql += `AND trnd.uom = '${search.uom}' `;
      }
      sql += `group by (trnd.material_id,
        trnd.material_description,
        trnd.nti_reason,
        trnd.scrap_reason,
        trnd.category,
        trnd.uom
        )`;
      sql += `LIMIT 1 `;
      const result = await this.tNtidtlRepo.query(sql);

      return result;
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginateDetailOutstanding(
    pagination: PaginationOptions,
    search?: any,
    groupBy?: string,
    select?: string[],
  ): Promise<any> {
    try {
      let whereClause: any;
      let orWhereClause: any;
      const andWhereClause = {};
      let andWhereClausesubmittedQty = {};
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          stringMust: [],
          string: [
            'materialDescription',
            'uom',
            'ntiReason',
            'scrapReason',
            'category',
            'requestId',
            'costCenter',
            'costCenterDescription',
            'matDoc',
            // 'status',
            // 'createdName',
            // 'createdBy',
          ],
          number: [/*'ntiId',*/ 'materialId', 'remainingQty', 'submittedQty'],
          date: ['createdDate'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        Object.assign(whereClause, {
          status: In(['Created', 'Approved', 'Partial']),
        });

        if (search?.materialId) {
          Object.assign(whereClause, {
            materialId: Raw(
              (alias) =>
                `cast(${alias} as text) like '%${search?.materialId}%'`,
            ),
          });
        }

        const columnNames = [
          'request_id',
          'plant',
          'posting_date',
          'created_by',
          'status',
          'material_id',
          'material_description',
          'submitted_qty',
          'remaining_qty',
          'confirm_qty',
          'uom',
          'nti_reason',
          'scrap_reason',
          'category',
          'cost_center',
          'cost_center_description',
          'mat_doc',
        ];
        const columnIndex = search?.columnIndex ? search.columnIndex : 0;
        const sortOrder = search?.sortOrder ? search.sortOrder : 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });

        // orWhereClause = whereClause;
        orWhereClause = {};
        if (Array.isArray(whereClause)) {
          orWhereClause = whereClause;
        } else {
          Object.assign(orWhereClause, whereClause);
        }
        if (search?.createdBy) {
          Object.assign(whereClause, {
            createdName: Like(`%${search?.createdBy}%`),
          });

          Object.assign(orWhereClause, {
            createdBy: Like(`%${search?.createdBy}%`),
          });
        }

        if (search?.status) {
          Object.assign(andWhereClause, {
            status: Like(`%${search?.status}%`),
          });
        }

        if (search?.submittedQty) {
          andWhereClausesubmittedQty = `cast(TReqNtiDtl.submittedQty as text) like '%${search?.submittedQty}%' `;
        }
        if (search?.remainingQty) {
          const whereRemainingQty = `cast(TReqNtiDtl.remainingQty as text) like '%${search?.remainingQty}%'`;
          andWhereClausesubmittedQty =
            andWhereClausesubmittedQty &&
            typeof andWhereClausesubmittedQty != 'object'
              ? andWhereClausesubmittedQty + ' AND ' + whereRemainingQty
              : whereRemainingQty;
        }
        if (search?.confirmQty) {
          const whereConfirmQty = `cast(TReqNtiDtl.confirmQty as text) like '%${search?.confirmQty}%'`;
          andWhereClausesubmittedQty =
            andWhereClausesubmittedQty &&
            typeof andWhereClausesubmittedQty != 'object'
              ? andWhereClausesubmittedQty + ' AND ' + whereConfirmQty
              : whereConfirmQty;
        }
        console.log(
          '🚀 ~ TReqNtiDtlService ~ andWhereClausesubmittedQty:',
          andWhereClausesubmittedQty,
        );
      }
      console.log('🚀 ~ TReqNtiDtlService ~ whereClause:', whereClause);
      console.log('🚀 ~ TReqNtiDtlService ~ orWhereClause:', orWhereClause);

      const result = await this.tNtidtlRepo
        .createQueryBuilder('TReqNtiDtl')
        .select(select)
        .where(whereClause)
        .andWhere(andWhereClause)
        .andWhere(andWhereClausesubmittedQty)
        .orWhere(orWhereClause)
        .andWhere(andWhereClause)
        .andWhere(andWhereClausesubmittedQty)
        .orderBy(orderClause)
        .groupBy(groupBy)
        .skip((pagination.page - 1) * pagination.limit)
        .take(pagination.limit)
        .getManyAndCount();

      return result;
    } catch (error) {
      throw error;
    }
  }

  async listRequestIdExecute(
    whereCondition: FindOptionsWhere<TReqNtiDtl>,
  ): Promise<any> {
    try {
      let sql = `select trnd.request_id, trnd.material_id, trnd.material_description 
      from t_req_nti_hdr trnh 
      join t_req_nti_dtl trnd on trnd.request_id = trnh.request_id 
      where trnh.initial_status != 'D' `;
      sql += `and trnd.material_id = '${whereCondition.materialId}' `;
      sql += `and trnd.nti_reason = '${whereCondition.ntiReason}' `;
      sql += `and trnd.scrap_reason = '${whereCondition.scrapReason}' `;
      sql += `and trnd.material_description = '${whereCondition.materialDescription}' `;
      sql += `group by trnd.request_id, trnd.material_id, trnd.material_description`;
      const data = await this.tNtidtlRepo.query(sql);

      if (!data) {
        throw new NotFoundException('Data t_nti_dtl not found');
      }

      return data;
    } catch (error) {
      throw error;
    }
  }
}
