import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In, ILike } from 'typeorm';
import { MType } from './entities/mtype.entity';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { BaseMTypeDto } from './dtos/base-mtype.dto';
import { CreateMTypeDto } from './dtos/create-mtype.dto';
import { Response } from 'src/common/helpers/response.helper';
import { UpdateMTypeDto } from './dtos/update-mtype.dto';
import {
  addWhereClause,
  generateWhereClauseByAttr,
} from 'src/common/helpers/global.helper';

@Injectable()
export class MTypeService {
  constructor(@InjectRepository(MType) private mTypeRepo: Repository<MType>) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause = {};
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: ['typeCode', 'typeDesc'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = ['typeCode', 'typeDesc'];
        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const results = await this.mTypeRepo.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        // skip: pagination.skip
        skip: (pagination.page - 1) * pagination.limit, //use (page-1) * limit
        select: [
          'typeCode',
          'typeDesc',
          'createdOn',
          'createdBy',
          'changedOn',
          'changedBy',
        ],
      });
      return results;
    } catch (error) {
      throw error;
    }
  }

  async findOne(typeCode: string): Promise<BaseMTypeDto> {
    try {
      const data = await this.mTypeRepo.findOne({
        where: { typeCode },
      });
      if (!data) throw new NotFoundException('data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async create(createFlowProcessDto: CreateMTypeDto): Promise<any> {
    try {
      const inserted = await this.mTypeRepo
        .createQueryBuilder()
        .insert()
        .values(createFlowProcessDto)
        .returning('*')
        .execute();
      // return  { message: 'Data saved',  data: [] };
      const returned = { message: 'Data saved', raw: inserted?.generatedMaps };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async update(typeCode: string, updateMTypeDto: UpdateMTypeDto): Promise<any> {
    try {
      const updated = await this.mTypeRepo
        .createQueryBuilder()
        .update()
        .set({ ...updateMTypeDto })
        .whereEntity({ typeCode } as MType)
        .returning('*')
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async delete(typeCode: string): Promise<any> {
    try {
      const deleted = await this.mTypeRepo
        .createQueryBuilder()
        .delete()
        .whereInIds(typeCode)
        .execute();
      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      // const returned = { message: 'Data deleted',  data: [] };
      // return returned;
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
