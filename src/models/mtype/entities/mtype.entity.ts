import { Entity, PrimaryColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity('m_type')
export class MType {
    @PrimaryColumn({
        name: 'type_code',
        type: 'varchar',
        length: 3,
        default: '',
        nullable: false
    })
    typeCode: string;

    @Column({
        name: 'type_desc',
        type: 'varchar',
        length: 100,
        default: '',
        nullable: false
    })
    typeDesc: string;

    @CreateDateColumn({ name: 'created_on', type: "timestamptz",  default: () => 'NOW()' })
    public createdOn: Date;

    @Column({ 
        name: 'created_by', 
        type: 'varchar',
        length: 20,
        default: '',
        nullable: true
    })
    public createdBy: string;

    @UpdateDateColumn({ name: 'changed_on', type: "timestamptz", onUpdate: "CURRENT_TIMESTAMP(6)" })
    public changedOn: Date;

    @Column({ 
        name: 'changed_by', 
        type: 'varchar',
        length: 20,
        default: '',
        nullable: false
    })
    public changedBy: string;
}
