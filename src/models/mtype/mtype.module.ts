import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MTypeService } from './mtype.service';
import { MTypeController } from './mtype.controller';
import { MType } from './entities/mtype.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MType])],
  providers: [MTypeService],
  controllers: [MTypeController],
  exports: [MTypeService],
})
export class MTypeModule {}
