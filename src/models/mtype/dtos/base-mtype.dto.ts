import { IsString, IsNumber } from "class-validator";

export class BaseMTypeDto {
    @IsString()
    typeCode: string;

    @IsString()
    typeDesc: string;
}