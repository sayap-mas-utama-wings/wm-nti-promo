import { PartialType } from "@nestjs/mapped-types";
import { BaseMTypeDto } from "./base-mtype.dto"; 

export class UpdateMTypeDto extends PartialType(BaseMTypeDto) {
    changedBy: string;
    changedOn: string;
}
