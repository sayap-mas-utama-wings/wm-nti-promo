import { BaseMTypeDto } from "./base-mtype.dto";

export class CreateMTypeDto extends BaseMTypeDto {
    createdBy: string;
    createdOn: string;
}
