import { Controller, Get, Post, Body, Put, Param, Delete, Req, HttpStatus, UseInterceptors } from '@nestjs/common';
import { MTypeService } from './mtype.service';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import { response, responseError, responsePage } from 'src/common/helpers/response.helper';
import { CreateMTypeDto } from './dtos/create-mtype.dto';
import { UpdateMTypeDto } from './dtos/update-mtype.dto';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';

@Controller('mtype')
@UseInterceptors(TransformInterceptor)
export class MTypeController {
    constructor(private MTypeService: MTypeService) {}

    @Get()
    async index(@Req() req) {
        try {
            const pagination = createPaginationOptions(req);
            const [results, total] = await this.MTypeService.findAndPaginate(pagination, req.query);
            return responsePage(results, total, pagination);
        } catch (error) {
            return responseError(error.message, error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    
    @Get(':typeCode')
    async find(@Param('typeCode') typeCode: string) {
        try {
            return await this.MTypeService.findOne(typeCode);
        } catch (error) {
            console.log(error, 'catch')
            return responseError(error.message, error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Post()
    async create(@Body() createMTypeDto: CreateMTypeDto) {
        try {
            return await this.MTypeService.create(createMTypeDto);
        } catch (error) {
            let message =  error.message;
            console.log(error);
            if(error?.code == '23505') {
                message = 'Data already exists!';
            }
            return responseError(message, error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Put(':typeCode')
    async update(@Param('typeCode') typeCode: string, @Body() updateMTypeDto: UpdateMTypeDto) {
        try {
            return await this.MTypeService.update(typeCode, updateMTypeDto);
        } catch (error) {
            return responseError(error.message, error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Delete(':typeCode')
    async delete(@Param('typeCode') typeCode: string) {
        try {
            return await this.MTypeService.delete(typeCode);
        } catch (error) {
            return responseError(error.message, error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
