import { TypeOrmModule } from '@nestjs/typeorm';
import { MSettingService } from './msetting.service';
import { Module } from '@nestjs/common';
import { MSetting } from './entities/msetting';
import { MSettingController } from './msetting.controller';

@Module({
  imports: [TypeOrmModule.forFeature([MSetting])],
  controllers: [MSettingController],
  providers: [MSettingService],
  exports: [MSettingService],
})
export class MSettingModule {}
