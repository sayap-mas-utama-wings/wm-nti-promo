import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOptionsWhere, Repository } from 'typeorm';
import { MSetting } from './entities/msetting';

@Injectable()
export class MSettingService {
  constructor(
    @InjectRepository(MSetting) private mSettingRepo: Repository<MSetting>,
  ) {}

  async findOne(
    whereCondition: FindOptionsWhere<MSetting>,
    hideResponse?: boolean,
  ): Promise<MSetting> {
    try {
      const data = await this.mSettingRepo.findOne({
        where: whereCondition,
      });

      if (!hideResponse && !data)
        throw new NotFoundException('Data MSetting not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    whereCondition: FindOptionsWhere<MSetting>,
  ): Promise<MSetting[]> {
    try {
      const data = await this.mSettingRepo.find({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
