import { Controller, Get, HttpStatus, Param, Req } from '@nestjs/common';
import { MSettingService } from './msetting.service';
import { responseError } from 'src/common/helpers/response.helper';
import { MSetting } from './entities/msetting';

@Controller('msetting')
export class MSettingController {
  constructor(private mSettingService: MSettingService) {}

  @Get('findAll/:description')
  async findAll(
    @Param('description') description: string,
  ): Promise<MSetting[]> {
    try {
      const whereClause = {
        settingName: description,
      };
      return await this.mSettingService.findAll(whereClause);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async findOne(@Req() req): Promise<MSetting> {
    try {
      return await this.mSettingService.findOne(req.query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
