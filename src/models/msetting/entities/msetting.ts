import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('m_setting')
export class MSetting {
  @PrimaryColumn({
    name: 'setting_name',
    type: 'varchar',
    // length: 4,
    default: '',
    nullable: false,
  })
  settingName: string;

  @PrimaryColumn({
    name: 'item',
    type: 'int2',
    // length: 4,
  })
  item: number;

  @PrimaryColumn({
    name: 'descr',
    type: 'varchar',
    length: 50,
    default: '',
    nullable: false,
  })
  descr: string;

  @PrimaryColumn({
    name: 'val',
    type: 'varchar',
    default: 50,
    nullable: false,
  })
  val: string;

  @CreateDateColumn({
    name: 'created_date',
    type: 'date',
    default: () => 'NOW()::date',
  })
  public createdOn: Date;

  @CreateDateColumn({
    name: 'created_time',
    type: 'date',
    default: () => 'NOW()::time',
  })
  createdTime: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @UpdateDateColumn({
    name: 'changed_date',
    type: 'date',
    onUpdate: 'NOW()',
  })
  public changedOn: Date;

  @UpdateDateColumn({
    name: 'changed_time',
    type: 'date',
    onUpdate: 'NOW()',
  })
  public changedTime: Date;

  @Column({
    name: 'changed_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public changedBy: string;
}
