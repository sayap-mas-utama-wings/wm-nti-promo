import { IsDateString, IsString } from 'class-validator';
import { BaseTNtiHdrDto } from './base-tntihdr.dto';

export class CreateTNtiHdrDto extends BaseTNtiHdrDto {
  @IsDateString()
  createdDate: string;

  @IsString()
  createdName: string;

  @IsString()
  createdBy: string;
}
