import { IsNumber, IsString } from 'class-validator';

export class BaseTNtiHdrDto {
  @IsNumber()
  ntiId: number;

  @IsNumber()
  requestId: string;

  @IsString()
  plant: string;

  @IsString()
  plantDescription: string;

  @IsString()
  initialStatus: string;

  @IsString()
  reasonReject: string;

  @IsString()
  currentUser: string;
}
