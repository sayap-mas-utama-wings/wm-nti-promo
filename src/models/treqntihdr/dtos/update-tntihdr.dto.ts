import { IsString } from 'class-validator';
import { BaseTNtiHdrDto } from './base-tntihdr.dto';

export class UpdateTNtiHdrDto extends BaseTNtiHdrDto {
  @IsString()
  updatedDate: string;

  @IsString()
  updatedName: string;

  @IsString()
  updatedBy: string;
}
