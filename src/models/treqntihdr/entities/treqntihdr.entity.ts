import { TReqNtiDtl } from 'src/models/treqntidtl/entities/treqntidtl.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('t_req_nti_hdr')
export class TReqNtiHdr {
  @PrimaryColumn({
    name: 'nti_id',
    type: 'integer',
  })
  ntiId: number;

  @Column({
    name: 'request_id',
    type: 'varchar',
    length: 12,
  })
  requestId: string;

  @Column({
    name: 'plant',
    type: 'varchar',
    length: 4,
  })
  plant: string;

  @Column({
    name: 'plant_description',
    type: 'varchar',
    length: 50,
  })
  plantDescription: string;

  @Column({
    name: 'initial_status',
    type: 'varchar',
    length: 10,
  })
  initialStatus: string;

  @Column({
    name: 'reason_reject',
    type: 'varchar',
    length: 10,
  })
  reasonReject: string;

  @Column({
    name: 'current_user',
    type: 'varchar',
    length: 10,
  })
  currentUser: string;

  @CreateDateColumn({
    name: 'created_date',
    type: 'date',
    default: () => 'NOW()::date',
  })
  public createdDate: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @Column({
    name: 'created_name',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdName: string;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'date',
    onUpdate: 'NOW()',
  })
  public updatedDate: Date;

  @Column({
    name: 'updated_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public updatedBy: string;

  @Column({
    name: 'updated_name',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public updatedName: string;

  public totalMid: string;

  total: number;

  @OneToMany(() => TReqNtiDtl, (treqntidtl) => treqntidtl.tReqNtiHdr)
  @JoinColumn({ name: 'request_id', referencedColumnName: 'requestId' })
  tReqNtiDtl: TReqNtiDtl[];
}
