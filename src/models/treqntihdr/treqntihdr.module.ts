import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TReqNtiHdr } from './entities/treqntihdr.entity';
import { TReqNtiHdrService } from './treqntihdr.service';

@Module({
  imports: [TypeOrmModule.forFeature([TReqNtiHdr])],
  controllers: [],
  providers: [TReqNtiHdrService],
  exports: [TReqNtiHdrService],
})
export class TReqNtiHdrModule {}
