import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TReqNtiHdr } from './entities/treqntihdr.entity';
import {
  EntityManager,
  FindOptionsWhere,
  In,
  InsertResult,
  Repository,
} from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { CreateTNtiHdrDto } from './dtos/create-tntihdr.dto';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { UpdateTNtiHdrDto } from './dtos/update-tntihdr.dto';

@Injectable()
export class TReqNtiHdrService {
  constructor(
    @InjectRepository(TReqNtiHdr) private tntiHdrRepo: Repository<TReqNtiHdr>,
  ) {}

  async create(
    createDto: QueryDeepPartialEntity<CreateTNtiHdrDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }
      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(TReqNtiHdr)
          .values(createDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.tntiHdrRepo
          .createQueryBuilder()
          .insert()
          .values(createDto)
          .returning(returning)
          .execute();
      }

      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(
    requestId: string,
    updateDto: QueryDeepPartialEntity<UpdateTNtiHdrDto>,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      const repositoryUpdate = transactionManager
        ? transactionManager
        : this.tntiHdrRepo;

      const updated = await repositoryUpdate
        .createQueryBuilder()
        .update(TReqNtiHdr)
        .set({ ...updateDto })
        .where('request_id = :requestId', {
          requestId,
        })
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async selectMaxId(): Promise<any> {
    try {
      const result = await this.tntiHdrRepo
        .createQueryBuilder('t_req_nti_hdr')
        .select('MAX(t_req_nti_hdr.nti_id)', 'maxId')
        .getRawOne();

      if (!result.maxId) {
        return 1;
      } else {
        return result.maxId;
      }
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    whereCondition: FindOptionsWhere<TReqNtiHdr>,
    select?: string[],
    groupBy?: string,
  ): Promise<any> {
    try {
      return await this.tntiHdrRepo
        .createQueryBuilder()
        .distinct(true)
        .select(select)
        .where(whereCondition)
        .groupBy(groupBy)
        .getRawMany();
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
    requestId?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: [
            'requestId',
            'plant',
            'plantDescription',
            // 'nama',
            // 'email',
            // 'jabatan',
            // 'initialStatus',
          ],
          number: ['ntiId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        if (requestId && requestId.length > 0) {
          // whereClause.push({
          //   requestId: In(requestId),
          // });
          whereClause['requestId'] = In(requestId);
          // whereClause['initialStatus'] = 'W';
        }
        Object.assign(whereClause);
        console.log('🚀 ~ TReqNtiHdrService ~ whereClause:', whereClause);

        const columnNames = [
          // 'tReqNtiHdr.ntiId',
          'tReqNtiHdr.requestId',
          'tReqNtiHdr.plant',
          // 'tReqNtiHdr.nama',
          // 'tReqNtiHdr.email',
          // 'tReqNtiHdr.jabatan',
          'tReqNtiHdr.plantDescription',
          'tReqNtiHdr.initialStatus',
          'totalMid',
        ];

        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.tntiHdrRepo
        .createQueryBuilder('tReqNtiHdr')
        .innerJoinAndSelect('tReqNtiHdr.tReqNtiDtl', 'tReqNtiDtl')
        .select([
          'tReqNtiHdr.ntiId',
          'tReqNtiHdr.requestId',
          'tReqNtiHdr.plant',
          'tReqNtiHdr.plantDescription',
          'tReqNtiHdr.createdBy',
          'tReqNtiHdr.createdDate',
          'tReqNtiHdr.createdName',
          'tReqNtiDtl.ntiId',
          'tReqNtiDtl.materialId',
        ])
        // .addSelect('COUNT(tReqNtiDtl.ntiId) AS "tReqNtiHdr_total"')
        .where(whereClause)
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .addGroupBy('tReqNtiHdr.ntiId')
        .addGroupBy('tReqNtiHdr.requestId')
        .addGroupBy('tReqNtiHdr.plant')
        .addGroupBy('tReqNtiHdr.plantDescription')
        .addGroupBy('tReqNtiHdr.createdBy')
        .addGroupBy('tReqNtiHdr.createdDate')
        .addGroupBy('tReqNtiDtl.ntiId')
        .addGroupBy('tReqNtiDtl.materialId')
        .getManyAndCount();

      console.log('🚀 ~ TReqNtiHdrService ~ result:', result);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async findOne(whereCondition: FindOptionsWhere<TReqNtiHdr>): Promise<any> {
    try {
      const data = await this.tntiHdrRepo.findOne({
        where: whereCondition,
      });
      if (!data) {
        throw new NotFoundException('Data t_nti_hdr not found');
      }
      return data;
    } catch (error) {
      throw error;
    }
  }

  async findOutstanding(
    whereCondition: FindOptionsWhere<TReqNtiHdr>,
    select?: string[],
  ): Promise<any> {
    try {
      const result = await this.tntiHdrRepo
        .createQueryBuilder()
        .select(select)
        .where({
          plant: whereCondition.plant,
        });

      if (whereCondition?.requestId) {
        result.andWhere(
          `cast(TReqNtiHdr.requestId as text) like '%${whereCondition?.requestId}%'`,
        );
      }

      return result.getRawMany();
    } catch (error) {
      throw error;
    }
  }
}
