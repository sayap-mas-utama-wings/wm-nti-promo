import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TLogNti } from './entities/tlognti';
import {
  Between,
  EntityManager,
  FindOptionsWhere,
  InsertResult,
  Like,
  Repository,
} from 'typeorm';
import { CreateTlogNtiDto } from './dtos/create-tlognti.dto';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';

@Injectable()
export class TLogNtiService {
  constructor(
    @InjectRepository(TLogNti) private tLogNtiRepo: Repository<TLogNti>,
  ) {}

  async create(
    createDto: QueryDeepPartialEntity<CreateTlogNtiDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }
      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(TLogNti)
          .values(createDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.tLogNtiRepo
          .createQueryBuilder()
          .insert()
          .values(createDto)
          .returning(returning)
          .execute();
      }

      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async findAndPagination(
    pagination: PaginationOptions,
    search?: any,
    select?: string[],
    orderColumns?: string[],
  ): Promise<any> {
    try {
      let whereClause: any;
      const whereClause2: any = {};
      const whereClauseOr = {};
      const orderClause = {};
      let andWhereClause = {};

      if (search) {
        // const number = [];
        // if (search?.genSearch.length <= 4) {
        //   number.push(['ntiId']);
        // } else if (search?.genSearch.length <= 8) {
        //   number.push([
        //     'submittedQty',
        //     'remainingQty',
        //     'confirmQty',
        //     'materialId',
        //   ]);
        // }

        const objSearchAttr = {
          stringMust: ['plant', 'requestId', 'costCenter', 'status'],
          string: [
            'plantDescription',
            // 'createdDate',
            'createdBy',
            'materialDescription',
            'uom',
            'ntiReason',
            'scrapReason',
            'category',
            'costCenterDescription',
            'matDoc',
            'reasonReject',
            'reasonComplete',
            'materialDescription',
          ],
          // number: number,
          number: [
            // 'ntiId',
            'materialId',
            'submittedQty',
            'remainingQty',
            // 'confirmQty',
          ],
          date: ['createdDate'],
        };

        console.log('🚀 ~ TLogNtiService ~ search before:', search);
        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        console.log('🚀 ~ TLogNtiService ~ search after:', search);
        console.log('🚀 ~ TLogNtiService ~ whereClause:', whereClause);

        Object.assign(whereClause);

        const columnNames = [
          'tLogNtiRepo.requestId',
          'tLogNtiRepo.plant',
          'tLogNtiRepo.createdDate',
          'tLogNtiRepo.createdBy',
          'tLogNtiRepo.status',
          'tLogNtiRepo.materialId',
          'tLogNtiRepo.materialDescription',
          'tLogNtiRepo.submittedQty',
          'tLogNtiRepo.remainingQty',
          'tLogNtiRepo.confirmQty',
          'tLogNtiRepo.uom',
          'tLogNtiRepo.ntiReason',
          'tLogNtiRepo.scrapReason',
          'tLogNtiRepo.category',
          'tLogNtiRepo.costCenter',
          'tLogNtiRepo.costCenterDescription',
          'tLogNtiRepo.matDoc',
          'tLogNtiRepo.reasonReject',
          'tLogNtiRepo.reasonComplete',
        ];
        console.log('🚀 ~ TLogNtiService ~ select:', select);
        console.log('🚀 ~ TLogNtiService ~ orderColumns:', orderColumns);

        const columnIndex = search?.columnIndex ? search.columnIndex : 2;
        const sortOrder = search?.sortOrder ? search?.sortOrder : 'ASC';

        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });

        if (search?.plant2) {
          Object.assign(whereClause2, {
            plant: Like(`%${search.plant2}%`),
          });
          Object.assign(whereClauseOr, {
            plantDescription: Like(`%${search.plant2}%`),
          });
        }

        if (search?.createdBy) {
          console.log('🚀 ~ TLogNtiService ~ whereClauseOr:', whereClauseOr);
          Object.assign(whereClauseOr, {
            createdName: Like(`%${search.createdBy}%`),
          });
        }

        if (search?.createdDateStart) {
          const endDate = search?.createdDateEnd
            ? search?.createdDateEnd
            : new Date().toISOString().split('T')[0];
          Object.assign(whereClause2, {
            createdDate: Between(
              `${search?.createdDateStart} 00:00:00`,
              `${endDate} 23:59:59`,
            ),
          });
          Object.assign(whereClauseOr, {
            createdDate: Between(
              `${search?.createdDateStart} 00:00:00`,
              `${endDate} 23:59:59`,
            ),
          });
        }

        if (!search?.requestId && search?.requestId2) {
          Object.assign(whereClause2, {
            requestId: Like(`%${search.requestId2}%`),
          });
        }
        if (!search?.costCenter && search?.costCenter2) {
          Object.assign(whereClause2, {
            costCenter: Like(`%${search.costCenter2}%`),
          });
        }
        if (!search?.status && search?.status2) {
          Object.assign(whereClause2, {
            status: Like(`%${search.status2}%`),
          });
        }
        console.log('🚀 ~ TLogNtiService ~ whereClause2:', whereClause2);

        if (search?.submittedQty) {
          andWhereClause = `cast(tLogNtiRepo.submittedQty as text) like '%${search?.submittedQty}%' `;
        }
        if (search?.remainingQty) {
          const whereRemainingQty = `cast(tLogNtiRepo.remainingQty as text) like '%${search?.remainingQty}%'`;
          andWhereClause =
            andWhereClause && typeof andWhereClause != 'object'
              ? andWhereClause + ' AND ' + whereRemainingQty
              : whereRemainingQty;
        }
        if (search?.confirmQty) {
          const whereConfirmQty = `cast(tLogNtiRepo.confirmQty as text) like '%${search?.confirmQty}%'`;
          andWhereClause =
            andWhereClause && typeof andWhereClause != 'object'
              ? andWhereClause + ' AND ' + whereConfirmQty
              : whereConfirmQty;
        }

        if (search?.materialId) {
          const whereRemainingQty = `cast(tLogNtiRepo.materialId as text) like '%${search?.materialId}%'`;
          andWhereClause =
            andWhereClause && typeof andWhereClause != 'object'
              ? andWhereClause + ' AND ' + whereRemainingQty
              : whereRemainingQty;
        }
      }

      const result = await this.tLogNtiRepo
        .createQueryBuilder('tLogNtiRepo')
        .distinct(true)
        .select(select)
        .where(whereClause)
        .andWhere(whereClause2)
        .andWhere(andWhereClause);

      let hasil1;
      if (Object.keys(whereClauseOr).length > 0) {
        delete whereClause['createdBy'];
        hasil1 = await result
          .orWhere(whereClause)
          .andWhere(whereClauseOr)
          // .andWhere(whereClause2)
          .andWhere(andWhereClause);
      } else {
        hasil1 = result;
      }

      const hasil = await hasil1
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .getManyAndCount();

      return hasil;
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    select?: string[],
    groupBy?: string,
    where?: FindOptionsWhere<TLogNti>,
  ): Promise<any> {
    try {
      const data = await this.tLogNtiRepo
        .createQueryBuilder('tLogNti')
        .select(select)
        .where(where)
        .groupBy(groupBy)
        .getRawMany();

      if (!data) throw new NotFoundException('Data t_flow not found!');

      return data;

      // return await this.tLogNtiRepo.find();
    } catch (error) {
      throw error;
    }
  }
}
