import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TLogNti } from './entities/tlognti';
import { TLogNtiService } from './tlognti.service';

@Module({
  imports: [TypeOrmModule.forFeature([TLogNti])],
  controllers: [],
  providers: [TLogNtiService],
  exports: [TLogNtiService],
})
export class TLogNtiModule {}
