import { IsNumber, IsString } from 'class-validator';

export class BaseTlogNtiDto {
  @IsNumber()
  requestId: number;

  @IsString()
  userId: string;

  @IsString()
  userName: string;

  @IsString()
  status: string;

  @IsNumber()
  lineItem: number;

  @IsNumber()
  materialId: number;

  @IsString()
  materialDescription: string;

  @IsNumber()
  submittedQty: number;

  @IsNumber()
  remainingQty: number;

  @IsNumber()
  confirmQty: number;

  @IsNumber()
  confirmInputQty: number;

  @IsString()
  uom: string;

  @IsString()
  ntiReason: string;

  @IsString()
  scrapReason: string;

  @IsString()
  category: string;

  @IsString()
  costCenter: string;

  @IsString()
  costCenterDescription: string;

  @IsString()
  sloc: string;

  @IsString()
  plant: string;

  @IsString()
  plantDescription: string;

  @IsString()
  reasonReject: string;

  @IsString()
  reasonComplete: string;

  @IsString()
  matDoc: string;

  @IsString()
  matDocYear: string;

  @IsString()
  postingDate: string;

  @IsString()
  notes: string;
}
