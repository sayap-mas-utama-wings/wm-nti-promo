import { IsString } from 'class-validator';
import { BaseTlogNtiDto } from './base-tlognti.dto';

export class CreateTlogNtiDto extends BaseTlogNtiDto {
  @IsString()
  createdDate: Date;

  @IsString()
  createdBy: string;

  @IsString()
  createdName: string;
}
