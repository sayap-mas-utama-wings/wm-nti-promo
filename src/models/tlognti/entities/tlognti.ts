import { Column, CreateDateColumn, Entity, PrimaryColumn } from 'typeorm';

@Entity('t_log_nti')
export class TLogNti {
  @PrimaryColumn({
    name: 'nti_id',
    type: 'integer',
  })
  ntiId: number;

  @PrimaryColumn({
    name: 'request_id',
    type: 'int4',
    // length: 3,
  })
  requestId: number;

  @Column({
    name: 'user_id',
    length: 12,
    type: 'varchar',
  })
  userId: string;

  @Column({
    name: 'user_name',
    length: 50,
    type: 'varchar',
  })
  userName: string;

  @Column({
    name: 'status',
    length: 10,
    type: 'varchar',
  })
  status: string;

  @Column({
    name: 'line_item',
    type: 'int',
  })
  lineItem: number;

  @Column({
    name: 'material_id',
    type: 'int8',
  })
  materialId: number;

  @Column({
    name: 'material_description',
    length: 50,
    type: 'varchar',
  })
  materialDescription: string;

  @Column({
    name: 'submitted_qty',
    type: 'float8',
  })
  submittedQty: number;

  @Column({
    name: 'remaining_qty',
    type: 'float8',
  })
  remainingQty: number;

  @Column({
    name: 'confirm_qty',
    type: 'float8',
  })
  confirmQty: number;

  @Column({
    name: 'confirm_input_qty',
    type: 'float8',
  })
  confirmInputQty: number;

  @Column({
    name: 'uom',
    length: 5,
    type: 'varchar',
  })
  uom: string;

  @Column({
    name: 'nti_reason',
    length: 100,
    type: 'varchar',
  })
  ntiReason: string;

  @Column({
    name: 'scrap_reason',
    length: 100,
    type: 'varchar',
  })
  scrapReason: string;

  @Column({
    name: 'category',
    length: 40,
    type: 'varchar',
  })
  category: string;

  @Column({
    name: 'cost_center',
    length: 10,
    type: 'varchar',
  })
  costCenter: string;

  @Column({
    name: 'cost_center_description',
    length: 50,
    type: 'varchar',
  })
  costCenterDescription: string;

  @Column({
    name: 'sloc',
    length: 50,
    type: 'varchar',
  })
  sloc: string;

  @Column({
    name: 'plant',
    type: 'varchar',
    length: 4,
  })
  plant: string;

  @Column({
    name: 'plant_description',
    type: 'varchar',
    length: 50,
  })
  plantDescription: string;

  @Column({
    name: 'reason_reject',
    type: 'varchar',
    length: 50,
  })
  reasonReject: string;

  @Column({
    name: 'reason_complete',
    type: 'varchar',
    length: 100,
  })
  reasonComplete: string;

  @Column({
    name: 'mat_doc',
    type: 'varchar',
    length: 20,
  })
  matDoc: string;

  @Column({
    name: 'mat_doc_year',
    type: 'date',
  })
  matDocYear: string;

  @Column({
    name: 'posting_date',
    type: 'date',
  })
  postingDate: string;

  @Column({
    name: 'notes',
    type: 'varchar',
  })
  notes: string;

  @CreateDateColumn({
    name: 'created_date',
    type: 'date',
    default: () => 'NOW()::date',
  })
  public createdDate: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @Column({
    name: 'created_name',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdName: string;
}
