import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { MNtiReasonService } from './mntireason.service';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import { CreateMNtiReasonDto } from './dtos/create-mntireason.dto';
import { UpdateMNtiReasonDto } from './dtos/update-mntireason.dto';

@Controller('mntireason')
export class MNtiReasonController {
  constructor(private readonly mNtiReasonService: MNtiReasonService) {}

  @Get()
  async findAndPaginate(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mNtiReasonService.findAndPaginate(
        pagination,
        req.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post('createData')
  async create(@Body() createValue: CreateMNtiReasonDto) {
    try {
      if (!createValue.reason) {
        return responseError(
          'reason harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      const checkExisting = await this.mNtiReasonService
        .findOne({
          reason: createValue.reason,
        })
        .catch(() => null);

      if (checkExisting) {
        return responseError(
          'Reason sudah ada',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.mNtiReasonService.create({
        ...createValue,
        createdDate() {
          return 'NOW()';
        },
      });
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put('updateData')
  async update(@Body() updateValue: UpdateMNtiReasonDto) {
    try {
      if (!updateValue.reasonId) {
        return responseError(
          'reasonId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.reason) {
        return responseError(
          'reason harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.mNtiReasonService.update(updateValue);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete('deleteData/:reasonId')
  async delete(@Param('reasonId') reasonId: number) {
    try {
      if (!reasonId) {
        return responseError(
          'reasonId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }
      return this.mNtiReasonService.delete({
        reasonId: reasonId,
      });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
