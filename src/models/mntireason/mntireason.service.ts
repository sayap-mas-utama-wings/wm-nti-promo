import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MNtiReason } from './entities/mntireason.entity';
import {
  DeleteResult,
  EntityManager,
  FindOptionsOrder,
  FindOptionsWhere,
  InsertResult,
  Not,
  Repository,
  UpdateResult,
} from 'typeorm';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { CreateMNtiReasonDto } from './dtos/create-mntireason.dto';
import { UpdateMNtiReasonDto } from './dtos/update-mntireason.dto';

@Injectable()
export class MNtiReasonService {
  constructor(
    @InjectRepository(MNtiReason)
    private mNtiReasonRepo: Repository<MNtiReason>,
  ) {}

  async findOne(
    whereCondition: FindOptionsWhere<MNtiReason>,
    orderCondition?: FindOptionsOrder<MNtiReason>,
  ): Promise<MNtiReason> {
    try {
      const data = await this.mNtiReasonRepo.findOne({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data t_flow not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['reason', 'status'],
          number: ['reasonId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);
        if (search.status && search.status == 'Active') {
          Object.assign(whereClause, {
            status: Not('Inactive'),
          });
        }

        const columnNames = ['reason', 'status'];

        const columnIndex = search?.columnIndex ? search?.columnIndex : 0;
        const sortOrder = search?.sortOrder ? search?.sortOrder : 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.mNtiReasonRepo
        .createQueryBuilder('m_nti_reason')
        .where(whereClause)
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .getManyAndCount();

      return result;
    } catch (error) {
      throw error;
    }
  }

  async create(
    createMNtiReasonDto: QueryDeepPartialEntity<CreateMNtiReasonDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }

      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(MNtiReason)
          .values(createMNtiReasonDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.mNtiReasonRepo
          .createQueryBuilder()
          .insert()
          .values(createMNtiReasonDto)
          .returning(returning)
          .execute();
      }
      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(
    updateMNtiReasonDto: QueryDeepPartialEntity<UpdateMNtiReasonDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    let updated: UpdateResult;
    if (transactionManager) {
      updated = await transactionManager
        .createQueryBuilder()
        .update(MNtiReason)
        .set({
          reason: updateMNtiReasonDto.reason,
          status: updateMNtiReasonDto.status,
          updatedBy: updateMNtiReasonDto.updatedBy,
          updatedDate() {
            return 'now()';
          },
          updatedName: updateMNtiReasonDto.updatedName,
        })
        .where('reason_id = :reasonId', {
          reasonId: updateMNtiReasonDto.reasonId,
        })
        .returning(returning)
        .execute();
    } else {
      updated = await this.mNtiReasonRepo
        .createQueryBuilder()
        .update(MNtiReason)
        .set({
          reason: updateMNtiReasonDto.reason,
          status: updateMNtiReasonDto.status,
          updatedBy: updateMNtiReasonDto.updatedBy,
          updatedDate() {
            return 'now()';
          },
          updatedName: updateMNtiReasonDto.updatedName,
        })
        .where('reason_id = :reasonId', {
          reasonId: updateMNtiReasonDto.reasonId,
        })
        .returning(returning)
        .updateEntity(true)
        .execute();
    }

    if (!updated?.affected) throw new NotFoundException('Update failed!');

    return {
      message: 'Data updated',
      raw: updated?.generatedMaps,
      affected: updated?.affected,
    };
  }

  async delete(
    whereCondition: FindOptionsWhere<MNtiReason>,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      let deleted: DeleteResult;
      if (transactionManager) {
        deleted = await transactionManager
          .createQueryBuilder()
          .delete()
          .from(MNtiReason)
          .where(whereCondition)
          .execute();
      } else {
        deleted = await this.mNtiReasonRepo
          .createQueryBuilder()
          .delete()
          .from(MNtiReason)
          .where(whereCondition)
          .execute();
      }

      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      return {
        message: 'Data deleted',
        raw: deleted?.raw,
        affected: deleted?.affected,
      };
    } catch (error) {
      throw error;
    }
  }
}
