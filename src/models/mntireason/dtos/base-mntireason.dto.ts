import { IsString } from 'class-validator';

export class BaseMNtiReasonDto {
  @IsString()
  reason: string;

  @IsString()
  status: string;
}
