import { IsString } from 'class-validator';
import { BaseMNtiReasonDto } from './base-mntireason.dto';

export class CreateMNtiReasonDto extends BaseMNtiReasonDto {
  @IsString()
  createdBy: string;

  @IsString()
  createdName: string;

  @IsString()
  createdDate: string;
}
