import { IsString } from 'class-validator';
import { BaseMNtiReasonDto } from './base-mntireason.dto';

export class UpdateMNtiReasonDto extends BaseMNtiReasonDto {
  @IsString()
  reasonId: string;

  @IsString()
  updatedBy: string;

  @IsString()
  updatedName: string;

  @IsString()
  updatedDate: string;
}
