import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_nti_reason')
export class MNtiReason {
  @PrimaryColumn({
    name: 'reason_id',
    type: 'int4',
  })
  reasonId: number;

  @Column({
    name: 'reason',
    type: 'varchar',
    length: 100,
  })
  reason: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 10,
  })
  status: string;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 15,
  })
  createdBy: string;

  @Column({
    name: 'created_name',
    type: 'varchar',
    length: 50,
  })
  createdName: string;

  @Column({
    name: 'created_date',
    type: 'timestamp',
    default: null,
    nullable: true,
  })
  createdDate: string;

  @Column({
    name: 'updated_by',
    type: 'varchar',
    length: 15,
  })
  updatedBy: string;

  @Column({
    name: 'updated_name',
    type: 'varchar',
    length: 50,
  })
  updatedName: string;

  @Column({
    name: 'updated_date',
    type: 'timestamp',
    onUpdate: 'NOW()',
  })
  updatedDate: string;
}
