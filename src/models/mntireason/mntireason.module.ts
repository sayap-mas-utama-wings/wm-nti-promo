import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MNtiReason } from './entities/mntireason.entity';
import { MNtiReasonController } from './mntireason.controller';
import { MNtiReasonService } from './mntireason.service';

@Module({
  imports: [TypeOrmModule.forFeature([MNtiReason])],
  controllers: [MNtiReasonController],
  providers: [MNtiReasonService],
  exports: [MNtiReasonService],
})
export class MNtiReasonModule {}
