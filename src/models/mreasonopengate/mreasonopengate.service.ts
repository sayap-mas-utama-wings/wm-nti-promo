import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMReasonOpenGateDto } from './dto/create-mreasonopengate.dto';
import { UpdateMReasonOpenGateDto } from './dto/update-mreasonopengate.dto';
import { FindOptionsWhere, Repository } from 'typeorm';
import { MReasonOpenGate } from './entities/mreasonopengate.entity';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MReasonOpenGateService {
  constructor(
    @InjectRepository(MReasonOpenGate)
    private readonly mReasonOpenGateRepository: Repository<MReasonOpenGate>,
  ) {}

  async findAndPaginate(pagination: PaginationOptions, search: any) {
    try {
      let whereClause: any;
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: ['reasonId', 'reason'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = ['reasonId', 'reason'];
        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const results = await this.mReasonOpenGateRepository.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        skip: (pagination.page - 1) * pagination.limit,
      });

      return results;
    } catch (error) {
      throw error;
    }
  }

  async findList(whereCondition: FindOptionsWhere<MReasonOpenGate>) {
    try {
      const results: MReasonOpenGate[] =
        await this.mReasonOpenGateRepository.find({
          where: whereCondition,
        });

      return results;
    } catch (error) {
      throw error;
    }
  }

  async findOne(whereCondition: FindOptionsWhere<MReasonOpenGate>) {
    try {
      const results: MReasonOpenGate =
        await this.mReasonOpenGateRepository.findOne({
          where: whereCondition,
        });

      return results;
    } catch (error) {
      throw error;
    }
  }

  async create(createMReasonOpenGateDto: CreateMReasonOpenGateDto) {
    try {
      const inserted = await this.mReasonOpenGateRepository
        .createQueryBuilder()
        .insert()
        .values(createMReasonOpenGateDto)
        .returning('*')
        .execute();

      const returned = { message: 'Data saved', raw: inserted?.generatedMaps };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async update(
    reasonId: string,
    updateMReasonOpenGateDto: UpdateMReasonOpenGateDto,
  ) {
    try {
      const updated = await this.mReasonOpenGateRepository
        .createQueryBuilder()
        .update(MReasonOpenGate)
        .set({ ...updateMReasonOpenGateDto })
        .whereEntity({ reasonId } as MReasonOpenGate)
        .returning('*')
        .updateEntity(true)
        .execute();

      if (!updated?.affected) throw new NotFoundException('Update failed!');

      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };

      return returned;
    } catch (error) {
      throw error;
    }
  }

  async delete(reasonId: string) {
    try {
      const deleted = await this.mReasonOpenGateRepository
        .createQueryBuilder()
        .delete()
        .from(MReasonOpenGate)
        .where('reason_id = :reasonId', {
          reasonId,
        })
        .execute();

      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
