import { Module } from '@nestjs/common';
import { MReasonOpenGateService } from './mreasonopengate.service';
import { MReasonOpenGateController } from './mreasonopengate.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MReasonOpenGate } from './entities/mreasonopengate.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MReasonOpenGate])],
  controllers: [MReasonOpenGateController],
  providers: [MReasonOpenGateService],
  exports: [MReasonOpenGateService],
})
export class MReasonOpenGateModule {}
