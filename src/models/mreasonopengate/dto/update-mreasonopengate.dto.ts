import { PartialType } from '@nestjs/mapped-types';
import { BaseMReasonOpenGateDto } from './base-mreasonopengate.dto';
import { IsString } from 'class-validator';

export class UpdateMReasonOpenGateDto extends PartialType(
  BaseMReasonOpenGateDto,
) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
