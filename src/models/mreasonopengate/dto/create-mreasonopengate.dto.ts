import { IsString } from 'class-validator';
import { BaseMReasonOpenGateDto } from './base-mreasonopengate.dto';

export class CreateMReasonOpenGateDto extends BaseMReasonOpenGateDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
