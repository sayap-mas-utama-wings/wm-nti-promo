import { IsString } from 'class-validator';

export class BaseMReasonOpenGateDto {
  @IsString()
  reasonId: string;

  @IsString()
  reason: string;
}
