import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  Req,
  HttpStatus,
  Put,
} from '@nestjs/common';
import { MReasonOpenGateService } from './mreasonopengate.service';
import { CreateMReasonOpenGateDto } from './dto/create-mreasonopengate.dto';
import { UpdateMReasonOpenGateDto } from './dto/update-mreasonopengate.dto';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';

@Controller('mreasonopengate')
@UseInterceptors(TransformInterceptor)
export class MReasonOpenGateController {
  constructor(
    private readonly mReasonOpenGateService: MReasonOpenGateService,
  ) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] =
        await this.mReasonOpenGateService.findAndPaginate(
          pagination,
          req.query,
        );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findList')
  async findList(@Req() req) {
    try {
      const query = req.query;
      return await this.mReasonOpenGateService.findList(query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async findOne(@Req() req) {
    try {
      const query = req.query;
      return await this.mReasonOpenGateService.findOne(query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(@Body() createMReasonOpenGateDto: CreateMReasonOpenGateDto) {
    try {
      return await this.mReasonOpenGateService.create(createMReasonOpenGateDto);
    } catch (error) {
      let message = error.message;
      let statusCode = error?.status;

      if (error?.code == '23505') {
        message = 'Data already exists!';
        statusCode = HttpStatus.CONFLICT;
      }
      return responseError(
        message,
        statusCode ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put(':reasonId')
  async update(
    @Param('reasonId') reasonId: string,
    @Body() updateMReasonOpenGateDto: UpdateMReasonOpenGateDto,
  ) {
    try {
      return await this.mReasonOpenGateService.update(
        reasonId,
        updateMReasonOpenGateDto,
      );
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete(':reasonId')
  remove(@Param('reasonId') reasonId: string) {
    try {
      return this.mReasonOpenGateService.delete(reasonId);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
