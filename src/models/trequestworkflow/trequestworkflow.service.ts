import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TRequestWorkflow } from './entities/trequestworkflow.entity';
import { EntityManager, InsertResult, Repository } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { CreateTRequestRowkflowDto } from './dtos/create-trequestrowkflow.dto';

@Injectable()
export class TRequestWorkflowService {
  constructor(
    @InjectRepository(TRequestWorkflow)
    private tRequestWorkFlowRepo: Repository<TRequestWorkflow>,
  ) {}

  async create(
    createTFlowDto: QueryDeepPartialEntity<CreateTRequestRowkflowDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }
      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(TRequestWorkflow)
          .values(createTFlowDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.tRequestWorkFlowRepo
          .createQueryBuilder()
          .insert()
          .values(createTFlowDto)
          .returning(returning)
          .execute();
      }

      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }
}
