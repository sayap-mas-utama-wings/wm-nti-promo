import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('t_request_workflow')
export class TRequestWorkflow {
  @PrimaryColumn({
    name: 'request_type',
    type: 'varchar',
    length: 3,
  })
  requestType: string;

  @PrimaryColumn({
    name: 'request_id',
    type: 'int4',
    // length: 3,
  })
  requestId: number;

  @PrimaryColumn({
    name: 'seq',
    type: 'varchar',
    length: 1,
  })
  seq: string;

  @Column({
    name: 'no_id',
    type: 'int4',
    // length: 10,
  })
  noId: number;

  @Column({
    name: 'request_name',
    type: 'varchar',
    length: 10,
  })
  requestName: string;

  @Column({
    name: 'plant',
    type: 'varchar',
    length: 4,
  })
  plant: string;

  @Column({
    name: 'transplant',
    type: 'varchar',
    length: 4,
  })
  transplant: string;

  @Column({
    name: 'sloc',
    type: 'varchar',
    length: 4,
  })
  sloc: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 10,
  })
  status: string;

  @Column({
    name: 'current_user',
    type: 'varchar',
    length: 20,
  })
  currentUser: string;

  @Column({
    name: 'current_user_name',
    type: 'varchar',
    length: 50,
  })
  currentUserName: string;

  @Column({
    name: 'notes',
    type: 'varchar',
    // length: 1000,
  })
  notes: string;

  @Column({
    name: 'ticket_no',
    type: 'varchar',
    length: 10,
  })
  ticketNo: string;

  @Column({
    name: 'flow_code',
    type: 'varchar',
    length: 10,
  })
  flowCode: string;

  @Column({
    name: 'bpk',
    type: 'varchar',
    length: 1,
  })
  bpk: string;

  @Column({
    name: 'gate',
    type: 'varchar',
    length: 4,
  })
  gate: string;

  @Column({
    name: 'created_date',
    type: 'date',
    default: null,
    nullable: true,
  })
  createdDate: string;

  @Column({
    name: 'created_time',
    type: 'time',
    default: null,
    nullable: false,
  })
  createdTime: string;

  @Column({
    name: 'changed_date',
    type: 'date',
    default: null,
    nullable: true,
  })
  changedDate: string;

  @Column({
    name: 'changed_time',
    type: 'time',
    default: null,
    nullable: false,
  })
  changed_time: string;
}
