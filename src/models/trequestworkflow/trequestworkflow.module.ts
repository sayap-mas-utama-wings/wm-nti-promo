import { Module } from '@nestjs/common';
import { TRequestWorkflow } from './entities/trequestworkflow.entity';
import { TRequestWorkflowService } from './trequestworkflow.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([TRequestWorkflow])],
  controllers: [],
  providers: [TRequestWorkflowService],
  exports: [TRequestWorkflowService],
})
export class TRequestWorkflowModule {}
