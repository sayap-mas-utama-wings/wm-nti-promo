import { IsNumber, IsString } from 'class-validator';

export class BaseTRequestWorkflowDto {
  @IsString()
  requestType: string;

  @IsNumber()
  requestId: number;

  @IsNumber()
  noId: number;

  @IsString()
  seq: string;

  @IsString()
  requestName: string;

  @IsString()
  plant: string;

  @IsString()
  transplant: string;

  @IsString()
  sloc: string;

  @IsString()
  status: string;

  @IsString()
  currentUser: string;

  @IsString()
  currentUserName: string;

  @IsString()
  notes: string;

  @IsString()
  ticketNo: string;

  @IsString()
  flowCode: string;

  @IsString()
  bpk: string;

  @IsString()
  gate: string;

  @IsString()
  createdDate: string;

  @IsString()
  createdTime: string;

  @IsString()
  changedDate: string;

  @IsString()
  changed_time: string;
}
