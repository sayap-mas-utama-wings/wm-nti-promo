import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('m_permission')
export class MPermission {
  @PrimaryColumn({
    name: 'plant',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  plant: string;

  @PrimaryColumn({
    name: 'transportationplan',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  transplan: string;

  @PrimaryColumn({
    name: 'perm_code',
    type: 'varchar',
    length: 3,
    default: '',
    nullable: false,
  })
  permCode: string;

  @Column({
    name: 'sloc',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  sloc: string;

  @Column({
    name: 'desc',
    type: 'varchar',
    length: 50,
    default: '',
    nullable: false,
  })
  desc: string;

  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
    default: () => 'NOW()',
  })
  public createdOn: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @UpdateDateColumn({
    name: 'changed_on',
    type: 'timestamptz',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public changedOn: Date;

  @Column({
    name: 'changed_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public changedBy: string;
}
