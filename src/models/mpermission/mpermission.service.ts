import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository, FindOptionsWhere } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  addWhereClause,
  generateWhereClauseByAttr,
} from 'src/common/helpers/global.helper';
import { MPermission } from './entities/mpermission.entity';
import { CreateMPermissionDto } from './dtos/create-mpermission.dto';
import { UpdateMPermissionDto } from './dtos/update-mpermission.dto';

@Injectable()
export class MPermissionService {
  constructor(
    @InjectRepository(MPermission)
    private mPermissionRepo: Repository<MPermission>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: ['plant', 'transplan', 'permCode', 'sloc', 'desc'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = ['plant', 'transplan', 'permCode', 'sloc', 'desc'];
        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.mPermissionRepo.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        skip: (pagination.page - 1) * pagination.limit,
      });
      return result;
    } catch (error) {
      throw error;
    }
  }

  async findList(
    whereCondition: FindOptionsWhere<MPermission>,
  ): Promise<MPermission[]> {
    try {
      const data: MPermission[] = await this.mPermissionRepo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MPermission>,
  ): Promise<MPermission> {
    try {
      const data: MPermission = await this.mPermissionRepo.findOne({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async create(createMPermissiondDto: CreateMPermissionDto): Promise<any> {
    try {
      const inserted = await this.mPermissionRepo
        .createQueryBuilder()
        .insert()
        .values(createMPermissiondDto)
        .returning('*')
        .execute();
      const returned = { message: 'Data saved', raw: inserted?.generatedMaps };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async update(updateMPermissionDto: UpdateMPermissionDto): Promise<any> {
    try {
      const updated = await this.mPermissionRepo
        .createQueryBuilder()
        .update(MPermission)
        .set({
          sloc: updateMPermissionDto.sloc,
          desc: updateMPermissionDto.desc,
          changedBy: updateMPermissionDto.changedBy,
        })
        .where(
          'plant = :plant AND transplan = :transplan AND perm_code = :permCode',
          {
            plant: updateMPermissionDto.plant,
            transplan: updateMPermissionDto.transplan,
            permCode: updateMPermissionDto.permCode,
          },
        )
        .returning('*')
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async delete(
    plant: string,
    transplan: string,
    permCode: string,
  ): Promise<any> {
    try {
      const deleted = await this.mPermissionRepo
        .createQueryBuilder()
        .delete()
        .from(MPermission)
        .where(
          'plant = :plant AND transplan = :transplan AND perm_code = :permCode',
          {
            plant,
            transplan,
            permCode,
          },
        )
        .execute();
      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
