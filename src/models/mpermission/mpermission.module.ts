import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MPermissionService } from './mpermission.service';
import { MPermissionController } from './mpermission.controller';
import { MPermission } from './entities/mpermission.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MPermission])],
  providers: [MPermissionService],
  controllers: [MPermissionController],
})
export class MPermissionModule {}
