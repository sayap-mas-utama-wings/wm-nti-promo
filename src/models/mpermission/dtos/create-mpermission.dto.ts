import { IsString } from 'class-validator';
import { BaseMPermissionDto } from './base-mpermission.dto';

export class CreateMPermissionDto extends BaseMPermissionDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
