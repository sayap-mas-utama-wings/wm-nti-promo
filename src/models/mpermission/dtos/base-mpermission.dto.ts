import { IsString } from 'class-validator';

export class BaseMPermissionDto {
  @IsString()
  plant: string;

  @IsString()
  transplan: string;

  @IsString()
  permCode: string;

  @IsString()
  sloc: string;

  @IsString()
  desc: string;
}
