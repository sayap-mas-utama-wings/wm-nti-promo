import { PartialType } from '@nestjs/mapped-types';
import { IsString } from 'class-validator';
import { BaseMPermissionDto } from './base-mpermission.dto';

export class UpdateMPermissionDto extends PartialType(BaseMPermissionDto) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
