import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Req,
  HttpStatus,
  UseInterceptors,
} from '@nestjs/common';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { MPermissionService } from './mpermission.service';
import { MPermission } from './entities/mpermission.entity';
import { CreateMPermissionDto } from './dtos/create-mpermission.dto';
import { UpdateMPermissionDto } from './dtos/update-mpermission.dto';

@Controller('mpermission')
@UseInterceptors(TransformInterceptor)
export class MPermissionController {
  constructor(private mPermissionService: MPermissionService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mPermissionService.findAndPaginate(
        pagination,
        req.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findList')
  async findList(@Req() req): Promise<MPermission[]> {
    try {
      const query = req.query;
      return await this.mPermissionService.findList({ ...query });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async find(@Req() req): Promise<MPermission> {
    try {
      const query = req.query;
      return await this.mPermissionService.findOne({ ...query });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(@Body() createMPermissionDto: CreateMPermissionDto) {
    try {
      return await this.mPermissionService.create(createMPermissionDto);
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put()
  async update(@Body() updateMPermissionDto: UpdateMPermissionDto) {
    try {
      return await this.mPermissionService.update(updateMPermissionDto);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete(':plant/:transplan/:permCode')
  async delete(
    @Param('plant') plant: string,
    @Param('transplan') transplan: string,
    @Param('permCode') permCode: string,
  ) {
    try {
      return await this.mPermissionService.delete(plant, transplan, permCode);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
