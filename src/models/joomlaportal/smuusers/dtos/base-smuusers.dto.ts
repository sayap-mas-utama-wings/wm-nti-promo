import { IsString } from 'class-validator';

export class BaseSmuUsersDto {
  @IsString()
  name: string;

  @IsString()
  email: string;

  @IsString()
  username: string;
}
