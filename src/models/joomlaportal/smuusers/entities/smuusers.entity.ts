import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('smu_users')
export class SmuUsers {
  @PrimaryColumn({
    name: 'id',
    type: 'int',
  })
  id: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 400,
  })
  name: string;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 150,
  })
  username: string;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 100,
  })
  email: string;
  @Column({
    name: 'password',
    type: 'varchar',
    length: 100,
  })
  password: string;

  @Column({
    name: 'block',
    type: 'tinyint',
    // length: 4,
  })
  block: string;

  @Column({
    name: 'sendEmail',
    type: 'tinyint',
    // length: 4,
  })
  sendEmail: string;
}
