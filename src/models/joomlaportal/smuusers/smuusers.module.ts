import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { SmuUsersService } from './smuusers.service';
import { SmuUsersController } from './smuusers.controller';

@Module({
  imports: [DynamicDbModule],
  controllers: [SmuUsersController],
  providers: [SmuUsersService],
  exports: [SmuUsersService],
})
export class SmuUserModule {}
