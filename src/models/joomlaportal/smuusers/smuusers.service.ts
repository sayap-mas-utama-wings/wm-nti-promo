import { Injectable, NotFoundException } from '@nestjs/common';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { DataSource, FindOptionsOrder, FindOptionsWhere } from 'typeorm';
import { SmuUsers } from './entities/smuusers.entity';

@Injectable()
export class SmuUsersService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['name', 'username', 'email'],
          number: ['id'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = ['name', 'username', 'email'];

        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionJoomlaPortal();
      const smuUsersRepo = ds.getRepository(SmuUsers);

      const result = await smuUsersRepo
        .createQueryBuilder('smu_users')
        .where(whereClause)
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .getManyAndCount();

      return result;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<SmuUsers>,
    orderCondition?: FindOptionsOrder<SmuUsers>,
  ): Promise<SmuUsers> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionJoomlaPortal();
      const smuUsersRepo = ds.getRepository(SmuUsers);

      const data = await smuUsersRepo.findOne({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data smu user not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    whereCondition: FindOptionsWhere<SmuUsers>,
    orderCondition?: FindOptionsOrder<SmuUsers>,
  ): Promise<SmuUsers[]> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionJoomlaPortal();
      const smuUsersRepo = ds.getRepository(SmuUsers);

      const data = await smuUsersRepo.find({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data smu user not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }
}
