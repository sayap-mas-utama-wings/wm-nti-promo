import { Controller, Get, HttpStatus, Req } from '@nestjs/common';
import { SmuUsersService } from './smuusers.service';
import { responseError, responsePage } from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';

@Controller('smuusers')
export class SmuUsersController {
  constructor(private readonly smuUsersService: SmuUsersService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.smuUsersService.findAndPaginate(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async findOne(@Req() req) {
    try {
      return await this.smuUsersService.findOne(req?.query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
