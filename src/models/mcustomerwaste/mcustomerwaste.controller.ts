import {
  Controller,
  Get,
  HttpStatus,
  Req,
  UseInterceptors,
} from '@nestjs/common';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { MCustomerWasteService } from './mcustomerwaste.service';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';

@Controller('mcustomerwaste')
@UseInterceptors(TransformInterceptor)
export class MCustomerWasteController {
  constructor(private readonly mcustomerwasteService: MCustomerWasteService) {}

  @Get()
  async findAndPaginate(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mcustomerwasteService.findAndPaginate(
        pagination,
        req.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('getMIjinMuat')
  async getMIjinMuat(@Req() req) {
    try {
      return await this.mcustomerwasteService.getMIjinMuat(req.query.plant);
    } catch (error) {
      console.log('error catch', error);
      return responseError(error.message, error?.status);
    }
  }
}
