import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  addWhereClause,
  generateWhereClauseByAttr,
} from 'src/common/helpers/global.helper';
import { Repository } from 'typeorm';
import { MCustomerWaste } from './entities/mcustomerwaste.entity';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';

@Injectable()
export class MCustomerWasteService {
  constructor(
    @InjectRepository(MCustomerWaste)
    private readonly mIjiMuatRepo: Repository<MCustomerWaste>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: ['plant', 'transplan', 'custCode', 'custName'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = ['plant', 'transplan', 'custCode', 'custName'];
        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const results = await this.mIjiMuatRepo.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        skip: (pagination.page - 1) * pagination.limit,
      });

      return results;
    } catch (error) {
      throw error;
    }
  }

  async getMIjinMuat(plantQuery: string): Promise<any> {
    try {
      const whereClause = {};
      if (plantQuery) {
        Object.assign(whereClause, {
          plant: addWhereClause(plantQuery),
        });
      }
      const data = await this.mIjiMuatRepo.find({
        where: whereClause,
      });
      return data;
    } catch (error) {
      throw error;
    }
  }
}
