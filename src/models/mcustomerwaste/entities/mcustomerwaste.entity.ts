import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_customer_waste')
export class MCustomerWaste {
  @PrimaryColumn({
    name: 'transplan',
    type: 'varchar',
    length: 4,
    nullable: false,
  })
  transplan: string;

  @PrimaryColumn({
    name: 'plant',
    type: 'varchar',
    length: 4,
    nullable: false,
  })
  plant: string;

  @PrimaryColumn({
    name: 'cust_code',
    type: 'varchar',
    length: 10,
    default: '',
    nullable: false,
  })
  custCode: string;

  @Column({
    name: 'cust_name',
    type: 'varchar',
    length: 30,
    default: '',
    nullable: true,
  })
  custName: string;

  @Column({
    name: 'insert_at',
    type: 'timestamp',
    nullable: true,
  })
  insertAt: Date;
}
