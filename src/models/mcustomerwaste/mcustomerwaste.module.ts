import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MCustomerWaste } from './entities/mcustomerwaste.entity';
import { MCustomerWasteController } from './mcustomerwaste.controller';
import { MCustomerWasteService } from './mcustomerwaste.service';

@Module({
  imports: [TypeOrmModule.forFeature([MCustomerWaste])],
  controllers: [MCustomerWasteController],
  providers: [MCustomerWasteService],
  exports: [],
})
export class MCustomerWasteModule {}
