import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMtimelimitDto } from './dto/create-mtimelimit.dto';
import { UpdateMtimelimitDto } from './dto/update-mtimelimit.dto';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { FindOptionsWhere, Repository } from 'typeorm';
import { MTimeLimit } from './entities/mtimelimit.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';

@Injectable()
export class MTimeLimitService {
  constructor(
    @InjectRepository(MTimeLimit)
    private readonly mTimelimitRepository: Repository<MTimeLimit>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: [
            'plant',
            'transplan',
            'vehicleType',
            'flowCode',
            'typeCode',
            'jenisMuatan',
          ],
          number: ['timeLimit'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = [
          'plant',
          'transplan',
          'vehicleType',
          'flowCode',
          'typeCode',
          'jenisMuatan',
          'timeLimit',
        ];
        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const results = await this.mTimelimitRepository.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        skip: (pagination.page - 1) * pagination.limit,
      });
      return results;
    } catch (error) {
      throw error;
    }
  }

  async findList(whereCondition: FindOptionsWhere<MTimeLimit>) {
    try {
      const results: MTimeLimit[] = await this.mTimelimitRepository.find({
        where: whereCondition,
      });

      return results;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MTimeLimit>[],
    hideResponse?: boolean,
  ) {
    try {
      const results: MTimeLimit = await this.mTimelimitRepository.findOne({
        where: whereCondition,
      });

      if (!hideResponse && !results)
        throw new NotFoundException('Data not found!');
      return results;
    } catch (error) {
      throw error;
    }
  }

  async create(createMtimelimitDto: CreateMtimelimitDto) {
    try {
      const inserted = await this.mTimelimitRepository
        .createQueryBuilder()
        .insert()
        .values(createMtimelimitDto)
        .returning('*')
        .execute();

      const returned = { message: 'Data saved', raw: inserted?.generatedMaps };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async update(updateMtimelimitDto: UpdateMtimelimitDto) {
    try {
      const updated = await this.mTimelimitRepository
        .createQueryBuilder()
        .update(MTimeLimit)
        .set({
          timeLimit: updateMtimelimitDto.timeLimit,
          changedBy: updateMtimelimitDto.changedBy,
        })
        .where(
          'plant = :plant AND transplan = :transplan AND vehicle_type = :vehicleType AND flow_code = :flowCode AND type_code = :typeCode AND jenis_muatan = :jenisMuatan',
          {
            plant: updateMtimelimitDto.plant,
            transplan: updateMtimelimitDto.transplan,
            vehicleType: updateMtimelimitDto.vehicleType,
            flowCode: updateMtimelimitDto.flowCode,
            typeCode: updateMtimelimitDto.typeCode,
            jenisMuatan: updateMtimelimitDto.jenisMuatan,
          },
        )
        .returning('*')
        .updateEntity(true)
        .execute();

      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };

      return returned;
    } catch (error) {
      throw error;
    }
  }

  async delete(
    plant: string,
    transplan: string,
    vehicleType: string,
    flowCode: string,
    typeCode: string,
    jenisMuatan: number,
  ) {
    try {
      const deleted = await this.mTimelimitRepository
        .createQueryBuilder()
        .delete()
        .from(MTimeLimit)
        .where(
          'plant = :plant AND transplan = :transplan AND vehicle_type = :vehicleType AND flow_code = :flowCode AND type_code = :typeCode AND jenis_muatan = :jenisMuatan',
          {
            plant,
            transplan,
            vehicleType,
            flowCode,
            typeCode,
            jenisMuatan,
          },
        )
        .execute();

      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
