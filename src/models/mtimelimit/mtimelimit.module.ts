import { Module } from '@nestjs/common';
import { MTimeLimitService } from './mtimelimit.service';
import { MTimeLimitController } from './mtimelimit.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MTimeLimit } from './entities/mtimelimit.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MTimeLimit])],
  controllers: [MTimeLimitController],
  providers: [MTimeLimitService],
  exports: [MTimeLimitService],
})
export class MTimeLimitModule {}
