import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('m_timelimit')
export class MTimeLimit {
  @PrimaryColumn({
    name: 'plant',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  plant: string;

  @PrimaryColumn({
    name: 'transportationplan',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  transplan: string;

  @PrimaryColumn({
    name: 'vehicle_type',
    type: 'varchar',
    length: 10,
    default: '',
    nullable: false,
  })
  vehicleType: string;

  @PrimaryColumn({
    name: 'flow_code',
    type: 'varchar',
    length: 2,
    default: '',
    nullable: false,
  })
  flowCode: string;

  @PrimaryColumn({
    name: 'type_code',
    type: 'varchar',
    length: 3,
    default: 0,
    nullable: false,
  })
  typeCode: string;

  @PrimaryColumn({
    name: 'jenis_muatan',
    type: 'varchar',
    length: 25,
    default: 0,
    nullable: false,
  })
  jenisMuatan: string;

  @Column({
    name: 'time_limit',
    type: 'int4',
    default: 0,
    nullable: false,
  })
  timeLimit: number;

  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
    default: () => 'NOW()',
  })
  public createdOn: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @UpdateDateColumn({
    name: 'changed_on',
    type: 'timestamptz',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public changedOn: Date;

  @Column({
    name: 'changed_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public changedBy: string;
}
