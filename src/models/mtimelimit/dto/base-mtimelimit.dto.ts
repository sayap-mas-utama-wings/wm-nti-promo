import { IsInt, IsString } from 'class-validator';

export class BaseMTimeLimitDto {
  @IsString()
  plant: string;

  @IsString()
  transplan: string;

  @IsString()
  vehicleType: string;

  @IsString()
  flowCode: string;

  @IsString()
  typeCode: string;

  @IsString()
  jenisMuatan: string;

  @IsInt()
  timeLimit: number;
}
