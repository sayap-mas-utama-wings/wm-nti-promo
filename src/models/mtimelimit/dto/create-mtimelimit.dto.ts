import { IsString } from 'class-validator';
import { BaseMTimeLimitDto } from './base-mtimelimit.dto';

export class CreateMtimelimitDto extends BaseMTimeLimitDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
