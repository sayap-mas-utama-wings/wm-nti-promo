import { PartialType } from '@nestjs/mapped-types';
import { BaseMTimeLimitDto } from './base-mtimelimit.dto';
import { IsString } from 'class-validator';

export class UpdateMtimelimitDto extends PartialType(BaseMTimeLimitDto) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
