import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Req,
  HttpStatus,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import { MTimeLimitService } from './mtimelimit.service';
import { CreateMtimelimitDto } from './dto/create-mtimelimit.dto';
import { UpdateMtimelimitDto } from './dto/update-mtimelimit.dto';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';

@Controller('mtimelimit')
@UseInterceptors(TransformInterceptor)
export class MTimeLimitController {
  constructor(private readonly mTimeLimitService: MTimeLimitService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mTimeLimitService.findAndPaginate(
        pagination,
        req.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findList')
  async findList(@Req() req) {
    try {
      const query = req.query;
      return await this.mTimeLimitService.findList(query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async findOne(@Req() req) {
    try {
      const query = req.query;
      return await this.mTimeLimitService.findOne([query]);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(@Body() createMtimelimitDto: CreateMtimelimitDto) {
    try {
      return await this.mTimeLimitService.create(createMtimelimitDto);
    } catch (error) {
      let message = error.message;
      let statusCode = error?.status;

      if (error?.code == '23505') {
        message = 'Data already exists!';
        statusCode = HttpStatus.CONFLICT;
      }
      return responseError(
        message,
        statusCode ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put()
  async update(@Body() updateMtimelimitDto: UpdateMtimelimitDto) {
    try {
      return await this.mTimeLimitService.update(updateMtimelimitDto);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete(':plant/:transplan/:vehicleType/:flowCode/:typeCode/:jenisMuatan')
  async delete(
    @Param('plant') plant: string,
    @Param('transplan') transplan: string,
    @Param('vehicleType') vehicleType: string,
    @Param('flowCode') flowCode: string,
    @Param('typeCode') typeCode: string,
    @Param('jenisMuatan') jenisMuatan: number,
  ) {
    try {
      return await this.mTimeLimitService.delete(
        plant,
        transplan,
        vehicleType,
        flowCode,
        typeCode,
        jenisMuatan,
      );
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
