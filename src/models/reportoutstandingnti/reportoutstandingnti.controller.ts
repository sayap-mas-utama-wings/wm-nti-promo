import { Controller, Get, HttpStatus, Req } from '@nestjs/common';
import { ReportOutStandingNtiService } from './reportoutstandingnti.service';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';

@Controller('reportoutstandingnti')
export class ReportOutStandingNtiController {
  constructor(
    private readonly reportOutStandingNtiService: ReportOutStandingNtiService,
  ) {}

  @Get()
  async findAndPaginate(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] =
        await this.reportOutStandingNtiService.listReportManual(
          pagination,
          req.query,
        );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('listDetail')
  async listDetail(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] =
        await this.reportOutStandingNtiService.listDetailReport(
          pagination,
          req.query,
        );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
