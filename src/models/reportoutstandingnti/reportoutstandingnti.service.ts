import { Injectable } from '@nestjs/common';
import { TReqNtiHdrService } from '../treqntihdr/treqntihdr.service';
import { TReqNtiDtlService } from '../treqntidtl/treqntidtl.service';
import { TReqNtiHdr } from '../treqntihdr/entities/treqntihdr.entity';
import { Repository } from 'typeorm';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { InjectRepository } from '@nestjs/typeorm';
import { TLogNtiService } from '../tlognti/tlognti.service';

@Injectable()
export class ReportOutStandingNtiService {
  constructor(
    private readonly tReqNtiHdrService: TReqNtiHdrService,
    private readonly tReqNtiDtlService: TReqNtiDtlService,
    private readonly tLogNtiService: TLogNtiService,
    @InjectRepository(TReqNtiHdr) private tntiHdrRepo: Repository<TReqNtiHdr>,
  ) {}

  async listReport(pagination: PaginationOptions, search?: any): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['plant', 'plantDescription'],
          number: ['ntiId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        console.log(
          '🚀 ~ ReportOutStandingNtiService ~ listReport ~ whereClause:',
          whereClause,
        );
        Object.assign(whereClause);

        const columnNames = ['tReqNtiHdr.plant', 'tReqNtiHdr.plantDescription'];

        const columnIndex = search?.columnIndex ? search.columnIndex : 0;
        const sortOrder = search?.sortOrder ? search?.sortOrder : 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.tntiHdrRepo
        .createQueryBuilder('tReqNtiHdr')
        // .innerJoinAndSelect('tReqNtiHdr.tReqNtiDtl', 'tReqNtiDtl')
        .select([
          // 'tReqNtiHdr.ntiId',
          'tReqNtiHdr.plant',
          'tReqNtiHdr.plantDescription',
        ])
        // .addSelect((db) => {
        //   // return (
        //   //   db
        //   //     .createQueryBuilder()
        //   //     // .from(TReqNtiDtl, 'treq')
        //   //     .where(
        //   //       'treq.plant = tReqNtiHdr.plant and treq.initial_status = :initialStatus',
        //   //       {
        //   //         initialStatus: 'W',
        //   //       },
        //   //     )
        //   // );
        //   const subQuery = db
        //     .subQuery()
        //     .from(TReqNtiDtl, 'treq')
        //     .addSelect('count(treq.material_id) as total')
        //     .where('plant = tReqNtiHdr.plant');
        //   return subQuery;
        // }, 'created')
        .where(whereClause)
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .groupBy('tReqNtiHdr.plant')
        // .addGroupBy('tReqNtiHdr.plant')
        .addGroupBy('tReqNtiHdr.plantDescription')
        .getManyAndCount();

      return result;
    } catch (error) {
      throw error;
    }
  }

  async listReportManual(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let sql = '';
      let whereOr = '';
      let whereCreatedDateStartEnd = '';
      let wherePlant = '';
      let whereCreated = '';
      let whereApproved = '';
      let wherePartial = '';
      let whereIfNol = '';

      sql += `select trnh.plant, trnh.plant_description as plantDescription, `;
      sql += `(
        select 
        count(trnd.material_id) as total
        from t_req_nti_hdr trnh2
          join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
        where trnh2.plant = trnh.plant
        and trnh2.plant_description = trnh.plant_description
        -- and trnh2.initial_status = 'W'
        and trnd.status = 'Created'
      ) as "created", `;
      sql += `(
        select 
        count(trnd.material_id) as total
        from t_req_nti_hdr trnh2
          join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
        where trnh2.plant = trnh.plant
        and trnh2.plant_description = trnh.plant_description
        -- and trnh2.initial_status = 'A'
        and trnd.status = 'Approved'
      ) as "approved", `;
      sql += `
      (
        select 
        count(trnd.material_id) as total
        from t_req_nti_hdr trnh2
          join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
        where trnh2.plant = trnh.plant
        and trnh2.plant_description = trnh.plant_description
        -- and trnh2.initial_status = 'P'
        and trnd.status = 'Partial'
      ) as "partial" `;
      // FROM
      sql += `from t_req_nti_hdr trnh `;
      // WHERE
      if (search?.genSearch) {
        sql += `WHERE `;
        sql += `trnh.plant LIKE '%${search.genSearch}%' `;
        sql += `OR trnh.plant_description LIKE '%${search.genSearch}%' `;
        sql += `OR CAST(
          (
          select 
          count(trnd.material_id) as total
          from t_req_nti_hdr trnh2
            join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
          where trnh2.plant = trnh.plant
          and trnh2.plant_description = trnh.plant_description
          and trnh2.initial_status = 'W'
        ) AS TEXT) like '%${search.genSearch}%' `;
        sql += `OR CAST(
          (
          select 
          count(trnd.material_id) as total
          from t_req_nti_hdr trnh2
            join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
          where trnh2.plant = trnh.plant
          and trnh2.plant_description = trnh.plant_description
          and trnh2.initial_status = 'A'
        ) AS TEXT) like '%${search.genSearch}%' `;
        sql += `OR CAST(
          (
          select 
          count(trnd.material_id) as total
          from t_req_nti_hdr trnh2
            join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
          where trnh2.plant = trnh.plant
          and trnh2.plant_description = trnh.plant_description
          and trnh2.initial_status = 'P'
        ) AS TEXT) like '$${search.genSearch}%' `;
      } else {
        console.log('search ', search);
        if (search?.createdDateStart && search?.createdDateEnd) {
          whereCreatedDateStartEnd += `trnh.created_date between '${search?.createdDateStart} 00:00:00' `;
          whereCreatedDateStartEnd += `AND '${search?.createdDateEnd} 23:59:59' `;
          if (search.plant2) {
            whereCreatedDateStartEnd += `AND trnh.plant in(${search.plant2}) `;
          }
          whereOr = 'AND';
        }

        whereIfNol = `
          ${whereOr} (
            select 
            count(trnd.material_id) as total
            from t_req_nti_hdr trnh2
              join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
            where trnh2.plant = trnh.plant
            and trnh2.plant_description = trnh.plant_description
            and trnd.status in ('Created', 'Approved', 'Partial')
          ) != 0 `;

        if (search?.plant) {
          wherePlant = ` ${whereOr} trnh.plant = '${search.plant}' `;
          wherePlant += `OR trnh.plant_description LIKE '%${search.plant}%' `;
          whereOr = 'OR';
        }
        if (search?.created) {
          whereCreated = `${whereOr} CAST(
            (
            select 
            count(trnd.material_id) as total
            from t_req_nti_hdr trnh2
              join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
            where trnh2.plant = trnh.plant
            and trnh2.plant_description = trnh.plant_description
            and trnh2.initial_status = 'W'
          ) AS TEXT) like '%${search.created}%' `;
          whereOr = 'OR';
        }
        if (search?.approved) {
          whereApproved = `${whereOr} CAST(
            (
            select 
            count(trnd.material_id) as total
            from t_req_nti_hdr trnh2
              join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
            where trnh2.plant = trnh.plant
            and trnh2.plant_description = trnh.plant_description
            and trnh2.initial_status = 'A'
          ) AS TEXT) like '%${search.approved}%' `;
          whereOr = 'OR';
        }
        if (search?.partial) {
          wherePartial = `${whereOr} CAST(
            (
            select 
            count(trnd.material_id) as total
            from t_req_nti_hdr trnh2
              join t_req_nti_dtl trnd on trnd.request_id = trnh2.request_id
            where trnh2.plant = trnh.plant
            and trnh2.plant_description = trnh.plant_description
            and trnh2.initial_status = 'P'
          ) AS TEXT) like '$${search.partial}%' `;
          whereOr = 'OR';
        }

        if (whereOr) {
          sql += `WHERE ${whereCreatedDateStartEnd} ${whereIfNol} ${wherePlant} ${whereCreated} ${whereApproved} ${wherePartial} `;
        }
      }
      sql += `group by (trnh.plant, trnh.plant_description) `;
      const sortOrder = search?.sortOrder ? search.sortOrder : 'ASC';
      sql += `order by plant ${sortOrder} `;

      if (pagination.limit) {
        sql += `LIMIT ${pagination.limit} `;
      }
      if (pagination.page > 1) {
        sql += `OFFSET ${(pagination.page - 1) * pagination.limit} `;
      }

      const result = await this.tntiHdrRepo.query(sql);
      const total = await this.tntiHdrRepo.query(
        `SELECT trnh.plant, trnh.plant_description 
        FROM t_req_nti_hdr as trnh 
        WHERE ${whereCreatedDateStartEnd} ${wherePlant} ${whereCreated} ${whereApproved} ${wherePartial} 
        group by (trnh.plant, trnh.plant_description) `,
      );

      return [result, total.length];
    } catch (error) {
      throw error;
    }
  }

  async listDetailReport(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      const tReqNtiHdrData = await this.tReqNtiHdrService.findOutstanding(
        {
          plant: search?.plant,
          requestId: search?.requestId,
        },
        [
          'TReqNtiHdr.requestId as "requestId"',
          'TReqNtiHdr.plantDescription as "plantDescription"',
        ],
      );
      console.log(
        '🚀 ~ ReportOutStandingNtiService ~ tReqNtiHdrData:',
        tReqNtiHdrData,
      );

      // const tReqNtiHdrData = await this.tReqNtiHdrService.findAll(
      //   whereClause,
      //   [
      //     'TReqNtiHdr.requestId as "requestId"',
      //     'TReqNtiHdr.plantDescription as "plantDescription"',
      //   ],
      //   // 'TReqNtiHdr.plant, TReqNtiHdr.plantDescription',
      // );

      if (tReqNtiHdrData && tReqNtiHdrData?.length <= 0) {
        return [[], 0];
      }

      const ck = tReqNtiHdrData.map((r) => {
        const cek = [];
        cek.push(r.requestId);
        return cek;
      });
      console.log(
        '🚀 ~ ReportOutStandingNtiService ~ tReqNtiHdrData:',
        tReqNtiHdrData,
        ck.toString(),
      );

      search.requestId = ck.toString();
      const [results, total] =
        await this.tReqNtiDtlService.findAndPaginateDetailOutstanding(
          pagination,
          search,
        );

      if (results && results.length > 0) {
        for (let i = 0; i < results.length; i++) {
          results[i]['plant'] = search?.plant;
          results[i]['plantDescription'] = tReqNtiHdrData[0].plantDescription;
        }
      }

      return [results, total];
    } catch (error) {
      throw error;
    }
  }
}
