import { Module } from '@nestjs/common';
import { TReqNtiHdrModule } from '../treqntihdr/treqntihdr.module';
import { TReqntiDtlModule } from '../treqntidtl/treqntidtl.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TReqNtiHdr } from '../treqntihdr/entities/treqntihdr.entity';
import { ReportOutStandingNtiService } from './reportoutstandingnti.service';
import { ReportOutStandingNtiController } from './reportoutstandingnti.controller';
import { TLogNtiModule } from '../tlognti/tlognti.module';

@Module({
  imports: [
    TReqNtiHdrModule,
    TReqntiDtlModule,
    TypeOrmModule.forFeature([TReqNtiHdr]),
    TLogNtiModule,
  ],
  providers: [ReportOutStandingNtiService],
  controllers: [ReportOutStandingNtiController],
})
export class ReportOutStandingNtiModule {}
