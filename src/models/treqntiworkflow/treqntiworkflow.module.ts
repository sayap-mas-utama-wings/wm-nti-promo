import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TReqNtiWorkflow } from './entities/treqntiworkflow';
import { TReqNtiWorkflowService } from './treqntiworkflow.service';

@Module({
  imports: [TypeOrmModule.forFeature([TReqNtiWorkflow])],
  controllers: [],
  providers: [TReqNtiWorkflowService],
  exports: [TReqNtiWorkflowService],
})
export class TReqNtiWorkflowModule {}
