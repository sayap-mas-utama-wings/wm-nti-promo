import { IsString } from 'class-validator';
import { BaseTreqNtiWorkflow } from './base-treqntiworkflow.dto';

export class CreateTReqNtiWorkflowDto extends BaseTreqNtiWorkflow {
  @IsString()
  createdBy: string;

  @IsString()
  createdName: string;

  @IsString()
  createdDate: string;
}
