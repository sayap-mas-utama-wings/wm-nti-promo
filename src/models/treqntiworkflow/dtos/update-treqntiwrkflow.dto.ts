import { IsString } from 'class-validator';
import { BaseTreqNtiWorkflow } from './base-treqntiworkflow.dto';

export class UpdateTReqNtiWorkflowDto extends BaseTreqNtiWorkflow {
  @IsString()
  updatedBy: string;

  @IsString()
  updatedName: string;

  @IsString()
  updatedDate: string;
}
