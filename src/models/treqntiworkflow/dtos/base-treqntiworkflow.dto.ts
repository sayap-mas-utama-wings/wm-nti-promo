import { IsNumber, IsString } from 'class-validator';

export class BaseTreqNtiWorkflow {
  @IsNumber()
  workflowId: number;

  @IsString()
  requestId: string;

  @IsString()
  userId: string;

  @IsString()
  userName: string;

  @IsString()
  status: string;

  @IsString()
  sequence: string;

  @IsString()
  notes: string;
}
