import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('t_req_nti_workflow')
export class TReqNtiWorkflow {
  @PrimaryColumn({
    type: 'int4',
    name: 'workflow_id',
  })
  workflowId: number;

  @Column({
    name: 'request_id',
    type: 'varchar',
    length: 12,
  })
  requestId: string;

  @Column({
    name: 'user_id',
    type: 'varchar',
    length: 15,
  })
  userId: string;

  @Column({
    name: 'user_name',
    type: 'varchar',
    length: 50,
  })
  userName: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 10,
  })
  status: string;

  @Column({
    name: 'sequence',
    type: 'int2',
  })
  sequence: string;

  @Column({
    name: 'notes',
  })
  notes: string;

  @CreateDateColumn({
    name: 'created_date',
    type: 'date',
    default: () => 'NOW()::date',
  })
  public createdDate: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @Column({
    name: 'created_name',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdName: string;

  @UpdateDateColumn({
    name: 'updated_date',
    type: 'date',
    onUpdate: 'NOW()',
  })
  public updatedDate: Date;

  @Column({
    name: 'updated_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public updatedBy: string;

  @Column({
    name: 'updated_name',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public updatedName: string;
}
