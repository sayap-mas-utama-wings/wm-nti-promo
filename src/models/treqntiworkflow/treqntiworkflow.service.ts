import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TReqNtiWorkflow } from './entities/treqntiworkflow';
import { EntityManager, InsertResult, Repository } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { CreateTReqNtiWorkflowDto } from './dtos/create-treqntiworkflow.dto';
import { UpdateTReqNtiWorkflowDto } from './dtos/update-treqntiwrkflow.dto';

@Injectable()
export class TReqNtiWorkflowService {
  constructor(
    @InjectRepository(TReqNtiWorkflow)
    private tReqNtiWorkflowRepo: Repository<TReqNtiWorkflow>,
  ) {}

  async create(
    createDto: QueryDeepPartialEntity<CreateTReqNtiWorkflowDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }
      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(TReqNtiWorkflow)
          .values(createDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.tReqNtiWorkflowRepo
          .createQueryBuilder()
          .insert()
          .values(createDto)
          .returning(returning)
          .execute();
      }

      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(
    requestId: string,
    updateDto: QueryDeepPartialEntity<UpdateTReqNtiWorkflowDto>,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      const repositoryUpdate = transactionManager
        ? transactionManager
        : this.tReqNtiWorkflowRepo;

      const updated = await repositoryUpdate
        .createQueryBuilder()
        .update(TReqNtiWorkflow)
        .set({ ...updateDto })
        .where('request_id = :requestId', {
          requestId,
        })
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
      return returned;
    } catch (error) {
      throw error;
    }
  }
}
