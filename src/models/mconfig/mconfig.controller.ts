import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Req,
  HttpStatus,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import { MConfigService } from './mconfig.service';
import { CreateMConfigDto } from './dto/create-mconfig.dto';
import { UpdateMConfigDto } from './dto/update-mconfig.dto';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';

@Controller('mconfig')
@UseInterceptors(TransformInterceptor)
export class MConfigController {
  constructor(private readonly mConfigService: MConfigService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mConfigService.findAndPaginate(
        pagination,
        req.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findList')
  async findList(@Req() req) {
    try {
      const query = req.query;
      return await this.mConfigService.findList(query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async findOne(@Req() req) {
    try {
      const query = req.query;
      return await this.mConfigService.findOne(query);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(@Body() createMConfigDto: CreateMConfigDto) {
    try {
      return await this.mConfigService.create(createMConfigDto);
    } catch (error) {
      let message = error.message;
      let statusCode = error?.status;

      if (error?.code == '23505') {
        message = 'Data already exists!';
        statusCode = HttpStatus.CONFLICT;
      }
      return responseError(
        message,
        statusCode ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put()
  async update(@Body() updateMConfigDto: UpdateMConfigDto) {
    try {
      return await this.mConfigService.update(updateMConfigDto);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete(':plant/:transplan')
  async remove(
    @Param('plant') plant: string,
    @Param('transplan') transplan: string,
  ) {
    try {
      return await this.mConfigService.delete(plant, transplan);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
