import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMConfigDto } from './dto/create-mconfig.dto';
import { UpdateMConfigDto } from './dto/update-mconfig.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOptionsWhere, Repository } from 'typeorm';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { MConfig } from 'src/models/mconfig/entities/mconfig.entity';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';

@Injectable()
export class MConfigService {
  constructor(
    @InjectRepository(MConfig)
    private readonly mConfigRepository: Repository<MConfig>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: [
            'plant',
            'transplan',
            'carMandatory',
            'area',
            'lastnumber',
            'address1',
            'address2',
            'address3',
            'lab',
            'noWarehouse',
            'stopper',
            'integratedSecureAcces',
            'ipServer',
            'companyLogo',
            'bpk',
            'oneway',
            'ewm',
            'scangudang',
            'coproduct',
            'frozen',
            'integratedVms',
            'slipsheetManagement',
            'flagPlant',
          ],
          number: ['totalGate', 'loadingDock', 'lastnumberIjin'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = [
          'plant',
          'transplan',
          'totalGate',
          'loadingDock',
          'carMandatory',
          'area',
          'lastnumber',
          'address1',
          'address2',
          'address3',
          'lab',
          'noWarehouse',
          'stopper',
          'integratedSecureAcces',
          'lastnumberIjin',
          'ipServer',
          'companyLogo',
          'bpk',
          'oneway',
          'ewm',
          'scangudang',
          'coproduct',
          'frozen',
          'integratedVms',
          'slipsheetManagement',
          'flagPlant',
        ];
        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const results = await this.mConfigRepository.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        skip: (pagination.page - 1) * pagination.limit,
      });
      return results;
    } catch (error) {
      throw error;
    }
  }

  async findList(whereCondition: FindOptionsWhere<MConfig>) {
    try {
      const results: MConfig[] = await this.mConfigRepository.find({
        where: whereCondition,
      });

      return results;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MConfig>,
    hideResponse?: boolean,
  ) {
    try {
      const results: MConfig = await this.mConfigRepository.findOne({
        where: whereCondition,
      });

      if (!results && !hideResponse)
        throw new NotFoundException('Data not found!');
      return results;
    } catch (error) {
      throw error;
    }
  }

  async create(createMConfigDto: CreateMConfigDto) {
    try {
      const inserted = await this.mConfigRepository
        .createQueryBuilder()
        .insert()
        .values(createMConfigDto)
        .returning('*')
        .execute();

      const returned = { message: 'Data saved', raw: inserted?.generatedMaps };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async update(updateMConfigDto: UpdateMConfigDto) {
    try {
      const updated = await this.mConfigRepository
        .createQueryBuilder()
        .update(MConfig)
        .set({
          totalGate: updateMConfigDto.totalGate,
          loadingDock: updateMConfigDto.loadingDock,
          carMandatory: updateMConfigDto.carMandatory,
          area: updateMConfigDto.area,
          lastnumber: updateMConfigDto.lastnumber,
          address1: updateMConfigDto.address1,
          address2: updateMConfigDto.address2,
          address3: updateMConfigDto.address3,
          lab: updateMConfigDto.lab,
          noWarehouse: updateMConfigDto.noWarehouse,
          stopper: updateMConfigDto.stopper,
          integratedSecureAcces: updateMConfigDto.integratedSecureAcces,
          lastnumberIjin: updateMConfigDto.lastnumberIjin,
          ipServer: updateMConfigDto.ipServer,
          companyLogo: updateMConfigDto.companyLogo,
          bpk: updateMConfigDto.bpk,
          oneway: updateMConfigDto.oneway,
          ewm: updateMConfigDto.ewm,
          scangudang: updateMConfigDto.scangudang,
          coproduct: updateMConfigDto.coproduct,
          frozen: updateMConfigDto.frozen,
          integratedVms: updateMConfigDto.integratedVms,
          slipsheetManagement: updateMConfigDto.slipsheetManagement,
          flagPlant: updateMConfigDto.flagPlant,
        })
        .where('plant = :plant AND transplan = :transplan', {
          plant: updateMConfigDto.plant,
          transplan: updateMConfigDto.transplan,
        })
        .returning('*')
        .updateEntity(true)
        .execute();

      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };

      return returned;
    } catch (error) {
      throw error;
    }
  }

  async delete(plant: string, transplan: string) {
    try {
      const deleted = await this.mConfigRepository
        .createQueryBuilder()
        .delete()
        .from(MConfig)
        .where('plant = :plant AND transplan = :transplan', {
          plant,
          transplan,
        })
        .execute();

      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
