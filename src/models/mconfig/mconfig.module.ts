import { Module } from '@nestjs/common';
import { MConfigService } from './mconfig.service';
import { MConfigController } from './mconfig.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MConfig } from './entities/mconfig.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MConfig])],
  controllers: [MConfigController],
  providers: [MConfigService],
  exports: [MConfigService],
})
export class MConfigModule {}
