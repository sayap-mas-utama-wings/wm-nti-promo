import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('m_config')
export class MConfig {
  @PrimaryColumn({
    name: 'plant',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  plant: string;

  @PrimaryColumn({
    name: 'transportationplan',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  transplan: string;

  @Column({
    name: 'total_gate',
    type: 'int2',
    default: 0,
    nullable: false,
  })
  totalGate: number;

  @Column({
    name: 'loading_dock',
    type: 'int2',
    default: 0,
    nullable: false,
  })
  loadingDock: number;

  @Column({
    name: 'car_mandatory',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  carMandatory: string;

  @Column({
    name: 'area',
    type: 'varchar',
    length: 200,
    default: '',
    nullable: false,
  })
  area: string;

  @Column({
    name: 'lastnumber',
    type: 'varchar',
    length: 10,
    default: '',
    nullable: false,
  })
  lastnumber: string;

  @Column({
    name: 'address1',
    type: 'varchar',
    length: 200,
    default: '',
    nullable: false,
  })
  address1: string;

  @Column({
    name: 'address2',
    type: 'varchar',
    length: 200,
    default: '',
    nullable: false,
  })
  address2: string;

  @Column({
    name: 'address3',
    type: 'varchar',
    length: 200,
    default: '',
    nullable: false,
  })
  address3: string;

  @Column({
    name: 'lab',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  lab: string;

  @Column({
    name: 'no_warehouse',
    type: 'varchar',
    length: 3,
    default: '',
    nullable: false,
  })
  noWarehouse: string;

  @Column({
    name: 'stopper',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  stopper: string;

  @Column({
    name: 'integrated_secure_access',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  integratedSecureAcces: string;

  @Column({
    name: 'lastnumber_ijin',
    type: 'int4',
    default: 0,
    nullable: false,
  })
  lastnumberIjin: number;

  @Column({
    name: 'ip_server',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  ipServer: string;

  @Column({
    name: 'company_logo',
    type: 'varchar',
    length: 255,
    default: '',
    nullable: false,
  })
  companyLogo: string;

  @Column({
    name: 'bpk',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  bpk: string;

  @Column({
    name: 'oneway',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  oneway: string;

  @Column({
    name: 'ewm',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  ewm: string;

  @Column({
    name: 'scangudang',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  scangudang: string;

  @Column({
    name: 'coproduct',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  coproduct: string;

  @Column({
    name: 'frozen',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  frozen: string;

  @Column({
    name: 'integrated_vms',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  integratedVms: string;

  @Column({
    name: 'slipsheet_management',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  slipsheetManagement: string;

  @Column({
    name: 'flag_plant',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  flagPlant: string;

  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
    default: () => 'NOW()',
  })
  public createdOn: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @UpdateDateColumn({
    name: 'changed_on',
    type: 'timestamptz',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public changedOn: Date;

  @Column({
    name: 'changed_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public changedBy: string;
}
