import { IsString } from 'class-validator';
import { BaseMConfigDto } from './base-mconfig.dto';

export class CreateMConfigDto extends BaseMConfigDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
