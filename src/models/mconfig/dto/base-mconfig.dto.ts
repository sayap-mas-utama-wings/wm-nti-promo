import { IsInt, IsString } from 'class-validator';

export class BaseMConfigDto {
  @IsString()
  plant: string;

  @IsString()
  transplan: string;

  @IsInt()
  totalGate: number;

  @IsInt()
  loadingDock: number;

  @IsString()
  carMandatory: string;

  @IsString()
  area: string;

  @IsString()
  lastnumber: string;

  @IsString()
  address1: string;

  @IsString()
  address2: string;

  @IsString()
  address3: string;

  @IsString()
  lab: string;

  @IsString()
  noWarehouse: string;

  @IsString()
  stopper: string;

  @IsString()
  integratedSecureAcces: string;

  @IsInt()
  lastnumberIjin: number;

  @IsString()
  ipServer: string;

  @IsString()
  companyLogo: string;

  @IsString()
  bpk: string;

  @IsString()
  oneway: string;

  @IsString()
  ewm: string;

  @IsString()
  scangudang: string;

  @IsString()
  coproduct: string;

  @IsString()
  frozen: string;

  @IsString()
  integratedVms: string;

  @IsString()
  slipsheetManagement: string;

  @IsString()
  flagPlant: string;
}
