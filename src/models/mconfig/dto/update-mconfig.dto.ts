import { PartialType } from '@nestjs/mapped-types';
import { IsString } from 'class-validator';
import { BaseMConfigDto } from './base-mconfig.dto';

export class UpdateMConfigDto extends PartialType(BaseMConfigDto) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
