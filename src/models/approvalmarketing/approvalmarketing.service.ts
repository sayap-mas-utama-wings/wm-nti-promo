import { DataSource, Entity, In } from 'typeorm';
import { TReqNtiHdrService } from '../treqntihdr/treqntihdr.service';
import { TReqNtiDtlService } from '../treqntidtl/treqntidtl.service';
import { TLogNtiService } from '../tlognti/tlognti.service';
import { TReqNtiWorkflowService } from '../treqntiworkflow/treqntiworkflow.service';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { UpdateApprovalMarketingDto } from './dtos/update-approvalmarketing.dto';
import { TInboxV2Service } from '../inbox/tinboxv2/tinboxv2.service';
import { EmailService } from '../email/email.service';
import { SendEmailApprovalMarketingDto } from './dtos/send-email-approvalmarketing.dto';
import { TReqNtiDtl } from '../treqntidtl/entities/treqntidtl.entity';
import { TReqNtiHdr } from '../treqntihdr/entities/treqntihdr.entity';
import { MNtiUserService } from '../mntiuser/mntiuser.service';
import { MNtiUser } from '../mntiuser/entities/mntiuser';
import { SendEmailRejectMarketingDto } from './dtos/send-email-rejectmarketing.dto';
import { MSlocService } from '../general/msloc/msloc.service';

@Entity()
export class ApprovalMarketingService {
  constructor(
    private readonly tReqNtiHdrService: TReqNtiHdrService,
    private readonly tReqNtiDtlService: TReqNtiDtlService,
    private readonly tLogNtiService: TLogNtiService,
    private readonly tReqNtiWorkflowService: TReqNtiWorkflowService,
    private readonly dynamicDbService: DynamicDbService,
    private readonly dataSource: DataSource,
    private readonly tInboxV2Service: TInboxV2Service,
    private readonly emailService: EmailService,
    private readonly mNtiUserService: MNtiUserService,
    private readonly mSlocService: MSlocService,
  ) {}

  async listDetailFromInbox(listRequestId: string[]): Promise<any> {
    try {
      const tReqNtiData = await this.tReqNtiDtlService.findAll({
        requestId: In(listRequestId),
      });

      if (tReqNtiData && tReqNtiData.length > 0) {
        for (const r of tReqNtiData) {
          if (r.plant && r.sloc) {
            const mSlocData = await this.mSlocService.findOne({
              werksLgort: `${r.plant};${r.sloc}`,
            });

            r['slocDescription'] = mSlocData?.description.split(';')[1];
          }
        }
      }

      return tReqNtiData;
    } catch (error) {
      throw error;
    }
  }

  async approval(value: UpdateApprovalMarketingDto): Promise<any> {
    try {
      await this.dataSource.transaction(async (manager) => {
        for (const row of value.listRequestId) {
          await this.tReqNtiHdrService.update(
            row,
            {
              initialStatus: 'A',
              currentUser: value.updatedBy,
              updatedBy: value.updatedBy,
              updatedName: value.updatedName,
              updatedDate() {
                return 'NOW()';
              },
            },
            manager,
          );

          await this.tReqNtiWorkflowService.update(
            row,
            {
              status: 'Y',
              notes: 'Approved by web',
              updatedBy: value.updatedBy,
              updatedName: value.updatedName,
              updatedDate() {
                return 'NOW()';
              },
            },
            manager,
          );

          const tReqNtiHdrData = await this.tReqNtiHdrService.findOne({
            requestId: row,
          });

          const tReqNtiDtlData = await this.tReqNtiDtlService.findAll({
            requestId: row,
          });

          if (tReqNtiDtlData && tReqNtiDtlData.length > 0) {
            for (const subRow of tReqNtiDtlData) {
              await this.tReqNtiDtlService.update(
                {
                  status: 'Approved',
                  remainingQty: subRow.submittedQty - subRow.confirmQty,
                  updatedBy: value.updatedBy,
                  updatedName: value.updatedName,
                  updatedDate() {
                    return 'NOW()';
                  },
                },
                {
                  ntiId: subRow.ntiId,
                },
                manager,
              );

              const tReqNtiDtlExisting = await this.tReqNtiDtlService.findOne({
                ntiId: subRow.ntiId,
              });

              await this.tLogNtiService.create(
                {
                  requestId: parseInt(row),
                  plant: tReqNtiHdrData?.plant,
                  plantDescription: tReqNtiHdrData?.plantDescription,
                  sloc: subRow.sloc,
                  notes: 'Approved by web' /** tambahan 30 april 2024 */,
                  status: 'Approved',
                  lineItem: subRow.lineItem,
                  materialId: subRow.materialId,
                  materialDescription: subRow.materialDescription,
                  submittedQty: subRow.submittedQty,
                  remainingQty: subRow.submittedQty - subRow.confirmQty,
                  uom: tReqNtiDtlExisting.uom,
                  ntiReason: subRow.ntiReason,
                  scrapReason: subRow.scrapReason,
                  category: subRow.category,
                  costCenter: tReqNtiDtlExisting.costCenter,
                  costCenterDescription:
                    tReqNtiDtlExisting.costCenterDescription,
                  createdBy: value.updatedBy,
                  createdName: value.updatedName,
                  createdDate() {
                    return 'NOW()';
                  },
                },
                '',
                manager,
              );
            }
          }

          const ds: DataSource =
            await this.dynamicDbService.createDynamicConnectionInbox();
          ds.transaction(async (manager2) => {
            await this.tInboxV2Service.delete(
              row,
              'SMU-NTIApproval',
              'NTIApprovalSMU',
              manager2,
            );
          });
        }
      });

      for (const row of value.listRequestId) {
        const senEmailParam: SendEmailApprovalMarketingDto[] = [];
        const tReqNtiHdrData_: TReqNtiHdr =
          await this.tReqNtiHdrService.findOne({
            requestId: row,
          });
        const tReqNtiDtlData_: TReqNtiDtl[] =
          await this.tReqNtiDtlService.findAll({
            requestId: row,
          });
        if (tReqNtiDtlData_ && tReqNtiDtlData_.length > 0) {
          for (const r of tReqNtiDtlData_) {
            senEmailParam.push({
              requestId: row,
              mid: r.materialId.toString(),
              description: r.materialDescription,
              qty: r.submittedQty,
              uom: r.uom,
              ntiReason: r.ntiReason,
              scrapReason: r.scrapReason,
              category: r.category,
              costCenter: r.costCenter,
              brand: r.costCenterDescription,
            });
          }
          if (tReqNtiHdrData_.requestId) {
            const mNtiUserData: MNtiUser = await this.mNtiUserService.findOne({
              plant: tReqNtiHdrData_.plant,
              jabatan: 'Kepala Gudang',
            });
            console.log(
              '🚀 ~ ApprovalMarketingService ~ approval ~ mNtiUserData:',
              mNtiUserData,
            );
            if (mNtiUserData.plant) {
              await this.emailService.actionSendEmailApprove({
                to: mNtiUserData.email,
                title: `NTI Promo No ${row} is waiting for your execution`,
                subTitle:
                  'The following documents are waiting for your execution:',
                clickHere: `${process.env.URL_PORTAL}index.php/inbox`,
                userCC: tReqNtiHdrData_.createdBy,
                type: 'approve',
                body: senEmailParam,
              });
            }
          }
        }
      }

      return {
        status: 'success',
        message: 'Berhasil approve',
      };
    } catch (error) {
      throw error;
    }
  }

  async reject(value: UpdateApprovalMarketingDto): Promise<any> {
    try {
      await this.dataSource.transaction(async (manager) => {
        for (const row of value.listRequestId) {
          await this.tReqNtiHdrService.update(
            row,
            {
              initialStatus: 'X',
              currentUser: '',
              reasonReject: value.reasonReject,
              updatedBy: value.updatedBy,
              updatedName: value.updatedName,
              updatedDate() {
                return 'NOW()';
              },
            },
            manager,
          );

          await this.tReqNtiWorkflowService.update(
            row,
            {
              status: 'X',
              updatedBy: value.updatedBy,
              updatedName: value.updatedName,
              updatedDate() {
                return 'NOW()';
              },
            },
            manager,
          );

          const tReqNtiHdrData = await this.tReqNtiHdrService.findOne({
            requestId: row,
          });

          const tReqNtiDtlData = await this.tReqNtiDtlService.findAll({
            requestId: row,
          });
          console.log(
            '🚀 ~ reject ~ awaitthis.dataSource.transaction ~ tReqNtiDtlData:',
            tReqNtiDtlData,
          );

          if (tReqNtiDtlData && tReqNtiDtlData.length > 0) {
            for (const subRow of tReqNtiDtlData) {
              await this.tReqNtiDtlService.update(
                {
                  status: 'Reject',
                  remainingQty: subRow.submittedQty - subRow.confirmQty,
                  updatedBy: value.updatedBy,
                  updatedName: value.updatedName,
                  updatedDate() {
                    return 'NOW()';
                  },
                },
                {
                  ntiId: subRow.ntiId,
                },
                manager,
              );

              await this.tLogNtiService.create(
                {
                  requestId: parseInt(row),
                  plant: tReqNtiHdrData?.plant,
                  plantDescription: tReqNtiHdrData?.plantDescription,
                  status: 'Rejected',
                  lineItem: subRow.lineItem,
                  materialId: subRow.materialId,
                  materialDescription: subRow.materialDescription,
                  submittedQty: subRow.submittedQty,
                  // remainingQty: subRow.submittedQty - subRow.confirmQty,
                  uom: subRow.uom,
                  ntiReason: subRow.ntiReason,
                  scrapReason: subRow.scrapReason,
                  reasonReject: value.reasonReject,
                  category: subRow.category,
                  costCenter: subRow.costCenter,
                  costCenterDescription: subRow.costCenterDescription,
                  createdBy: value.updatedBy,
                  createdName: value.updatedName,
                  createdDate() {
                    return 'NOW()';
                  },
                },
                '',
                manager,
              );
            }
          }

          const ds: DataSource =
            await this.dynamicDbService.createDynamicConnectionInbox();
          ds.transaction(async (manager2) => {
            await this.tInboxV2Service.delete(
              row,
              'SMU-NTIApproval',
              'NTIApprovalSMU',
              manager2,
            );
          });
        }
      });

      const senEmailParam: SendEmailRejectMarketingDto[] = [];
      for (const row of value.listRequestId) {
        const tReqNtiHdrData_: TReqNtiHdr =
          await this.tReqNtiHdrService.findOne({
            requestId: row,
          });
        const tReqNtiDtlData_: TReqNtiDtl[] =
          await this.tReqNtiDtlService.findAll({
            requestId: row,
          });
        if (tReqNtiDtlData_ && tReqNtiDtlData_.length > 0) {
          for (const r of tReqNtiDtlData_) {
            senEmailParam.push({
              requestId: row,
              mid: r.materialId.toString(),
              description: r.materialDescription,
              qty: r.submittedQty,
              uom: r.uom,
              ntiReason: r.ntiReason,
              scrapReason: r.scrapReason,
              category: r.category,
              costCenter: r.costCenter,
              brand: r.costCenterDescription,
            });
          }
          if (tReqNtiHdrData_.requestId) {
            await this.emailService.actionSendEmailReject({
              // to: 'nopezisaputra.pratama@wingscorp.com', //mNtiUserData.email,
              to: tReqNtiHdrData_.createdBy,
              title: `NTI Promo No ${row} is Rejected by Marketing`,
              subTitle: 'The following documents are rejected:',
              clickHere: `${process.env.URL_PORTAL}index.php/inbox`,
              userCC: '', //,
              type: 'reject',
              reasonReject: tReqNtiHdrData_.reasonReject,
              body: senEmailParam,
            });
          }
        }
      }

      return {
        status: 'success',
        message: 'Berhasil approve',
      };
    } catch (error) {
      throw error;
    }
  }
}
