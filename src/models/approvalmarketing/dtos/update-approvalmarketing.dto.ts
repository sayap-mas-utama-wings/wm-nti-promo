import { IsArray, IsString } from 'class-validator';
import { BaseApprovalMarketingDto } from './base-approvalmarketing.dto';

export class UpdateApprovalMarketingDto extends BaseApprovalMarketingDto {
  @IsString()
  reasonReject: string;

  @IsArray()
  listRequestId: string[];
}
