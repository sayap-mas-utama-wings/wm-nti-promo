import { IsString } from 'class-validator';

export class BaseApprovalMarketingDto {
  @IsString()
  updatedBy: string;

  @IsString()
  updatedName: string;
}
