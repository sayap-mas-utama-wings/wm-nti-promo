export interface SendEmailRejectMarketingDto {
  requestId: string;
  mid: string;
  description: string;
  qty: number;
  uom: string;
  ntiReason: string;
  scrapReason: string;
  category: string;
  costCenter: string;
  brand: string;
}
