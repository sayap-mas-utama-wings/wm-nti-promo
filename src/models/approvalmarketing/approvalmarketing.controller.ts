import { Body, Controller, HttpStatus, Post, Put } from '@nestjs/common';
import { ApprovalMarketingService } from './approvalmarketing.service';
import { UpdateApprovalMarketingDto } from './dtos/update-approvalmarketing.dto';
import { responseError } from 'src/common/helpers/response.helper';

@Controller('approvalmarketing')
export class ApprovalMarketingController {
  constructor(
    private readonly approvalMarketingService: ApprovalMarketingService,
  ) {}

  @Post('listDetailFromInbox')
  async listDetailFromInbox(@Body() listRequestId: string[]): Promise<any> {
    try {
      if (!listRequestId || (listRequestId && listRequestId.length <= 0)) {
        return responseError(
          'list request id harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.approvalMarketingService.listDetailFromInbox(
        listRequestId,
      );
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put('approve')
  async approve(@Body() updateValue: UpdateApprovalMarketingDto): Promise<any> {
    try {
      if (updateValue.listRequestId.length <= 0) {
        return responseError(
          'list request id harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.approvalMarketingService.approval(updateValue);
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put('reject')
  async reject(@Body() updateValue: UpdateApprovalMarketingDto): Promise<any> {
    try {
      if (updateValue.listRequestId.length <= 0) {
        return responseError(
          'list request id harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      if (!updateValue.reasonReject) {
        return responseError(
          'reason wajib di isi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      if (
        updateValue.reasonReject.length < 10 ||
        updateValue.reasonReject.length > 100
      ) {
        return responseError(
          'text reason tidak boleh kurang dari 10 atau melebihi 100 karakter',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.approvalMarketingService.reject(updateValue);
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
