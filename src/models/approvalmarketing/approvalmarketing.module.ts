import { Module } from '@nestjs/common';
import { TReqNtiHdrModule } from '../treqntihdr/treqntihdr.module';
import { TReqntiDtlModule } from '../treqntidtl/treqntidtl.module';
import { TInboxV2Module } from '../inbox/tinboxv2/tinboxv2.module';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { TLogNtiModule } from '../tlognti/tlognti.module';
import { ApprovalMarketingService } from './approvalmarketing.service';
import { ApprovalMarketingController } from './approvalmarketing.controller';
import { TReqNtiWorkflowModule } from '../treqntiworkflow/treqntiworkflow.module';
import { EmailModule } from '../email/email.module';
import { MNtiUserModule } from '../mntiuser/mntiuser.module';
import { MSlocModule } from '../general/msloc/msloc.module';

@Module({
  imports: [
    TReqNtiHdrModule,
    TReqntiDtlModule,
    TInboxV2Module,
    DynamicDbModule,
    TLogNtiModule,
    TReqNtiWorkflowModule,
    EmailModule,
    MNtiUserModule,
    MSlocModule,
  ],
  controllers: [ApprovalMarketingController],
  providers: [ApprovalMarketingService],
  exports: [ApprovalMarketingService],
})
export class ApprovalMarketingModule {}
