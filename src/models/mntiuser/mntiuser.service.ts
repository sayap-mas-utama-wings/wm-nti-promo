import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MNtiUser } from './entities/mntiuser';
import {
  DeleteResult,
  EntityManager,
  FindOptionsOrder,
  FindOptionsWhere,
  InsertResult,
  Like,
  Repository,
  UpdateResult,
} from 'typeorm';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { CreateMNtiUserDto } from './dtos/create-mntiuser.dto';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { UpdateMNtiUserDto } from './dtos/update-mntiuser.dto';

@Injectable()
export class MNtiUserService {
  constructor(
    @InjectRepository(MNtiUser) private mNtiUserRepo: Repository<MNtiUser>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const whereClauseOr = {};
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['plant', 'nama', 'email', 'jabatan'],
          number: ['userId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        if (search?.nama) {
          Object.assign(whereClauseOr, {
            username: Like(`%${search?.nama}%`),
          });
        }

        if (search?.plant) {
          Object.assign(whereClauseOr, {
            plantDesc: Like(`%${search?.plant}%`),
          });
        }

        const columnNames = ['plant', 'nama', 'email', 'jabatan'];

        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.mNtiUserRepo
        .createQueryBuilder('m_nti_user')
        .where(whereClause);

      let hasil1;
      if (Object.keys(whereClauseOr).length > 0) {
        hasil1 = await result.orWhere(whereClauseOr);
      } else {
        hasil1 = result;
      }

      const hasil = await hasil1
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .getManyAndCount();

      return hasil;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MNtiUser>,
    orderCondition?: FindOptionsOrder<MNtiUser>,
  ) {
    try {
      const data = await this.mNtiUserRepo.findOne({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data master nti user not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async create(
    createMNtiUserDto: QueryDeepPartialEntity<CreateMNtiUserDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }

      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(MNtiUser)
          .values(createMNtiUserDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.mNtiUserRepo
          .createQueryBuilder()
          .insert()
          .values(createMNtiUserDto)
          .returning(returning)
          .execute();
      }
      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(
    UpdateMNtiUserDto: QueryDeepPartialEntity<UpdateMNtiUserDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    let updated: UpdateResult;
    if (transactionManager) {
      updated = await transactionManager
        .createQueryBuilder()
        .update(MNtiUser)
        .set({
          nama: UpdateMNtiUserDto.nama,
          email: UpdateMNtiUserDto.email,
          plant: UpdateMNtiUserDto.plant,
          jabatan: UpdateMNtiUserDto.jabatan,
          updatedBy: UpdateMNtiUserDto.updatedBy,
          updatedDate() {
            return 'now()';
          },
          updatedName: UpdateMNtiUserDto.updatedName,
        })
        .where('user_id = :userId', { userId: UpdateMNtiUserDto.userId })
        .returning(returning)
        .execute();
    } else {
      updated = await this.mNtiUserRepo
        .createQueryBuilder()
        .update(MNtiUser)
        .set({
          nama: UpdateMNtiUserDto.nama,
          email: UpdateMNtiUserDto.email,
          plant: UpdateMNtiUserDto.plant,
          plantDesc: UpdateMNtiUserDto.plantDesc,
          jabatan: UpdateMNtiUserDto.jabatan,
          updatedBy: UpdateMNtiUserDto.updatedBy,
          updatedDate() {
            return 'now()';
          },
          updatedName: UpdateMNtiUserDto.updatedName,
        })
        .where('user_id = :userId', { userId: UpdateMNtiUserDto.userId })
        .returning(returning)
        .updateEntity(true)
        .execute();
    }

    if (!updated?.affected) throw new NotFoundException('Update failed!');

    return {
      message: 'Data updated',
      raw: updated?.generatedMaps,
      affected: updated?.affected,
    };
  }

  async delete(
    whereCondition: FindOptionsWhere<MNtiUser>,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      let deleted: DeleteResult;
      if (transactionManager) {
        deleted = await transactionManager
          .createQueryBuilder()
          .delete()
          .from(MNtiUser)
          .where(whereCondition)
          .execute();
      } else {
        deleted = await this.mNtiUserRepo
          .createQueryBuilder()
          .delete()
          .from(MNtiUser)
          .where(whereCondition)
          .execute();
      }

      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      return {
        message: 'Data deleted',
        raw: deleted?.raw,
        affected: deleted?.affected,
      };
    } catch (error) {
      throw error;
    }
  }
}
