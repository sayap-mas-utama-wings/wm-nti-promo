import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { MNtiUserService } from './mntiuser.service';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { CreateMNtiUserDto } from './dtos/create-mntiuser.dto';
import { UpdateMNtiUserDto } from './dtos/update-mntiuser.dto';
import { Response } from 'express';
import { MSettingService } from '../msetting/msetting.service';

@Controller('mntiuser')
export class MNtiUserController {
  constructor(
    private readonly mNtiUserService: MNtiUserService,
    private readonly mSettingService: MSettingService,
  ) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mNtiUserService.findAndPaginate(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne/:userId')
  async findOne(@Param('userId') userId: string) {
    try {
      if (!userId) {
        return responseError(
          'userId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }
      return await this.mNtiUserService.findOne({ userId: userId });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post('uploadData')
  async uploadData(
    @Body() createValue: CreateMNtiUserDto[],
    @Res({ passthrough: true }) response: Response,
  ) {
    try {
      const pesanError = [];
      if (createValue.length == 0) {
        pesanError.push('Data tidak boleh kosong');
      }

      let checkExisting;
      if (createValue.length > 0) {
        let row = 0;
        for (const element of createValue) {
          const urut = row + 2;
          if (!element.plant) {
            pesanError.push(`Row ${urut}, plant harus diisi`);
          } else if (!element.nama) {
            pesanError.push(`Row ${urut}, nik harus diisi`);
          } else if (!element.email) {
            pesanError.push(`Row ${urut}, email harus diisi`);
          } else if (!element.jabatan) {
            pesanError.push(`Row ${urut}, jabatan harus diisi`);
          }

          if (element.jabatan) {
            const findJabatan = await this.mSettingService
              .findOne({
                item: parseInt(element.jabatan) as number,
                settingName: 'nti_jabatan',
              })
              .catch(() => null);
            console.log(findJabatan);

            if (!findJabatan) {
              pesanError.push(`Row ${urut}, data jabatan tidak di temukan`);
            } else {
              const checkExisting = await this.mNtiUserService
                .findOne({
                  plant: element.plant,
                  email: element.email,
                  jabatan: findJabatan.val,
                })
                .catch(() => null);
              console.log(
                '🚀 ~ MNtiUserController ~ awaitcreateValue.forEach ~ checkExisting:',
                checkExisting,
              );
            }
          }

          if (checkExisting && checkExisting.userId) {
            pesanError.push(`Row ${urut}, user id sudah di role tersebut`);
          }

          row++;
        }
      }

      if (pesanError && pesanError.length > 0) {
        response.status(HttpStatus.UNPROCESSABLE_ENTITY).send({
          status: 'error',
          message: pesanError,
        });
        return;
      }

      const resultSuccess = [];
      for (let i = 0; i < createValue.length; i++) {
        const pecahNama = createValue[i].nama.split('-');
        const findJabatan = await this.mSettingService
          .findOne({
            item: parseInt(createValue[i].jabatan) as number,
            settingName: 'nti_jabatan',
          })
          .catch(() => null);
        createValue[i].jabatan = findJabatan.val;

        createValue[i].username = pecahNama[0].trim();
        createValue[i].nama = pecahNama[1].trim();
        const hasil = await this.mNtiUserService.create({
          ...createValue[i],
          createdDate() {
            return 'NOW()';
          },
        });
        resultSuccess.push(hasil);
      }

      return resultSuccess;
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return [
        responseError(
          message,
          error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
        ),
      ];
    }
  }

  @Post('createData')
  async create(@Body() createValue: CreateMNtiUserDto) {
    try {
      if (!createValue.plant) {
        return responseError(
          'plant harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!createValue.nama) {
        return responseError(
          'nik harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!createValue.email) {
        return responseError(
          'email harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!createValue.jabatan) {
        return responseError(
          'jabatan harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      const pecahNama = createValue.nama.split('-');

      createValue.username = pecahNama[0].trim();
      createValue.nama = pecahNama[1].trim();

      const checkExisting = await this.mNtiUserService
        .findOne({
          plant: createValue.plant,
          email: createValue.email,
          jabatan: createValue.jabatan,
        })
        .catch(() => null);

      if (checkExisting) {
        return responseError(
          'user id sudah di role tersebut',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.mNtiUserService.create({
        ...createValue,
        createdDate() {
          return 'NOW()';
        },
      });
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put('updateData')
  async update(@Body() updateValue: UpdateMNtiUserDto) {
    try {
      if (!updateValue.userId) {
        return responseError(
          'userId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.plant) {
        return responseError(
          'plant harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.nama) {
        return responseError(
          'nik harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.email) {
        return responseError(
          'email harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.jabatan) {
        return responseError(
          'jabatan harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      const checkExisting = await this.mNtiUserService
        .findOne({
          plant: updateValue.plant,
          email: updateValue.email,
          jabatan: updateValue.jabatan,
        })
        .catch(() => null);

      if (checkExisting) {
        return responseError(
          'user id sudah di role tersebut',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.mNtiUserService.update(updateValue);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete('deleteData/:userId')
  async delete(@Param('userId') userId: string) {
    try {
      if (!userId) {
        return responseError(
          'userId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }
      return this.mNtiUserService.delete({
        userId: userId,
      });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
