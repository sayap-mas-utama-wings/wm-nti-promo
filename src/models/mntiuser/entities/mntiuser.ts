import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_nti_user')
export class MNtiUser {
  @PrimaryColumn({
    name: 'user_id',
    type: 'int4',
  })
  userId: string;

  @Column({
    name: 'plant',
    type: 'varchar',
    length: 4,
  })
  plant: string;

  @Column({
    name: 'plant_description',
    type: 'varchar',
    length: 50,
  })
  plantDesc: string;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 15,
  })
  username: string;

  @Column({
    name: 'nama',
    type: 'varchar',
    length: 50,
  })
  nama: string;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 50,
  })
  email: string;

  @Column({
    name: 'jabatan',
    type: 'varchar',
    length: 50,
  })
  jabatan: string;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 15,
  })
  createdBy: string;

  @Column({
    name: 'created_name',
    type: 'varchar',
    length: 50,
  })
  createdName: string;

  @Column({
    name: 'created_date',
    type: 'timestamp',
    default: null,
    nullable: true,
  })
  createdDate: string;

  @Column({
    name: 'updated_by',
    type: 'varchar',
    length: 15,
  })
  updatedBy: string;

  @Column({
    name: 'updated_name',
    type: 'varchar',
    length: 50,
  })
  updatedName: string;

  @Column({
    name: 'updated_date',
    type: 'timestamp',
    onUpdate: 'NOW()',
  })
  updatedDate: string;
}
