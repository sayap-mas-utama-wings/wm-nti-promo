import { IsString } from 'class-validator';

export class BaseMNtiUserDto {
  @IsString()
  plant: string;

  @IsString()
  plantDesc: string;

  @IsString()
  username: string;

  @IsString()
  nama: string;

  @IsString()
  email: string;

  @IsString()
  jabatan: string;
}
