import { IsString } from 'class-validator';
import { BaseMNtiUserDto } from './base-mntiuser.dto';

export class UpdateMNtiUserDto extends BaseMNtiUserDto {
  @IsString()
  userId: string;

  @IsString()
  updatedBy: string;

  @IsString()
  updatedName: string;

  @IsString()
  updatedDate: string;
}
