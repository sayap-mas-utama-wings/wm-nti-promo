import { IsString } from 'class-validator';
import { BaseMNtiUserDto } from './base-mntiuser.dto';

export class CreateMNtiUserDto extends BaseMNtiUserDto {
  @IsString()
  createdBy: string;

  @IsString()
  createdName: string;

  @IsString()
  createdDate: string;
}
