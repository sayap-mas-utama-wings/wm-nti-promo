import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MNtiUser } from './entities/mntiuser';
import { MNtiUserService } from './mntiuser.service';
import { MNtiUserController } from './mntiuser.controller';
import { MSettingModule } from '../msetting/msetting.module';

@Module({
  imports: [TypeOrmModule.forFeature([MNtiUser]), MSettingModule],
  controllers: [MNtiUserController],
  providers: [MNtiUserService],
  exports: [MNtiUserService],
})
export class MNtiUserModule {}
