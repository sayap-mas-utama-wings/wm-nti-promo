import { Module } from '@nestjs/common';
import { MReportWrhService } from './mreportwrh.service';
import { MReportWrhController } from './mreportwrh.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MReportWrh } from './entities/mreportwrh.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MReportWrh])],
  controllers: [MReportWrhController],
  providers: [MReportWrhService],
  exports: [MReportWrhService],
})
export class MReportWrhModule {}
