import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  HttpStatus,
  Req,
  UseInterceptors,
} from '@nestjs/common';
import { MReportWrhService } from './mreportwrh.service';
import { CreateMReportWrhDto } from './dto/create-mreportwrh.dto';
import { UpdateMReportWrhDto } from './dto/update-mreportwrh.dto';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import { MReportWrh } from './entities/mreportwrh.entity';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';

@Controller('mreportwrh')
@UseInterceptors(TransformInterceptor)
export class MReportWrhController {
  constructor(private readonly mreportwrhService: MReportWrhService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mreportwrhService.findAndPaginate(
        pagination,
        req.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findList')
  async findList(@Req() req): Promise<MReportWrh[]> {
    try {
      const query = req.query;
      return await this.mreportwrhService.findList({ ...query });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async find(@Req() req): Promise<MReportWrh> {
    try {
      const query = req.query;
      return await this.mreportwrhService.findOne({ ...query });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(@Body() createMReportWrhDto: CreateMReportWrhDto) {
    try {
      return await this.mreportwrhService.create(createMReportWrhDto);
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put()
  async update(@Body() updateMReportWrhDto: UpdateMReportWrhDto) {
    try {
      return await this.mreportwrhService.update(updateMReportWrhDto);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete(':plant/:transplan/:sloc')
  async delete(
    @Param('plant') plant: string,
    @Param('transplan') transplan: string,
    @Param('sloc') sloc: string,
  ) {
    try {
      return await this.mreportwrhService.delete(plant, transplan, sloc);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
