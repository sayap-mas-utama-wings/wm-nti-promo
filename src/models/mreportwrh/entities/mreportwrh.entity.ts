import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('m_report_wrh')
export class MReportWrh {
  @PrimaryColumn({
    name: 'plant',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  plant: string;

  @PrimaryColumn({
    name: 'transplan',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  transplan: string;

  @PrimaryColumn({
    name: 'sloc',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  sloc: string;

  @Column({
    name: 'tipe_report',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  tipeReport: string;

  @Column({
    name: 'tipe_report_desc',
    type: 'varchar',
    length: 30,
    default: '',
    nullable: false,
  })
  tipeReportDesc: string;

  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
    default: () => 'NOW()',
  })
  public createdOn: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @UpdateDateColumn({
    name: 'changed_on',
    type: 'timestamptz',
    default: () => 'NOW()',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public changedOn: Date;

  @Column({
    name: 'changed_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public changedBy: string;
}
