import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMReportWrhDto } from './dto/create-mreportwrh.dto';
import { UpdateMReportWrhDto } from './dto/update-mreportwrh.dto';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { InjectRepository } from '@nestjs/typeorm';
import { MReportWrh } from './entities/mreportwrh.entity';
import { FindOptionsWhere, Repository } from 'typeorm';

@Injectable()
export class MReportWrhService {
  constructor(
    @InjectRepository(MReportWrh)
    private mReportWrhRepo: Repository<MReportWrh>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: ['plant', 'transplan', 'sloc', 'tipeReportDesc'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = ['plant', 'transplan', 'sloc', 'tipeReportDesc'];

        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.mReportWrhRepo.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        skip: (pagination.page - 1) * pagination.limit,
      });
      return result;
    } catch (error) {
      throw error;
    }
  }

  async findList(
    whereCondition: FindOptionsWhere<MReportWrh>,
  ): Promise<MReportWrh[]> {
    try {
      const data: MReportWrh[] = await this.mReportWrhRepo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MReportWrh>,
  ): Promise<MReportWrh> {
    try {
      const data: MReportWrh = await this.mReportWrhRepo.findOne({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async create(createMReportWrhDto: CreateMReportWrhDto): Promise<any> {
    try {
      const inserted = await this.mReportWrhRepo
        .createQueryBuilder()
        .insert()
        .values(createMReportWrhDto)
        .returning('*')
        .execute();
      const returned = { message: 'Data saved', raw: inserted?.generatedMaps };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async update(updateMReportWrhDto: UpdateMReportWrhDto): Promise<any> {
    try {
      const updated = await this.mReportWrhRepo
        .createQueryBuilder()
        .update(MReportWrh)
        .set({
          tipeReport: updateMReportWrhDto.tipeReport,
          tipeReportDesc: updateMReportWrhDto.tipeReportDesc,
          changedBy: updateMReportWrhDto.changedBy,
        })
        .where('plant = :plant AND transplan = :transplan AND sloc = :sloc', {
          plant: updateMReportWrhDto.plant,
          transplan: updateMReportWrhDto.transplan,
          sloc: updateMReportWrhDto.sloc,
        })
        .returning('*')
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async delete(plant: string, transplan: string, sloc: string): Promise<any> {
    try {
      const deleted = await this.mReportWrhRepo
        .createQueryBuilder()
        .delete()
        .from(MReportWrh)
        .where('plant = :plant AND transplan = :transplan AND sloc = :sloc', {
          plant,
          transplan,
          sloc,
        })
        .execute();
      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
