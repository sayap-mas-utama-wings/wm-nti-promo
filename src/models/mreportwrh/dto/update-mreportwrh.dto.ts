import { PartialType } from '@nestjs/mapped-types';
import { BaseMReportWrhDto } from './base-mreportwrh.dto';
import { IsString } from 'class-validator';

export class UpdateMReportWrhDto extends PartialType(BaseMReportWrhDto) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
