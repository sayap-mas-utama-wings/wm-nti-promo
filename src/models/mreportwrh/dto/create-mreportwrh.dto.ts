import { IsString } from 'class-validator';
import { BaseMReportWrhDto } from './base-mreportwrh.dto';

export class CreateMReportWrhDto extends BaseMReportWrhDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
