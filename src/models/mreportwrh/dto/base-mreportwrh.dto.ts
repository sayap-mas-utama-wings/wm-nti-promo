import { IsString } from 'class-validator';

export class BaseMReportWrhDto {
  @IsString()
  plant: string;

  @IsString()
  transplan: string;

  @IsString()
  sloc: string;

  @IsString()
  tipeReport: string;

  @IsString()
  tipeReportDesc: string;
}
