import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMConfigsapDto } from './dtos/create-mconfigsap.dto';
import { UpdateMConfigsapDto } from './dtos/update-mconfigsap.dto';
import { MConfigsap } from './entities/mconfigsap.entity';
import { DeleteResult, FindOptionsWhere, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { DynamicSapService } from 'src/providers/sap/dynamic-sap.service';
import { SapService } from 'nestjs-sap-rfc';
import { isBase64 } from 'class-validator';

@Injectable()
export class MConfigsapService {
  constructor(
    @InjectRepository(MConfigsap)
    private mConfigsapRepo: Repository<MConfigsap>,
    private dynamicSapService: DynamicSapService,
  ) {}

  async findList(
    whereCondition: FindOptionsWhere<MConfigsap>,
  ): Promise<MConfigsap[]> {
    try {
      const data: MConfigsap[] = await this.mConfigsapRepo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MConfigsap>,
  ): Promise<MConfigsap> {
    try {
      const data: MConfigsap = await this.mConfigsapRepo.findOne({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async create(createMConfigsapDto: CreateMConfigsapDto): Promise<any> {
    try {
      const inserted = await this.mConfigsapRepo
        .createQueryBuilder()
        .insert()
        .values(createMConfigsapDto)
        .returning('*')
        .execute();
      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(updateMConfigsapDto: UpdateMConfigsapDto): Promise<any> {
    try {
      const updated = await this.mConfigsapRepo
        .createQueryBuilder()
        .update(updateMConfigsapDto)
        .set({
          host: updateMConfigsapDto.host,
          client: updateMConfigsapDto.client,
          systemNumber: updateMConfigsapDto.systemNumber,
          systemId: updateMConfigsapDto.systemId,
          username: updateMConfigsapDto.username,
          password: updateMConfigsapDto.password,
          lang: updateMConfigsapDto.lang,
          compDesc: updateMConfigsapDto.compDesc,
          changedBy: updateMConfigsapDto.changedBy,
        })
        .where('transplan = :transplan AND plant = :plant', {
          transplan: updateMConfigsapDto.transplan,
          plant: updateMConfigsapDto.plant,
        })
        .returning('*')
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      return {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
    } catch (error) {
      throw error;
    }
  }

  async delete(
    updateMConfigsapDto: UpdateMConfigsapDto,
  ): Promise<DeleteResult> {
    try {
      const deleted: DeleteResult = await this.mConfigsapRepo
        .createQueryBuilder()
        .delete()
        .from(MConfigsap)
        .where('transplan = :transplan AND plant = :plant', {
          transplan: updateMConfigsapDto.transplan,
          plant: updateMConfigsapDto.plant,
        })
        .execute();
      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }

  async createDynamicSapConnection(config: MConfigsap): Promise<SapService> {
    try {
      if (isBase64(config.password)) {
        config.password = Buffer.from(config.password, 'base64').toString(
          'ascii',
        );
      }
      return this.dynamicSapService.createDynamicConnection({
        ashost: config.msHost ? '' : config.host,
        client: config.client,
        user: config.username,
        passwd: config.password,
        sysid: config.systemId,
        sysnr: config.systemNumber,
        lang: config.lang,
        group: config.group,
        mshost: config.msHost,
      });
    } catch (error) {
      throw error;
    }
  }

  async findAndCreateSapConnection(
    plant: string,
    transplan: string,
  ): Promise<SapService> {
    try {
      const configSap: MConfigsap = await this.findOne({
        plant: plant,
        transplan: transplan,
      });

      return this.createDynamicSapConnection(configSap);
    } catch (error) {
      throw error;
    }
  }
}
