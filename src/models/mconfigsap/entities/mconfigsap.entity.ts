import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('m_configsap')
export class MConfigsap {
  @PrimaryColumn({
    name: 'transplan',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  transplan: string;

  @PrimaryColumn({
    name: 'plant',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  plant: string;

  @Column({
    name: 'host',
    type: 'varchar',
    length: 15,
    default: '',
    nullable: false,
  })
  host: string;

  @Column({
    name: 'client',
    type: 'varchar',
    length: 3,
    default: '',
    nullable: false,
  })
  client: string;

  @Column({
    name: 'system_number',
    type: 'varchar',
    length: 2,
    default: '',
    nullable: false,
  })
  systemNumber: string;

  @Column({
    name: 'system_id',
    type: 'varchar',
    length: 3,
    default: '',
    nullable: false,
  })
  systemId: string;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 12,
    default: '',
    nullable: false,
  })
  username: string;

  @Column({
    name: 'password',
    type: 'varchar',
    length: 100,
    default: '',
    nullable: false,
  })
  password: string;

  @Column({
    name: 'lang',
    type: 'varchar',
    length: 2,
    default: '',
    nullable: false,
  })
  lang: string;

  @Column({
    name: 'comp_desc',
    type: 'varchar',
    length: 10,
    default: '',
    nullable: false,
  })
  compDesc: string;

  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
    default: () => 'NOW()',
  })
  public createdOn: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @UpdateDateColumn({
    name: 'changed_on',
    type: 'timestamptz',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public changedOn: Date;

  @Column({
    name: 'changed_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public changedBy: string;

  @Column({
    name: 'group',
    type: 'varchar',
    length: 30,
    default: '',
    nullable: false,
  })
  group: string;

  @Column({
    name: 'ms_host',
    type: 'varchar',
    length: 30,
    default: '',
    nullable: false,
  })
  msHost: string;
}
