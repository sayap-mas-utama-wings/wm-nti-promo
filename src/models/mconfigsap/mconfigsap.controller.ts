import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Delete,
  HttpStatus,
  Req,
  UseInterceptors,
} from '@nestjs/common';
import { MConfigsapService } from './mconfigsap.service';
import { CreateMConfigsapDto } from './dtos/create-mconfigsap.dto';
import { UpdateMConfigsapDto } from './dtos/update-mconfigsap.dto';
import { MConfigsap } from './entities/mconfigsap.entity';
import { responseError } from 'src/common/helpers/response.helper';
import { DeleteResult } from 'typeorm';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';

@Controller('m_configsap')
@UseInterceptors(TransformInterceptor)
export class MConfigsapController {
  constructor(private readonly mConfigsapService: MConfigsapService) {}

  @Get('findList')
  async findList(@Req() req): Promise<MConfigsap[]> {
    try {
      const query = req.query;
      return await this.mConfigsapService.findList({ ...query });
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async findOne(@Req() req): Promise<MConfigsap> {
    try {
      const query: any = req.query;
      return await this.mConfigsapService.findOne({ ...query });
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(@Body() createMConfigsapDto: CreateMConfigsapDto): Promise<any> {
    try {
      return await this.mConfigsapService.create(createMConfigsapDto);
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Patch()
  async update(@Body() updateMConfigsapDto: UpdateMConfigsapDto): Promise<any> {
    try {
      return await this.mConfigsapService.update(updateMConfigsapDto);
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete()
  async delete(
    @Body() updateMConfigsapDto: UpdateMConfigsapDto,
  ): Promise<DeleteResult> {
    try {
      return await this.mConfigsapService.delete(updateMConfigsapDto);
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
