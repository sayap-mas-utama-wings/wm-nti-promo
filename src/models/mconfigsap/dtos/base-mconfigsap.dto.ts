import { IsString } from 'class-validator';

export class BaseMConfigsapDto {
  @IsString()
  transplan: string;

  @IsString()
  plant: string;

  @IsString()
  host: string;

  @IsString()
  client: string;

  @IsString()
  systemNumber: string;

  @IsString()
  systemId: string;

  @IsString()
  username: string;

  @IsString()
  password: string;

  @IsString()
  lang: string;

  @IsString()
  compDesc: string;

  @IsString()
  group: string;

  @IsString()
  msHost: string;
}
