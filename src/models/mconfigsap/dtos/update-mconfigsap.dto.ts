import { PartialType } from '@nestjs/mapped-types';
import { IsString } from 'class-validator';
import { BaseMConfigsapDto } from './base-mconfigsap.dto';

export class UpdateMConfigsapDto extends PartialType(BaseMConfigsapDto) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
