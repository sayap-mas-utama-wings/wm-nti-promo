import { IsString } from 'class-validator';
import { BaseMConfigsapDto } from './base-mconfigsap.dto';

export class CreateMConfigsapDto extends BaseMConfigsapDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
