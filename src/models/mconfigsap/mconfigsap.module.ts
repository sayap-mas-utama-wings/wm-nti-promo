import { Module } from '@nestjs/common';
import { MConfigsapService } from './mconfigsap.service';
import { MConfigsapController } from './mconfigsap.controller';
import { DynamicSapModule } from 'src/providers/sap/dynamic-sap.module';
import { MConfigsap } from './entities/mconfigsap.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [DynamicSapModule, TypeOrmModule.forFeature([MConfigsap])],
  controllers: [MConfigsapController],
  providers: [MConfigsapService],
  exports: [MConfigsapService],
})
export class MConfigsapModule {}
