import { IsString } from 'class-validator';

export class BaseTInboxV2Dto {
  @IsString()
  userId: string;

  @IsString()
  type: string;

  @IsString()
  noDoc: string;

  @IsString()
  status: string;
}
