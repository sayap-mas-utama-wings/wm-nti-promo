import { IsString } from 'class-validator';
import { BaseTInboxV2Dto } from './base-tinboxv2.dto';

export class CreateTInboxv2Dto extends BaseTInboxV2Dto {
  @IsString()
  createdOn: string;
}
