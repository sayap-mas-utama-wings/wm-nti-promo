import { Injectable, NotFoundException } from '@nestjs/common';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { CreateTInboxv2Dto } from './dtos/create-tinboxv2.dto';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import {
  DataSource,
  DeleteResult,
  EntityManager,
  FindOptionsOrder,
  FindOptionsWhere,
  InsertResult,
} from 'typeorm';
import { TInboxV2 } from './entities/tinboxv2';

@Injectable()
export class TInboxV2Service {
  constructor(private dynamicDbService: DynamicDbService) {}

  async create(
    createDto: QueryDeepPartialEntity<CreateTInboxv2Dto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    const ds: DataSource =
      await this.dynamicDbService.createDynamicConnectionInbox();
    const tInboxV2Repo = ds.getRepository(TInboxV2);
    // const queryRunnerDs: QueryRunner = ds.createQueryRunner();

    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }

      let inserted: InsertResult;
      if (transactionManager) {
        // await queryRunnerDs.connect();
        // await queryRunnerDs.startTransaction();

        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(TInboxV2)
          .values(createDto)
          //   .returning(returning)
          .execute();

        // queryRunnerDs.commitTransaction();
      } else {
        inserted = await tInboxV2Repo
          .createQueryBuilder()
          .insert()
          .values(createDto)
          //   .returning(returning)
          .execute();
      }
      return { message: 'Data saved', raw: inserted?.raw };
      return true;
    } catch (error) {
      if (transactionManager) {
        // await queryRunnerDs.rollbackTransaction();
      }

      throw error;
    }
  }

  async findList(
    whereCondition: FindOptionsWhere<TInboxV2>,
    orderBy?: FindOptionsOrder<TInboxV2>,
  ): Promise<TInboxV2[]> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionInbox();
      const mPlantTransplanRepo = ds.getRepository(TInboxV2);

      const data: TInboxV2[] = await mPlantTransplanRepo.find({
        where: whereCondition,
        order: orderBy,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  async delete(
    noDoc: string,
    type: string,
    status: string,
    transactionManager?: EntityManager,
  ): Promise<DeleteResult> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionInbox();
      const tInboxV2Repo = ds.getRepository(TInboxV2);

      const repositoryUpdate = transactionManager
        ? transactionManager
        : tInboxV2Repo;

      const deleted: DeleteResult = await repositoryUpdate
        .createQueryBuilder()
        .delete()
        .from(TInboxV2)
        .where('no_doc = :noDoc AND type = :type AND status = :status', {
          noDoc,
          type,
          status,
        })
        .execute();
      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
