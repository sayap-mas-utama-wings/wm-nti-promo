import { CreateDateColumn, Entity, PrimaryColumn } from 'typeorm';

@Entity('t_inbox_v2')
export class TInboxV2 {
  @PrimaryColumn({
    name: 'user_id',
  })
  userId: string;

  @PrimaryColumn({
    name: 'type',
  })
  type: string;

  @PrimaryColumn({
    name: 'no_doc',
  })
  noDoc: string;

  @PrimaryColumn({
    name: 'status',
  })
  status: string;

  @CreateDateColumn({
    name: 'created_on',
    type: 'date',
    default: () => 'NOW()::date',
  })
  public createdOn: Date;
}
