import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { TInboxV2Service } from './tinboxv2.service';

@Module({
  imports: [DynamicDbModule],
  controllers: [],
  providers: [TInboxV2Service],
  exports: [TInboxV2Service],
})
export class TInboxV2Module {}
