import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { EmailService } from './email.service';
import { responseError } from 'src/common/helpers/response.helper';

@Controller('email')
export class EmailController {
  constructor(private emailService: EmailService) {}

  @Post('sendEmailApprove')
  async sendEmailApprove(@Body() value: any): Promise<any> {
    try {
      return await this.emailService.actionSendEmailApprove(value);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post('sendEmailExecute')
  async sendEmailExecute(@Body() value: any): Promise<any> {
    try {
      return await this.emailService.actionSendEmailExecute(value);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
