import { SendEmailRejectMarketingDto } from 'src/models/approvalmarketing/dtos/send-email-rejectmarketing.dto';

export interface RejectNtiEmailDto {
  title: string;
  subTitle?: string;
  to: string;
  type?: string;
  userCC: string;
  clickHere: string;
  reasonReject: string;
  body: SendEmailRejectMarketingDto[];
}
