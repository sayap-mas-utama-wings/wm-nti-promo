import { SendEmailApprovalMarketingDto } from 'src/models/approvalmarketing/dtos/send-email-approvalmarketing.dto';

export interface ApproveNtiEmailDto {
  title: string;
  subTitle?: string;
  to: string;
  type?: string;
  userCC: string;
  clickHere: string;
  body: SendEmailApprovalMarketingDto[];
}
