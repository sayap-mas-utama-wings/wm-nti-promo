import { SendEmailNtiPromoExecutionDto } from 'src/models/ntipromoexecution/dtos/send-email-ntipromoexecution.dto';

export class ExecuteEmailDto {
  title: string;
  subTitle?: string;
  to: string;
  type?: string;
  userCC: string;
  clickHere: string;
  body: SendEmailNtiPromoExecutionDto[];
}
