import { MailerService } from '@nestjs-modules/mailer/dist';
import { Injectable } from '@nestjs/common';
import { SmuUsersService } from '../joomlaportal/smuusers/smuusers.service';
import { ApproveNtiEmailDto } from './dtos/approve-nti-email.dto';
import { ExecuteEmailDto } from './dtos/execute-email.dto';
import { RejectNtiEmailDto } from './dtos/reject-nti-email.dto';

@Injectable()
export class EmailService {
  constructor(
    private mailerService: MailerService,
    private readonly smuUserService: SmuUsersService,
  ) {}

  async actionSendEmailApprove(value: ApproveNtiEmailDto): Promise<any> {
    console.log('🚀 ~ EmailService ~ actionSendEmailApprove ~ value:', value);
    try {
      const ccEmail = await this.smuUserService.findOne({
        username: value?.userCC,
      });

      const result = await this.mailerService.sendMail({
        to: value.to,
        subject: value.title,
        cc: ccEmail.email,
        template: './approve-nti',
        context: {
          clickHere: value.clickHere,
          subTitle: value.subTitle,
          body: value.body,
        },
      });
      console.log(
        '🚀 ~ EmailService ~ actionSendEmailApprove ~ result:',
        result,
      );

      return result;
    } catch (error) {
      throw error;
    }
  }

  async actionSendEmailReject(value: RejectNtiEmailDto): Promise<any> {
    console.log('🚀 ~ EmailService ~ actionSendEmailReject ~ value:', value);
    try {
      const to = await this.smuUserService.findOne({
        username: value?.to,
      });

      const result = await this.mailerService.sendMail({
        to: to.email,
        subject: value.title,
        template: './reject-nti',
        context: {
          clickHere: value.clickHere,
          subTitle: value.subTitle,
          reason: value.reasonReject,
          body: value.body,
        },
      });
      console.log(
        '🚀 ~ EmailService ~ actionSendEmailReject ~ result:',
        result,
      );

      return result;
    } catch (error) {
      throw error;
    }
  }

  async actionSendEmailExecute(value: ExecuteEmailDto): Promise<any> {
    console.log('🚀 ~ EmailService ~ actionSendEmailExecute ~ value:', value);
    try {
      const result = await this.mailerService.sendMail({
        to: value.to,
        subject: value.title,
        template: './execute-nti',
        context: {
          clickHere: value.clickHere,
          subTitle: value.subTitle,
          body: value.body,
        },
      });
      console.log(
        '🚀 ~ EmailService ~ actionSendEmailApprove ~ result:',
        result,
      );

      return result;
    } catch (error) {
      throw error;
    }
  }
}
