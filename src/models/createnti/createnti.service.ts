import { Injectable } from '@nestjs/common';
import { TReqNtiHdrService } from '../treqntihdr/treqntihdr.service';
import { TReqNtiDtlService } from '../treqntidtl/treqntidtl.service';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { MMaterialService } from '../general/mmaterial/mmaterial.service';
import { MMaterial } from '../general/mmaterial/entities/mmaterial.entity';
import { MCostCenterService } from '../general/mcostcenter/mcostcenter.service';
import { MCostCenter } from '../general/mcostcenter/entities/mcostcenter';
import { ResponseMidCreatentiDto } from './dtos/response-mid-createnti.dto';
import { MConfigsap } from '../mconfigsap/entities/mconfigsap.entity';
import { DynamicSapService } from 'src/providers/sap/dynamic-sap.service';
import { SapRfcObject } from 'nestjs-sap-rfc';
import { SapCreateNti } from './dtos/sap-createnti.dto';
import { MPlantService } from '../general/mplant/mplant.service';
import { MNtiReasonService } from '../mntireason/mntireason.service';
import { MPlantSalesOfficeService } from '../general/mplantsalesoffice/mplantsalesoffice.service';
import { DataSource } from 'typeorm';
import { CreateCreateNtiDto } from './dtos/create-createnti.dto';
import { numberGenerateDigit } from 'src/common/helpers/global.helper';
import { MPicMarketingService } from '../mpicmarketing/mpicmarketing.service';
import { TLogNtiService } from '../tlognti/tlognti.service';
import { TReqNtiWorkflowService } from '../treqntiworkflow/treqntiworkflow.service';
import { SmuUsersService } from '../joomlaportal/smuusers/smuusers.service';
import { TInboxV2Service } from '../inbox/tinboxv2/tinboxv2.service';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { MSlocService } from '../general/msloc/msloc.service';
import { MPlantSalesOffice } from '../general/mplantsalesoffice/entities/mplantsalesoffice';
import { MRfcSalesofficeService } from '../general/mrfcsalesoffice/mrfcsalesoffice.service';
import { MPh3CcService } from '../general/mph3cc/mph3cc.service';

@Injectable()
export class CreateNtiService {
  constructor(
    private readonly tReqNtiHdrService: TReqNtiHdrService,
    private readonly tReqNtiDtlService: TReqNtiDtlService,
    private readonly mMaterialService: MMaterialService,
    private readonly mCostCenterService: MCostCenterService,
    private dynamicSapService: DynamicSapService,
    private readonly mPlantService: MPlantService,
    private readonly mNtiReasonService: MNtiReasonService,
    private readonly mPlantSalesOfficeService: MPlantSalesOfficeService,
    private readonly dataSource: DataSource,
    private readonly mPicMarketingService: MPicMarketingService,
    private readonly tLogNtiService: TLogNtiService,
    private readonly tReqNtiWorkflowService: TReqNtiWorkflowService,
    private readonly smuUsersService: SmuUsersService,
    private readonly tInboxV2Service: TInboxV2Service,
    private readonly dynamicDbService: DynamicDbService,
    private readonly mSlocService: MSlocService,
    private readonly mRfcSalesofficeService: MRfcSalesofficeService,
    private readonly mPh3ccService: MPh3CcService,
  ) {}

  async createData(value: CreateCreateNtiDto): Promise<any> {
    try {
      const sapConnConfig: MConfigsap = new MConfigsap();
      sapConnConfig.host = process.env.SAP_HOST;
      sapConnConfig.client = process.env.SAP_CLIENT;
      // sapConnConfig.username = process.env.SAP_USERNAME;
      // sapConnConfig.password = process.env.SAP_PASSWORD;
      sapConnConfig.systemId = process.env.SAP_SYSTEM_ID;
      sapConnConfig.systemNumber = process.env.SAP_SYSTEM_NUMBER;
      sapConnConfig.lang = process.env.SAP_LANGUAGE;
      sapConnConfig.group = process.env.SAP_GROUP;
      sapConnConfig.msHost = process.env.SAP_MSHOST;

      const mRfcSalesofficeData = await this.mRfcSalesofficeService.findOne(
        {
          // vkbur: process.env.SAP_VKBUR,
          module: process.env.SAP_MODULE,
        },
        {
          vkbur: 'ASC',
        },
      );

      sapConnConfig.username = mRfcSalesofficeData.rfcuser;
      sapConnConfig.password = mRfcSalesofficeData.password;

      console.log(
        '🚀 ~ CreateNtiService ~ createData ~ sapConnConfig:',
        sapConnConfig,
      );

      const paramTable = [];
      for (const row of value.data) {
        console.log('row ', row);
        const detailMid: ResponseMidCreatentiDto = await this.getDetailMid(
          row.mid,
          row.prodhier,
        );
        console.log(
          '🚀 ~ CreateNtiService ~ createData ~ detailMid:',
          detailMid,
        );

        paramTable.push({
          WERKS: row.plant,
          LGORT: row.sloc,
          MATNR: row.mid,
          MEINH: detailMid.meins ? detailMid.meins : '',
          MENGE: row.qty,
        });
      }

      console.log(
        '🚀 ~ CreateNtiService ~ createData ~ paramTable:',
        paramTable,
      );
      const sapConn =
        await this.dynamicSapService.createDynamicSapConnection(sapConnConfig);

      const param: SapRfcObject = {
        T_DATA: paramTable,
      };
      console.log('🚀 ~ CreateNtiService ~ createData ~ param:', param);

      const resultSap = await sapConn.execute<SapCreateNti>(
        'ZFN_WEB_W_NTIPRM_CHK_STOCK',
        param,
      );

      console.log('🚀 ~ CreateNtiService ~ createData ~ resultSap:', resultSap);
      const resultDataError = [];
      if (
        resultSap &&
        resultSap?.T_INSUFFICIENT &&
        resultSap?.T_INSUFFICIENT.length > 0
      ) {
        for (const row of resultSap?.T_INSUFFICIENT) {
          const plantDesc = await this.mPlantService.findOne({
            werks: row?.['WERKS'],
          });
          const slocDesc: MPlantSalesOffice =
            await this.mPlantSalesOfficeService.findOne(
              {
                lgort: row?.['LGORT'],
              },
              {},
              {
                mSalesOffice: true,
              },
            );
          const materialData = await this.mMaterialService.findOne({
            matnr: row?.['MATNR'],
          });
          resultDataError.push({
            plant: row?.['WERKS'],
            plantDesc: plantDesc?.name1,
            sloc: row?.['LGORT'],
            slocDesc: slocDesc.mSalesOffice.descr, //slocDesc?.vkbur,
            mid: row?.['MATNR'],
            materialDesc: materialData.maktx,
            qty: row?.['STOCK'],
            uom: row?.['MEINH'],
          });
        }

        if (resultSap?.E_ISERROR == 'X') {
          return {
            status: 'error',
            message: resultSap.E_MESSAGE,
            data: resultDataError,
          };
        }
      } else if (resultSap?.E_ISERROR == 'X') {
        return {
          status: 'error',
          message: resultSap.E_MESSAGE,
          data: resultDataError,
        };
      }

      // ambil data unique / clear duplicate plant dan sloc
      const plantslocCheck = value.data.map((elem) => ({
        plant: elem.plant,
        sloc: elem.sloc,
      }));

      const listPlantSloc = plantslocCheck.filter((obj, index) => {
        return (
          index ===
          plantslocCheck.findIndex((o) => {
            return obj.plant === o.plant && obj.sloc === o.sloc;
          })
        );
      });

      const resultRow = [];
      let runNo = 0;
      for (const row of listPlantSloc) {
        const resultSubrow = [];
        for (const subRow of value.data) {
          if (row.plant == subRow.plant && row.sloc == subRow.sloc) {
            const detailMid: ResponseMidCreatentiDto = await this.getDetailMid(
              subRow.mid,
              subRow.prodhier,
            );
            resultSubrow.push({
              MATNR: subRow.mid,
              MEINH: detailMid.meins,
              MENGE: subRow.qty,
              ntiReason: subRow.ntiReason,
              scrapReason: subRow.scrapReason,
              costCenter: subRow.costCenter,
              costCenterDesc: subRow.costCenterDesc,
              prodhier: subRow.prodhier,
            });
          }
        }
        const runningId = await this.tReqNtiHdrService.selectMaxId();

        const requestId =
          `${new Date().getFullYear()}${row.plant}` +
          numberGenerateDigit(runningId);
        resultRow.push({
          requestId: parseInt(requestId) + runNo,
          WERKS: row.plant,
          LGORT: row.sloc,
          detail: resultSubrow,
        });
        runNo++;
      }

      console.log('🚀 ~ CreateNtiService ~ createData ~ resultRow:', resultRow);
      const returnSuccess = [];
      await this.dataSource.transaction(async (manager) => {
        let no = 1;
        // let n = 0;
        for (const row of resultRow) {
          const plantDesc = await this.mPlantService.findOne({
            werks: row.WERKS,
          });

          const mSlocData = await this.mSlocService.findOne({
            werksLgort: `${row.WERKS};${row.LGORT}`,
          });

          const mPicMarketing = await this.mPicMarketingService.findAll({
            settingName: 'MKT',
          });
          console.log(
            '🚀 ~ CreateNtiService ~ awaitthis.dataSource.transaction ~ mPicMarketing:',
            mPicMarketing,
          );

          console.log(
            '🚀 ~ CreateNtiService ~ awaitthis.dataSource.transaction ~ requestId:',
            row.requestId,
          );
          returnSuccess.push({
            requestId: row.requestId,
            plant: row.WERKS,
            plantDescription: plantDesc.name1,
            sloc: row.LGORT,
            slocDescription: mSlocData.description,
          });

          await this.tReqNtiHdrService.create(
            {
              requestId: row.requestId,
              plant: row.WERKS,
              plantDescription: plantDesc.name1,
              initialStatus: 'W',
              currentUser: 'MKT',
              createdBy: value.createdBy,
              createdName: value.createdName,
              createdDate() {
                return 'NOW()';
              },
            },
            '',
            manager,
          );

          await this.tReqNtiWorkflowService.create(
            {
              requestId: row.requestId,
              status: 'N',
              userId: 'MKT',
              userName: 'MKT',
              sequence: no.toString(),
              createdBy: value.createdBy,
              createdName: value.createdName,
              createdDate() {
                return 'NOW()';
              },
            },
            '',
            manager,
          );

          if (row.detail && row.detail.length > 0) {
            for (const subRow of row.detail) {
              const ntiReason = await this.mNtiReasonService.findOne({
                reasonId: subRow.ntiReason,
              });
              const detailMid: ResponseMidCreatentiDto =
                await this.getDetailMid(subRow.MATNR, subRow.prodhier);
              console.log(
                '🚀 ~ CreateNtiService ~ awaitthis.dataSource.transaction ~ detailMid:',
                detailMid,
              );
              console.log(
                '🚀 ~ CreateNtiService ~ awaitthis.dataSource.transaction ~ subRow:',
                subRow,
              );
              const materialData = await this.mMaterialService.findOne({
                matnr: subRow?.['MATNR'],
              });
              await this.tReqNtiDtlService.create(
                {
                  requestId: row.requestId,
                  lineItem: no,
                  plant: row.WERKS, // tambahan 21-05-2024
                  status: 'Created',
                  materialId: subRow.MATNR,
                  materialDescription: materialData.maktx,
                  submittedQty: subRow.MENGE,
                  sloc: row.LGORT,
                  uom: subRow.MEINH,
                  // ntiId: subRow.ntiReason,
                  scrapReason: subRow.scrapReason,
                  ntiReason: ntiReason.reason,
                  reasonId: ntiReason.reasonId,
                  costCenter: detailMid.costCenter
                    ? detailMid.costCenter
                    : subRow.costCenter,
                  costCenterDescription: detailMid.costCenterDescription
                    ? detailMid.costCenterDescription
                    : subRow.costCenterDesc,
                  category: detailMid.category,
                  createdBy: value.createdBy,
                  createdName: value.createdName,
                  createdDate() {
                    return 'NOW()';
                  },
                },
                '',
                manager,
              );

              // insert data ke t_log_nti
              await this.tLogNtiService.create(
                {
                  requestId: row.requestId,
                  userId: 'MKT',
                  userName: 'MKT',
                  status: 'Created',
                  lineItem: no,
                  plant: row.WERKS,
                  plantDescription: plantDesc.name1,
                  materialId: subRow.MATNR,
                  materialDescription: materialData.maktx,
                  submittedQty: subRow.MENGE,
                  sloc: row.LGORT,
                  uom: subRow.MEINH,
                  ntiReason: ntiReason.reason,
                  scrapReason: subRow.scrapReason,
                  costCenter: detailMid.costCenter
                    ? detailMid.costCenter
                    : subRow.costCenter,
                  costCenterDescription: detailMid.costCenterDescription
                    ? detailMid.costCenterDescription
                    : subRow.costCenterDesc,
                  category: detailMid.category,
                  createdBy: value.createdBy,
                  createdName: value.createdName,
                  createdDate() {
                    return 'NOW()';
                  },
                },
                '',
                manager,
              );
            }
          }

          // insert data ke t_inbox_v2
          if (mPicMarketing && mPicMarketing.length > 0) {
            const ds: DataSource =
              await this.dynamicDbService.createDynamicConnectionInbox();
            await ds.transaction(async (manager2) => {
              for (const element of mPicMarketing) {
                await this.tInboxV2Service.create(
                  {
                    userId: element.value,
                    noDoc: row.requestId,
                    type: 'SMU-NTIApproval',
                    status: 'NTIApprovalSMU',
                    createdOn() {
                      return 'NOW()';
                    },
                  },
                  '',
                  manager2,
                );
              }
            });
          }
          no++;
          // n++;
        }
      });

      return {
        status: 'success',
        message: 'Berhasil membuat Request ID',
        data: returnSuccess,
      };
    } catch (error) {
      throw error;
    }
  }

  async getData(pagination: PaginationOptions, search?: any): Promise<any> {
    try {
      const tReqNtiHdrData = await this.tReqNtiHdrService.findAndPaginate(
        pagination,
        search,
      );
      console.log(
        '🚀 ~ CreateNtiService ~ getData ~ tReqNtiHdrData:',
        tReqNtiHdrData,
      );
      if (!tReqNtiHdrData || (tReqNtiHdrData && tReqNtiHdrData.length === 0)) {
        return [[], 0];
      }

      return tReqNtiHdrData;
    } catch (error) {
      throw error;
    }
  }

  async getMid(pagination: PaginationOptions, search?: any): Promise<any> {
    try {
      const [results, total] = await this.mMaterialService.findAndPaginate(
        pagination,
        search,
      );

      // clear duplicate data by matnr
      const uniqueArray = results.filter(
        (obj, index, self) =>
          index === self.findIndex((o) => o.matnr === obj.matnr),
      );

      return [uniqueArray, total];
    } catch (error) {
      throw error;
    }
  }

  async getDetailMid(matnr: string, prodhier?: string): Promise<any> {
    try {
      const result: ResponseMidCreatentiDto = new ResponseMidCreatentiDto();

      const whereMaterial = {};
      Object.assign(whereMaterial, {
        matnr,
      });
      if (prodhier) {
        Object.assign(whereMaterial, {
          prodhier,
        });
      }
      const mMaterialData: MMaterial = await this.mMaterialService
        .findOne(whereMaterial)
        .catch(() => null);
      console.log(
        '🚀 ~ CreateNtiService ~ getDetailMid ~ mMaterialData:',
        mMaterialData,
      );

      if (!mMaterialData.prodhier) {
        result.message = `Product hierarchy pada MID ${matnr} tidak boleh kosong`;

        return result;
      }

      const mPh3CcData = await this.mPh3ccService
        .findOne({
          prodh: mMaterialData.prodhier,
        })
        .catch(() => null);
      console.log(
        '🚀 ~ CreateNtiService ~ getDetailMid ~ mMaterialData:',
        mMaterialData,
        mPh3CcData,
      );

      if (mMaterialData && mMaterialData.maktx && mPh3CcData) {
        result.maktx = mMaterialData?.maktx;
        result.meins = mMaterialData?.meins;
        mMaterialData.mPh3Cc = mPh3CcData;
        console.log(
          '🚀 ~ CreateNtiService ~ getDetailMid ~ mMaterialData.mPh3Cc:',
          mMaterialData.mPh3Cc,
        );
        result.category = mMaterialData.mPh3Cc.category;
        result.costCenter = mMaterialData.mPh3Cc.kostl;
        if (mMaterialData.mPh3Cc && mMaterialData.mPh3Cc.kostl) {
          const mCostCenterData: MCostCenter = await this.mCostCenterService
            .findOne({
              kostl: mMaterialData.mPh3Cc.kostl,
              kokrs: 'SMCO',
            })
            .catch(() => null);
          result.costCenterDescription = mCostCenterData.kostl
            ? mCostCenterData.ltext
            : '';
        }
      } else if (!mPh3CcData) {
        result.message = `data prodhier ${mMaterialData.prodhier} di table m_ph3_cc tidak ditemukan`;
      } else {
        result.message = `data mid ${matnr} di table m_material tidak di temukan`;
      }

      return result;
    } catch (error) {
      throw error;
    }
  }

  async getDetailMidUpload(matnr: string, prodhier?: string): Promise<any> {
    try {
      const result: ResponseMidCreatentiDto = new ResponseMidCreatentiDto();

      const whereMaterial = {};
      Object.assign(whereMaterial, {
        matnr,
      });
      if (prodhier) {
        Object.assign(whereMaterial, {
          prodhier,
        });
      }
      const mMaterialData: MMaterial = await this.mMaterialService
        .findOne(whereMaterial)
        .catch(() => null);
      console.log(
        '🚀 ~ CreateNtiService ~ getDetailMid ~ mMaterialData:',
        mMaterialData,
      );

      if (!mMaterialData.prodhier) {
        result['status'] = 'error';
        result.message = `Product hierarchy pada MID ${matnr} tidak boleh kosong`;

        return result;
      }

      const mPh3CcData = await this.mPh3ccService
        .findOne({
          prodh: mMaterialData.prodhier,
        })
        .catch(() => null);
      console.log(
        '🚀 ~ CreateNtiService ~ getDetailMid ~ mMaterialData:',
        mMaterialData,
        mPh3CcData,
      );

      if (mMaterialData && mMaterialData.maktx && mPh3CcData) {
        result.maktx = mMaterialData?.maktx;
        result.meins = mMaterialData?.meins;
        mMaterialData.mPh3Cc = mPh3CcData;
        console.log(
          '🚀 ~ CreateNtiService ~ getDetailMid ~ mMaterialData.mPh3Cc:',
          mMaterialData.mPh3Cc,
        );
        result.category = mMaterialData.mPh3Cc.category;
        result.costCenter = mMaterialData.mPh3Cc.kostl;
        if (mMaterialData.mPh3Cc && mMaterialData.mPh3Cc.kostl) {
          const mCostCenterData: MCostCenter = await this.mCostCenterService
            .findOne({
              kostl: mMaterialData.mPh3Cc.kostl,
              kokrs: 'SMCO',
            })
            .catch(() => null);
          result.costCenterDescription = mCostCenterData.kostl
            ? mCostCenterData.ltext
            : '';
        }
      } else if (!mPh3CcData) {
        result['status'] = 'error';
        result.message = `data prodhier ${mMaterialData.prodhier} di table m_ph3_cc tidak ditemukan`;
      } else {
        result['status'] = 'error';
        result.message = `data mid ${matnr} di table m_material tidak di temukan`;
      }

      return result;
    } catch (error) {
      throw error;
    }
  }

  async getListCostCenter(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      search.kokrs = 'SMCO';
      return await this.mCostCenterService.findAndPagination(
        pagination,
        search,
        [
          'mPh3CcRepo.kokrs',
          'mPh3CcRepo.kostl',
          // 'mPh3CcRepo.bukrs',
          'mPh3CcRepo.ltext',
          // 'mPh3CcRepo.mctxt',
        ],
      );

      // return await this.mCostCenterService.findAll({});
    } catch (error) {
      throw error;
    }
  }

  async getDataFromInbox(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      const tInboxV2Data = await this.tInboxV2Service.findList(
        {
          type: 'SMU-NTIApproval',
          // status: 'NTIApprovalSMU',
          userId: search.userId,
        },
        {
          createdOn: 'DESC',
        },
      );
      console.log(
        '🚀 ~ CreateNtiService ~ getDataFromInbox ~ tInboxV2Data:',
        tInboxV2Data,
      );
      if (!tInboxV2Data || (tInboxV2Data && tInboxV2Data.length === 0)) {
        return [[], 0];
      }

      const listInbox = [];
      for (const row of tInboxV2Data) {
        listInbox.push(row.noDoc);
      }

      // search.requestId = In(listInbox);
      search['initialStatus'] = 'W';
      delete search.userId;
      const tReqNtiHdrData = await this.tReqNtiHdrService.findAndPaginate(
        pagination,
        search,
        listInbox,
      );
      console.log('🚀 ~ CreateNtiService ~ tReqNtiHdrData:', tReqNtiHdrData);

      if (tReqNtiHdrData && tReqNtiHdrData[0].length > 0) {
        let i = 0;
        for (const e of tReqNtiHdrData[0]) {
          tReqNtiHdrData[0][i]['totalMid'] = e['tReqNtiDtl'].length;
          i++;
        }
      }

      return tReqNtiHdrData;
    } catch (error) {
      throw error;
    }
  }
}
