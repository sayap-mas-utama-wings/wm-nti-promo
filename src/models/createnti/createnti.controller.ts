import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { CreateNtiService } from './createnti.service';
import { BaseCreateNtiDto } from './dtos/base-createnti.dto';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import { ResponseUploadCreatentiDto } from './dtos/response-upload-createnti.dto';
import { MPlantService } from '../general/mplant/mplant.service';
import { MNtiReasonService } from '../mntireason/mntireason.service';
import { MPlantSalesOfficeService } from '../general/mplantsalesoffice/mplantsalesoffice.service';
import { CreateCreateNtiDto } from './dtos/create-createnti.dto';
import { MMaterialService } from '../general/mmaterial/mmaterial.service';
import { Response } from 'express';
import { MPh3CcService } from '../general/mph3cc/mph3cc.service';

@Controller('createnti')
export class CreateNtiController {
  constructor(
    private readonly createNtiService: CreateNtiService,
    private readonly mPlantService: MPlantService,
    private readonly mNtiReasonService: MNtiReasonService,
    private readonly mPlantSalesOfficeService: MPlantSalesOfficeService,
    private readonly mMaterialService: MMaterialService,
    private readonly mPh3CcService: MPh3CcService,
  ) {}

  @Get()
  async getData(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.createNtiService.getData(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('mid')
  async getDataMid(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.createNtiService.getMid(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('detailMid')
  async getDetailMid(@Req() req): Promise<any> {
    try {
      if (!req.query?.matnr) {
        return responseError(
          'matnr harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }
      const result = await this.createNtiService.getDetailMid(
        req.query.matnr,
        req.query.prodhier,
      );

      return result;
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('listCostCenter')
  async listCostCenter(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.createNtiService.getListCostCenter(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post('create')
  async createData(
    @Body() createValue: CreateCreateNtiDto,
    @Res({ passthrough: true }) response: Response,
  ): Promise<any> {
    try {
      const pesanError = [];
      if (createValue.data.length == 0) {
        pesanError.push('Data tidak boleh kosong');
      }

      if (createValue.data.length > 0) {
        let row = 0;
        for (const element of createValue.data) {
          if (element.qty.toString().length > 10) {
            const urut = row + 1;
            pesanError.push(
              `Row ${urut}, data qty tidak boleh lebih dari 10 digit`,
            );
          }
          row++;
        }
      }

      if (pesanError && pesanError.length > 0) {
        return {
          status: 'error',
          message: pesanError,
        };
      }
      const result = await this.createNtiService.createData(createValue);

      if (result?.status == 'error') {
        response.status(HttpStatus.BAD_REQUEST).send(result);
        return;
      } else {
        return result;
      }
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post('uploadData')
  async uploadData(
    @Body() createValue: BaseCreateNtiDto[],
    @Res({ passthrough: true }) res: Response,
  ): Promise<any> {
    try {
      const pesanError = [];
      if (createValue.length == 0) {
        pesanError.push('Data tidak boleh kosong');
      }

      if (createValue.length > 0) {
        let row = 0;
        let kolomKosong = '';
        let tidakDitemukan = '';
        let lengthQty = '';
        for (const element of createValue) {
          if (!element.plant) {
            kolomKosong += 'Plant';
          } else {
            const checkPlant = await this.mPlantService
              .findOne({
                werks: element.plant,
              })
              .catch(() => null);
            if (!checkPlant) {
              tidakDitemukan += 'Plant';
            }
          }

          if (!element.mid) {
            kolomKosong += kolomKosong ? ', MID' : 'MID';
          } else {
            const checkMid = await this.mMaterialService
              .findOne({
                matnr: element.mid,
              })
              .catch(() => null);
            if (!checkMid) {
              tidakDitemukan += tidakDitemukan ? ', MID' : 'MID';
            } else {
              const checkMph3Cc = await this.mPh3CcService
                .findOne({
                  prodh: checkMid?.prodhier,
                })
                .catch(() => null);

              if (!checkMph3Cc) {
                tidakDitemukan += tidakDitemukan
                  ? `, data prodhier ${checkMid?.prodhier} di table m_ph3_cc tidak ditemukan`
                  : `data prodhier ${checkMid?.prodhier} di table m_ph3_cc tidak ditemukan`;
              }
            }
          }

          // cek data kosong
          if (!element.ntiReason) {
            kolomKosong += kolomKosong ? ', Nti Reason' : 'Nti Reason';
          } else {
            const checkNtiReason = await this.mNtiReasonService
              .findOne({
                reasonId: element.ntiReason,
              })
              .catch(() => null);
            // cek data tidak ditemukan
            if (!checkNtiReason) {
              tidakDitemukan += tidakDitemukan ? ', Nti Reason' : 'Nti Reason';
            }
          }

          if (!element.qty) {
            kolomKosong += kolomKosong ? ', Qty' : 'Qty';
          } else if (element.qty.toString().length > 10) {
            lengthQty = 'data qty tidak boleh lebih dari 10 digit ';
          }
          if (!element.scrapReason) {
            kolomKosong += kolomKosong ? ', Scrap Reason' : 'Scrap Reason';
          }

          if (!element.sloc) {
            kolomKosong += kolomKosong ? ', Sloc' : 'Sloc';
          } else {
            const checkSloc = await this.mPlantSalesOfficeService
              .findOne({
                lgort: element.sloc,
              })
              .catch(() => null);
            if (!checkSloc) {
              tidakDitemukan += tidakDitemukan ? ', Sloc' : 'Sloc';
            }
          }

          if (kolomKosong) {
            const urut = row + 2;
            pesanError.push('Row ' + urut + `, ${kolomKosong} harus di isi`);
          }
          if (tidakDitemukan) {
            const urut = row + 2;
            pesanError.push(
              'Row ' + urut + `, ${tidakDitemukan} tidak di temukan`,
            );
          }
          if (lengthQty) {
            const urut = row + 2;
            pesanError.push(`Row ${urut} ${lengthQty}`);
          }
          kolomKosong = '';
          tidakDitemukan = '';
          row++;
        }
      }

      if (pesanError && pesanError.length > 0) {
        res.status(HttpStatus.BAD_REQUEST).send({
          status: 'error',
          message: pesanError,
        });
        return;
      }

      const response: ResponseUploadCreatentiDto[] = [];

      for (const row of createValue) {
        const responseOne = new ResponseUploadCreatentiDto();
        const plantDesc = await this.mPlantService.findOne({
          werks: row.plant,
        });
        responseOne.plant = row.plant;
        responseOne.plantDesc = plantDesc.name1;
        responseOne.mid = row.mid;
        responseOne.ntiReason = row.ntiReason;
        responseOne.qty = row.qty;
        responseOne.sloc = row.sloc;
        responseOne.scrapReason = row.scrapReason;

        const detailMid = await this.createNtiService.getDetailMidUpload(
          row.mid,
          row.prodhier,
        );
        responseOne.detailMid = detailMid ?? null;
        response.push(responseOne);
      }

      return response;
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('getDataFromInbox')
  async getDataFromInbox(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.createNtiService.getDataFromInbox(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
