import { Module } from '@nestjs/common';
import { CreateNtiService } from './createnti.service';
import { CreateNtiController } from './createnti.controller';
import { TReqNtiHdrModule } from '../treqntihdr/treqntihdr.module';
import { TReqntiDtlModule } from '../treqntidtl/treqntidtl.module';
import { MMaterialModule } from '../general/mmaterial/mmaterial.module';
import { MCostCenterModule } from '../general/mcostcenter/mcostcenter.module';
import { MPlantModule } from '../general/mplant/mplant.module';
import { MPlantSalesOfficeModule } from '../general/mplantsalesoffice/mplantsalesoffice.module';
import { MNtiReasonModule } from '../mntireason/mntireason.module';
import { DynamicSapModule } from 'src/providers/sap/dynamic-sap.module';
import { MPicMarketingModule } from '../mpicmarketing/mpicmarketing.module';
import { TLogNtiModule } from '../tlognti/tlognti.module';
import { TReqNtiWorkflowModule } from '../treqntiworkflow/treqntiworkflow.module';
import { SmuUserModule } from '../joomlaportal/smuusers/smuusers.module';
import { TInboxV2Module } from '../inbox/tinboxv2/tinboxv2.module';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MSlocModule } from '../general/msloc/msloc.module';
import { MRfcSalesofficeModule } from '../general/mrfcsalesoffice/mrfcsalesoffice.module';
import { MPh3CCModule } from '../general/mph3cc/mph3cc.module';

@Module({
  imports: [
    TReqNtiHdrModule,
    TReqntiDtlModule,
    MMaterialModule,
    MCostCenterModule,
    MPlantModule,
    MPlantSalesOfficeModule,
    MNtiReasonModule,
    DynamicSapModule,
    DynamicDbModule,
    MPicMarketingModule,
    TLogNtiModule,
    TReqNtiWorkflowModule,
    SmuUserModule,
    TInboxV2Module,
    MSlocModule,
    MRfcSalesofficeModule,
    MPh3CCModule,
  ],
  controllers: [CreateNtiController],
  providers: [CreateNtiService],
  exports: [CreateNtiService],
})
export class CreateNtiModule {}
