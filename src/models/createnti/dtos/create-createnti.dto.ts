import { IsString } from 'class-validator';
import { BaseCreateNtiDto } from './base-createnti.dto';

export class CreateCreateNtiDto {
  @IsString()
  createdName: string;

  @IsString()
  createdBy: string;

  data: BaseCreateNtiDto[];
}
