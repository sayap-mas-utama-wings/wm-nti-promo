import { IsNumber, IsString } from 'class-validator';

export class BaseCreateNtiDto {
  @IsString()
  plant: string;

  @IsString()
  plantDesc: string;

  @IsString()
  mid: string;

  @IsString()
  qty: string;

  @IsNumber()
  ntiReason: number;

  @IsString()
  scrapReason: string;

  @IsString()
  sloc: string;

  @IsString()
  costCenter: string;

  @IsString()
  costCenterDesc: string;

  @IsString()
  matDesc: string;

  @IsString()
  prodhier: string;
}
