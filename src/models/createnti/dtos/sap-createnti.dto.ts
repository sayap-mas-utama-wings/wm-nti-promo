import { SapRfcObject } from 'nestjs-sap-rfc';
import { RfcArray } from 'node-rfc';

export interface SapCreateNti extends SapRfcObject {
  readonly T_DATA?: RfcArray;
  readonly T_INSUFFICIENT?: RfcArray;
  readonly E_ISERROR: string;
  readonly E_MESSAGE?: string;
}
