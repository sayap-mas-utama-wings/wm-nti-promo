import { IsString } from 'class-validator';

export class ResponseMidCreatentiDto {
  @IsString()
  maktx: string;

  @IsString()
  meins: string;

  @IsString()
  category: string;

  @IsString()
  scrapReason: string;

  @IsString()
  costCenter: string;

  @IsString()
  costCenterDescription: string;

  @IsString()
  message: string;
}
