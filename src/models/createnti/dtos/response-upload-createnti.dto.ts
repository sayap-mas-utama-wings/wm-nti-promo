import { BaseCreateNtiDto } from './base-createnti.dto';
import { ResponseMidCreatentiDto } from './response-mid-createnti.dto';

export class ResponseUploadCreatentiDto extends BaseCreateNtiDto {
  detailMid: ResponseMidCreatentiDto;
}
