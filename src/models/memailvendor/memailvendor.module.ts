import { Module } from '@nestjs/common';
import { MEmailVendorService } from './memailvendor.service';
import { MEmailVendorController } from './memailvendor.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MEmailVendor } from './entities/memailvendor.entity';
import { MConfigsapModule } from '../mconfigsap/mconfigsap.module';

@Module({
  imports: [TypeOrmModule.forFeature([MEmailVendor]), MConfigsapModule],
  providers: [MEmailVendorService],
  controllers: [MEmailVendorController],
  exports: [MEmailVendorService],
})
export class MEmailVendorModule {}
