import { Column, CreateDateColumn, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_email_vendor')
export class MEmailVendor {
  @PrimaryColumn({
    name: 'company_id',
    type: 'varchar',
    length: 3,
    default: '',
    nullable: false,
  })
  companyId: string;

  @PrimaryColumn({
    name: 'vendor_id',
    type: 'varchar',
    length: 10,
    default: '',
    nullable: false,
  })
  vendorId: string;

  @PrimaryColumn({
    name: 'seq',
    type: 'smallint',
    default: 0,
    nullable: false,
  })
  seq: string;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 241,
    default: '',
    nullable: false,
  })
  email: string;

  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
    default: () => 'NOW()',
  })
  public createdOn: Date;
}
