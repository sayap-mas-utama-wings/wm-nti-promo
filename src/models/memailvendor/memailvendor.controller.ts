import { Controller, Get, HttpStatus, Param, Req } from '@nestjs/common';
import { MEmailVendorService } from './memailvendor.service';
import { responseError } from 'src/common/helpers/response.helper';

@Controller('memailvendor')
export class MEmailVendorController {
  constructor(private readonly mEmailVendorService: MEmailVendorService) {}

  @Get('jobDownloadFromSap')
  async find(@Req() req) {
    try {
      return await this.mEmailVendorService.jobDownloadEmailVendor(
        req.query.companyId,
      );
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
