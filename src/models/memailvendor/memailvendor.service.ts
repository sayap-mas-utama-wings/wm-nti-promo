import { HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MEmailVendor } from './entities/memailvendor.entity';
import { FindOptionsWhere, Repository } from 'typeorm';
import { MConfigsapService } from '../mconfigsap/mconfigsap.service';
import { MConfigsap } from '../mconfigsap/entities/mconfigsap.entity';
import { SapRfcObject, SapService } from 'nestjs-sap-rfc';

@Injectable()
export class MEmailVendorService {
  constructor(
    @InjectRepository(MEmailVendor)
    private mEmailVendorRepo: Repository<MEmailVendor>,
    private readonly mConfigsapService: MConfigsapService,
  ) {}

  async jobDownloadEmailVendor(companyId: string) {
    try {
      let sapConnConfig: MConfigsap = null;
      try {
        sapConnConfig = await this.mConfigsapService.findOne({
          compDesc: companyId,
        });
      } catch (error) {
        if (error?.status === HttpStatus.NOT_FOUND) {
          error.message = 'Gagal menemukan koneksi SAP';
        }
        throw error;
      }

      const rfcFunctionName = 'ZFN_W_DWNLD_EMAIL_VENDOR';
      const param: SapRfcObject = {};

      const sapConn: SapService =
        await this.mConfigsapService.createDynamicSapConnection(sapConnConfig);
      const resultSap: SapRfcObject = await sapConn.execute(
        rfcFunctionName,
        param,
      );
      console.log(
        '🚀 ~ MEmailVendorService ~ jobDownloadEmailVendor ~ resultSap:',
        resultSap,
      );

      if (Array.isArray(resultSap?.T_DATA)) {
        // delete m_email_vendor based on company_id
        await this.mEmailVendorRepo.delete({ companyId: companyId });

        const dataInsert: MEmailVendor[] = [];
        resultSap.T_DATA.forEach((item: any) => {
          const mEmailVendor = new MEmailVendor();
          mEmailVendor.companyId = companyId;
          mEmailVendor.vendorId = item.LIFNR;
          mEmailVendor.seq = item.POSNR;
          mEmailVendor.email = item.SMTP_ADDR;
          dataInsert.push(mEmailVendor);
        });
        await this.mEmailVendorRepo.insert(dataInsert);
      }

      let rtn: any;
      rtn = { message: `Sukses Download Email Vendor ${companyId}` };
      return rtn;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MEmailVendor>,
    hideResponse?: boolean,
  ): Promise<MEmailVendor> {
    try {
      const data: MEmailVendor = await this.mEmailVendorRepo.findOne({
        where: whereCondition,
      });
      if (!hideResponse && !data)
        throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
