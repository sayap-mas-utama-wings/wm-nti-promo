import { Module } from '@nestjs/common';
import { MSlocHoService } from './mslocho.service';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';

@Module({
  imports: [DynamicDbModule],
  controllers: [],
  providers: [MSlocHoService],
  exports: [MSlocHoService],
})
export class MSlocHoModule {}
