import { Injectable } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
// import { MSlocHo } from './entities/mslocho.entity';
// import { Repository } from 'typeorm';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { MSlocHo } from './entities/mslocho.entity';
import { FindOptionsWhere } from 'typeorm';

@Injectable()
export class MSlocHoService {
  constructor(private readonly dynamicDbService: DynamicDbService) {}

  async findOne(whereCondition: FindOptionsWhere<MSlocHo>): Promise<any> {
    try {
      const data = await this.dynamicDbService.createDynamicConnectionGeneral();
      // return (await data.initialize()).getRepository(MSlocHo).findOne({
      return await data.getRepository(MSlocHo).findOne({
        where: whereCondition,
      });
    } catch (error) {
      throw error;
    }
  }
}
