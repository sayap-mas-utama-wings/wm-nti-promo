import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_sloc_ho')
export class MSlocHo {
  @PrimaryColumn({
    name: 'bukrs',
    type: 'varchar',
    length: 4,
    default: '',
  })
  bukrs: string;

  @Column({
    name: 'butxt',
    type: 'varchar',
    length: 25,
    default: '',
  })
  butxt: string;
}
