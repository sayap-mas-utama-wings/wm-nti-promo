import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMConfigdatabaseDto } from './dtos/create-mconfigdatabase.dto';
import { UpdateMConfigdatabaseDto } from './dtos/update-mconfigdatabase.dto';
import { MConfigdatabase } from './entities/mconfigdatabase.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindOptionsWhere, Repository } from 'typeorm';

@Injectable()
export class MConfigdatabaseService {
  constructor(
    @InjectRepository(MConfigdatabase)
    private mConfigdatabaseRepo: Repository<MConfigdatabase>,
  ) {}

  async findList(
    whereCondition: FindOptionsWhere<MConfigdatabase>,
  ): Promise<MConfigdatabase[]> {
    try {
      const data: MConfigdatabase[] = await this.mConfigdatabaseRepo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MConfigdatabase>,
  ): Promise<MConfigdatabase> {
    try {
      const data: MConfigdatabase = await this.mConfigdatabaseRepo.findOne({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async create(
    createMConfigdatabaseDto: CreateMConfigdatabaseDto,
  ): Promise<any> {
    try {
      const inserted = await this.mConfigdatabaseRepo
        .createQueryBuilder()
        .insert()
        .values(createMConfigdatabaseDto)
        .returning('*')
        .execute();
      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(
    updateMConfigdatabaseDto: UpdateMConfigdatabaseDto,
  ): Promise<any> {
    try {
      const updated = await this.mConfigdatabaseRepo
        .createQueryBuilder()
        .update(updateMConfigdatabaseDto)
        .set({
          host: updateMConfigdatabaseDto.host,
          port: updateMConfigdatabaseDto.port,
          dbName: updateMConfigdatabaseDto.dbName,
          username: updateMConfigdatabaseDto.username,
          password: updateMConfigdatabaseDto.password,
          type: updateMConfigdatabaseDto.type,
          compDesc: updateMConfigdatabaseDto.compDesc,
          changedBy: updateMConfigdatabaseDto.changedBy,
        })
        .where('transplan = :transplan AND plant = :plant', {
          transplan: updateMConfigdatabaseDto.transplan,
          plant: updateMConfigdatabaseDto.plant,
        })
        .returning('*')
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      return {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
    } catch (error) {
      throw error;
    }
  }

  async delete(
    updateMConfigdatabaseDto: UpdateMConfigdatabaseDto,
  ): Promise<DeleteResult> {
    try {
      const deleted: DeleteResult = await this.mConfigdatabaseRepo
        .createQueryBuilder()
        .delete()
        .from(MConfigdatabase)
        .where('transplan = :transplan AND plant = :plant', {
          transplan: updateMConfigdatabaseDto.transplan,
          plant: updateMConfigdatabaseDto.plant,
        })
        .execute();
      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
