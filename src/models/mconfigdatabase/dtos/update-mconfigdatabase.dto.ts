import { PartialType } from '@nestjs/mapped-types';
import { IsString } from 'class-validator';
import { BaseMConfigdatabaseDto } from './base-mconfigdatabase.dto';

export class UpdateMConfigdatabaseDto extends PartialType(BaseMConfigdatabaseDto) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
