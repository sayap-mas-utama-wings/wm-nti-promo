import { IsString } from 'class-validator';

export class BaseMConfigdatabaseDto {
  @IsString()
  transplan: string;

  @IsString()
  plant: string;

  @IsString()
  host: string;

  @IsString()
  port: string;

  @IsString()
  dbName: string;

  @IsString()
  username: string;

  @IsString()
  password: string;

  @IsString()
  type: string;

  @IsString()
  compDesc: string;
}
