import { IsString } from 'class-validator';
import { BaseMConfigdatabaseDto } from './base-mconfigdatabase.dto';

export class CreateMConfigdatabaseDto extends BaseMConfigdatabaseDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
