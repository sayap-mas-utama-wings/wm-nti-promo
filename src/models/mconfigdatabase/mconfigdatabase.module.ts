import { Module } from '@nestjs/common';
import { MConfigdatabaseService } from './mconfigdatabase.service';
import { MConfigdatabaseController } from './mconfigdatabase.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MConfigdatabase } from './entities/mconfigdatabase.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MConfigdatabase])],
  controllers: [MConfigdatabaseController],
  providers: [MConfigdatabaseService],
  exports: [MConfigdatabaseService],
})
export class MConfigdatabaseModule {}
