import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Delete,
  Req,
  HttpStatus,
  UseInterceptors,
} from '@nestjs/common';
import { MConfigdatabaseService } from './mconfigdatabase.service';
import { CreateMConfigdatabaseDto } from './dtos/create-mconfigdatabase.dto';
import { UpdateMConfigdatabaseDto } from './dtos/update-mconfigdatabase.dto';
import { responseError } from 'src/common/helpers/response.helper';
import { MConfigdatabase } from './entities/mconfigdatabase.entity';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';
import { DeleteResult } from 'typeorm';

@Controller('mconfigdatabase')
@UseInterceptors(TransformInterceptor)
export class MConfigdatabaseController {
  constructor(
    private readonly mConfigdatabaseService: MConfigdatabaseService,
  ) {}

  @Get()
  async findList(@Req() req): Promise<MConfigdatabase[]> {
    try {
      const query = req.query;
      return await this.mConfigdatabaseService.findList({ ...query });
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne')
  async findOne(@Req() req): Promise<MConfigdatabase> {
    try {
      const query: any = req.query;
      return await this.mConfigdatabaseService.findOne({ ...query });
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(
    @Body() createMConfigdatabaseDto: CreateMConfigdatabaseDto,
  ): Promise<any> {
    try {
      return await this.mConfigdatabaseService.create(createMConfigdatabaseDto);
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Patch()
  async update(
    @Body() updateMConfigdatabaseDto: UpdateMConfigdatabaseDto,
  ): Promise<any> {
    try {
      return await this.mConfigdatabaseService.update(updateMConfigdatabaseDto);
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete()
  async delete(
    @Body() updateMConfigdatabaseDto: UpdateMConfigdatabaseDto,
  ): Promise<DeleteResult> {
    try {
      return await this.mConfigdatabaseService.delete(updateMConfigdatabaseDto);
    } catch (error) {
      console.log(error, 'catch');
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
