import {
  Column,
  CreateDateColumn,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

export class MConfigdatabase {
  @PrimaryColumn({
    name: 'transplan',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  transplan: string;

  @PrimaryColumn({
    name: 'plant',
    type: 'varchar',
    length: 4,
    default: '',
    nullable: false,
  })
  plant: string;

  @Column({
    name: 'host',
    type: 'varchar',
    length: 15,
    default: '',
    nullable: false,
  })
  host: string;

  @Column({
    name: 'port',
    type: 'varchar',
    length: 5,
    default: '',
    nullable: false,
  })
  port: string;

  @Column({
    name: 'db_name',
    type: 'varchar',
    length: 35,
    default: '',
    nullable: false,
  })
  dbName: string;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 15,
    default: '',
    nullable: false,
  })
  username: string;

  @Column({
    name: 'password',
    type: 'varchar',
    length: 100,
    default: '',
    nullable: false,
  })
  password: string;

  @Column({
    name: 'type',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  type: string;

  @Column({
    name: 'comp_desc',
    type: 'varchar',
    length: 10,
    default: '',
    nullable: false,
  })
  compDesc: string;

  @CreateDateColumn({
    name: 'created_on',
    type: 'timestamptz',
    default: () => 'NOW()',
  })
  public createdOn: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public createdBy: string;

  @UpdateDateColumn({
    name: 'changed_on',
    type: 'timestamptz',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public changedOn: Date;

  @Column({
    name: 'changed_by',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  public changedBy: string;
}
