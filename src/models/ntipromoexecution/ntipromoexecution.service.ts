import { Injectable } from '@nestjs/common';
import { TReqNtiHdrService } from '../treqntihdr/treqntihdr.service';
import { TReqNtiDtlService } from '../treqntidtl/treqntidtl.service';
import { TLogNtiService } from '../tlognti/tlognti.service';
import { DataSource, In, Not } from 'typeorm';
import { CompleteNtiPromoExecutionDto } from './dtos/complete-ntipromoexecution.dto';
import { TReqNtiDtl } from '../treqntidtl/entities/treqntidtl.entity';
import { TReqNtiHdr } from '../treqntihdr/entities/treqntihdr.entity';
import { MConfigsap } from '../mconfigsap/entities/mconfigsap.entity';
import { DynamicSapService } from 'src/providers/sap/dynamic-sap.service';
import { SapRfcObject } from 'nestjs-sap-rfc';
import { SapNtiPromoExecutionDto } from './dtos/sap-ntipromoexecution.dto';
import { SapExecuteNtiPromoExecutionDto } from './dtos/sap-execute-ntipromoexecution.dto';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { SendEmailNtiPromoExecutionDto } from './dtos/send-email-ntipromoexecution.dto';
import { MNtiUser } from '../mntiuser/entities/mntiuser';
import { MNtiUserService } from '../mntiuser/mntiuser.service';
import { EmailService } from '../email/email.service';
import { MRfcSalesofficeService } from '../general/mrfcsalesoffice/mrfcsalesoffice.service';
import { MPltGsberSlsoffService } from '../general/mpltgsberslsoff/mpltgsberslsoff.service';

@Injectable()
export class NtiPromoExecutionService {
  constructor(
    private readonly tReqNtiHdrService: TReqNtiHdrService,
    private readonly tReqNtiDtlService: TReqNtiDtlService,
    private readonly tLogNtiService: TLogNtiService,
    private readonly dataSource: DataSource,
    private dynamicSapService: DynamicSapService,
    private readonly mNtiUserService: MNtiUserService,
    private readonly emailService: EmailService,
    private readonly mRfcSalesofficeService: MRfcSalesofficeService,
    private readonly mPltGsberSlsoffService: MPltGsberSlsoffService,
  ) {}

  async getListData(pagination: PaginationOptions, search?: any): Promise<any> {
    try {
      const result = await this.tReqNtiDtlService.findAndPaginateExecuteManual(
        pagination,
        search,
      );

      return result;
    } catch (error) {
      throw error;
    }
  }

  async complete(value: CompleteNtiPromoExecutionDto): Promise<any> {
    try {
      await this.dataSource.transaction(async (manager) => {
        for (const row of value.listRow) {
          const tReqNtiDtlDetail: TReqNtiDtl =
            await this.tReqNtiDtlService.findOne({
              ntiId: row.ntiId,
            });

          const tReqNtiDtlHeader: TReqNtiHdr =
            await this.tReqNtiHdrService.findOne({
              requestId: tReqNtiDtlDetail.requestId,
            });

          await this.tReqNtiHdrService.update(
            tReqNtiDtlDetail.requestId,
            {
              initialStatus: 'C',
              updatedBy: value.createdBy,
              updatedName: value.createdName,
              updatedDate() {
                return 'NOW()';
              },
            },
            manager,
          );

          // const remainingQty = tReqNtiDtlDetail.submittedQty - row.confirmQty;
          await this.tReqNtiDtlService.findAll({
            requestId: tReqNtiDtlDetail.requestId,
            materialId: tReqNtiDtlDetail.materialId,
            ntiReason: tReqNtiDtlDetail.ntiReason,
          });

          const allTreqNtiDtl = await this.tReqNtiDtlService.findAll(
            {
              materialId: parseInt(row.materialId),
              ntiReason: row.ntiReason,
              scrapReason: row.scrapReason,
              category: row.category,
              uom: row.uom,
              remainingQty: Not(0),
              statusRequest: Not(In(['Done Execution', 'Done Complete'])),
              status: Not('Reject'),
              plant: value?.plant,
            },
            {
              // remainingQty: 'DESC',
              requestId: 'ASC',
              ntiId: 'ASC',
            },
          );
          console.log(
            '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ allTreqNtiDtl:',
            allTreqNtiDtl,
          );

          let lastRemainingQty = 0;
          if (allTreqNtiDtl && allTreqNtiDtl.length > 0) {
            let dynamicQty = row.confirmQty;
            for (let l = 0; l < allTreqNtiDtl.length; l++) {
              if (allTreqNtiDtl[l].remainingQty <= dynamicQty) {
                console.log(
                  '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ dynamicQty1:',
                  dynamicQty,
                );
                dynamicQty = dynamicQty - allTreqNtiDtl[l].remainingQty;
                lastRemainingQty = 0;
                await this.tReqNtiDtlService.update(
                  {
                    remainingQty: lastRemainingQty,
                    status: 'Complete',
                    reasonComplete: value.reason,
                    // submittedQty: row.confirmQtyFix,
                    confirmQty: allTreqNtiDtl[l].confirmQty + row.confirmQty,
                    confirmInputQty: allTreqNtiDtl[l].remainingQty, //row.confirmQty,
                  },
                  {
                    ntiId: allTreqNtiDtl[l].ntiId,
                  },
                  manager,
                );
                await this.tLogNtiService.create(
                  {
                    reasonComplete: value.reason,
                    requestId: parseInt(allTreqNtiDtl[l].requestId),
                    status: 'Complete',
                    lineItem: l,
                    plant: value?.plant,
                    plantDescription: tReqNtiDtlHeader.plantDescription,
                    materialId: allTreqNtiDtl[l].materialId,
                    materialDescription: allTreqNtiDtl[l].materialDescription,
                    submittedQty: allTreqNtiDtl[l].submittedQty,
                    remainingQty: lastRemainingQty,
                    sloc: tReqNtiDtlDetail.sloc,
                    uom: allTreqNtiDtl[l].uom,
                    ntiReason: allTreqNtiDtl[l].ntiReason,
                    scrapReason: allTreqNtiDtl[l].scrapReason,
                    category: allTreqNtiDtl[l].category,
                    confirmQty:
                      allTreqNtiDtl[l].confirmQty +
                      allTreqNtiDtl[l].remainingQty, // allTreqNtiDtl[l].confirmQty + row.confirmQty,
                    confirmInputQty: allTreqNtiDtl[l].confirmQty,
                    costCenter: allTreqNtiDtl[l].costCenter,
                    costCenterDescription:
                      allTreqNtiDtl[l].costCenterDescription,
                    createdBy: value.createdBy,
                    createdName: value.createdName,
                    createdDate() {
                      return 'NOW()';
                    },
                  },
                  '',
                  manager,
                );
              } else if (
                allTreqNtiDtl[l].remainingQty > dynamicQty &&
                dynamicQty > 0
              ) {
                console.log(
                  '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ dynamicQty2:',
                  dynamicQty,
                );
                // dynamicQty = dynamicQty - allTreqNtiDtl[l].remainingQty;
                const remainingQty = allTreqNtiDtl[l].remainingQty - dynamicQty;
                const confimQty = remainingQty;
                dynamicQty = 0;
                await this.tReqNtiDtlService.update(
                  {
                    remainingQty: remainingQty,
                    status: 'Partial',
                    reasonComplete: value.reason,
                    confirmQty: confimQty, //allTreqNtiDtl[l].confirmQty + row.confirmQty,
                    confirmInputQty: row.confirmQty,
                  },
                  {
                    ntiId: allTreqNtiDtl[l].ntiId,
                  },
                  manager,
                );
                await this.tLogNtiService.create(
                  {
                    reasonComplete: value.reason,
                    requestId: parseInt(allTreqNtiDtl[l].requestId),
                    status: 'Partial',
                    lineItem: l,
                    plant: value?.plant,
                    plantDescription: tReqNtiDtlHeader.plantDescription,
                    materialId: allTreqNtiDtl[l].materialId,
                    materialDescription: allTreqNtiDtl[l].materialDescription,
                    submittedQty: allTreqNtiDtl[l].submittedQty,
                    remainingQty: remainingQty,
                    confirmQty: confimQty, //allTreqNtiDtl[l].confirmQty + row.confirmQty,
                    sloc: tReqNtiDtlDetail.sloc,
                    uom: allTreqNtiDtl[l].uom,
                    ntiReason: allTreqNtiDtl[l].ntiReason,
                    scrapReason: allTreqNtiDtl[l].scrapReason,
                    confirmInputQty: row.confirmQty, //allTreqNtiDtl[l].confirmQty,
                    costCenter: allTreqNtiDtl[l].costCenter,
                    costCenterDescription:
                      allTreqNtiDtl[l].costCenterDescription,
                    category: allTreqNtiDtl[l].category,
                    createdBy: value.createdBy,
                    createdName: value.createdName,
                    createdDate() {
                      return 'NOW()';
                    },
                  },
                  '',
                  manager,
                );
              } else if (dynamicQty > 0) {
                console.log(
                  '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ dynamicQty3:',
                  dynamicQty,
                );
                lastRemainingQty = allTreqNtiDtl[l].remainingQty - dynamicQty;
                dynamicQty = lastRemainingQty;
                console.log(
                  '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ dynamicQty2: after',
                  dynamicQty,
                );
                await this.tReqNtiDtlService.update(
                  {
                    reasonComplete: value.reason,
                    status: lastRemainingQty <= 0 ? 'Complete' : 'Partial',
                    remainingQty: lastRemainingQty,
                    confirmQty: allTreqNtiDtl[l].confirmQty + row.confirmQty,
                    confirmInputQty: dynamicQty, //row.confirmQty,
                  },
                  {
                    ntiId: allTreqNtiDtl[l].ntiId,
                  },
                  manager,
                );

                await this.tLogNtiService.create(
                  {
                    reasonComplete: value.reason,
                    requestId: parseInt(allTreqNtiDtl[l].requestId),
                    status: lastRemainingQty <= 0 ? 'Complete' : 'Partial',
                    lineItem: l,
                    plant: value?.plant,
                    plantDescription: tReqNtiDtlHeader.plantDescription,
                    materialId: allTreqNtiDtl[l].materialId,
                    materialDescription: allTreqNtiDtl[l].materialDescription,
                    submittedQty: allTreqNtiDtl[l].submittedQty,
                    remainingQty: lastRemainingQty,
                    sloc: tReqNtiDtlDetail.sloc,
                    uom: allTreqNtiDtl[l].uom,
                    ntiReason: allTreqNtiDtl[l].ntiReason,
                    scrapReason: allTreqNtiDtl[l].scrapReason,
                    category: allTreqNtiDtl[l].category,
                    confirmQty: allTreqNtiDtl[l].confirmQty + row.confirmQty,
                    confirmInputQty: row.confirmQty,
                    costCenter: allTreqNtiDtl[l].costCenter,
                    costCenterDescription:
                      allTreqNtiDtl[l].costCenterDescription,
                    createdBy: value.createdBy,
                    createdName: value.createdName,
                    createdDate() {
                      return 'NOW()';
                    },
                  },
                  '',
                  manager,
                );
              }
            }
          }
        }
      });

      await this.dataSource.transaction(async (manager) => {
        for (const r of value?.listRow) {
          const totalAll = await this.tReqNtiDtlService.totalExecuted({
            materialId: parseInt(r.materialId),
            ntiReason: r.ntiReason,
            scrapReason: r.scrapReason,
            category: r.category,
            uom: r.uom,
            plant: value?.plant,
            statusRequest: Not(In(['Done Execution', 'Done Complete'])),
            status: Not(In(['Reject', 'Created', 'Done', 'Complete'])),
          });

          console.log(
            '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ totalAll:',
            totalAll,
          );
          // update status request untuk semua mid yang semua remaining qty nya 0
          if (!totalAll?.total) {
            await this.tReqNtiDtlService
              .update(
                {
                  statusRequest: 'Done Complete',
                },
                {
                  materialId: parseInt(r.materialId),
                  ntiReason: r.ntiReason,
                  scrapReason: r.scrapReason,
                  category: r.category,
                  uom: r.uom,
                  statusRequest: Not(In(['Done Execution', 'Done Complete'])),
                  status: Not(In(['Reject', 'Created', 'Done', 'Complete'])),
                },
                manager,
              )
              .catch(() => null);
          }
        }
      });

      return {
        status: 'success',
        message: 'Sukses Melakukan Complete Data',
      };
    } catch (error) {
      throw error;
    }
  }

  async execute(value: CompleteNtiPromoExecutionDto): Promise<any> {
    try {
      const sapConnConfig: MConfigsap = new MConfigsap();
      sapConnConfig.host = process.env.SAP_HOST;
      sapConnConfig.client = process.env.SAP_CLIENT;
      sapConnConfig.systemId = process.env.SAP_SYSTEM_ID;
      sapConnConfig.systemNumber = process.env.SAP_SYSTEM_NUMBER;
      sapConnConfig.lang = process.env.SAP_LANGUAGE;
      sapConnConfig.group = process.env.SAP_GROUP;
      sapConnConfig.msHost = process.env.SAP_MSHOST;

      const mPltGsberSlsoffData = await this.mPltGsberSlsoffService.findOne({
        werks: value?.plant,
      });

      const mRfcSalesofficeData = await this.mRfcSalesofficeService
        .findOne({
          // vkbur: value.plant ?? process.env.SAP_VKBUR,
          vkbur: mPltGsberSlsoffData.vkbur,
          module: process.env.SAP_MODULE,
        })
        .catch(() => null);

      console.log(
        '🚀 ~ NtiPromoExecutionService ~ execute ~ mRfcSalesofficeData:',
        mRfcSalesofficeData,
      );
      if (!mRfcSalesofficeData) {
        return {
          status: 'error',
          message: `data rfc salesoffice dengan plant ${value.plant} tidak ditemukan`,
          data: null,
        };
      }

      sapConnConfig.username = mRfcSalesofficeData.rfcuser;
      sapConnConfig.password = mRfcSalesofficeData.password;

      const paramTable: SapExecuteNtiPromoExecutionDto[] = [];
      const paramUpdate = [];

      for (const row of value.listRow) {
        const tReqNtiDtlDetail: TReqNtiDtl =
          await this.tReqNtiDtlService.findOne({
            ntiId: row.ntiId,
            plant: value.plant,
          });

        const tReqNtiDtlHeader: TReqNtiHdr =
          await this.tReqNtiHdrService.findOne({
            requestId: tReqNtiDtlDetail.requestId,
            plant: value.plant,
          });

        const confirmQtyFix = tReqNtiDtlDetail.confirmQty + row.confirmQty;
        paramUpdate.push({
          requestId: tReqNtiDtlDetail.requestId,
          ntiId: tReqNtiDtlDetail.ntiId,
          confirmQtyFix: confirmQtyFix,
          confirmQty: row.confirmQty,
          remainingQty: tReqNtiDtlDetail.submittedQty - confirmQtyFix,
          lineItem: tReqNtiDtlDetail.lineItem,
          plant: tReqNtiDtlHeader.plant,
          plantDescription: tReqNtiDtlHeader.plantDescription,
          materialId: tReqNtiDtlDetail.materialId,
          materialDescription: tReqNtiDtlDetail.materialDescription,
          submittedQty: row.submittedQty, //tReqNtiDtlDetail.submittedQty,
          sloc: tReqNtiDtlDetail.sloc,
          uom: tReqNtiDtlDetail.uom,
          ntiReason: tReqNtiDtlDetail.ntiReason,
          scrapReason: tReqNtiDtlDetail.scrapReason,
          category: tReqNtiDtlDetail.category,
          costCenter: tReqNtiDtlDetail.costCenter,
          costCenterDescription: tReqNtiDtlDetail.costCenterDescription,
        });
      } // for

      for (const row of value.listRow) {
        const tReqNtiDtlDetail: TReqNtiDtl =
          await this.tReqNtiDtlService.findOne({
            ntiId: row.ntiId,
            plant: value.plant,
          });

        const tReqNtiDtlHeader: TReqNtiHdr =
          await this.tReqNtiHdrService.findOne({
            requestId: tReqNtiDtlDetail.requestId,
            plant: value.plant,
          });

        const totalAll = await this.tReqNtiDtlService.findAll({
          materialId: parseInt(row.materialId),
          ntiReason: row.ntiReason,
          scrapReason: row.scrapReason,
          category: row.category,
          uom: row.uom,
          plant: value.plant,
          // statusRequest: Not('Done Execution'),
          statusRequest: Not(In(['Done Execution', 'Done Complete'])),
        });

        const listRequestId1 = totalAll.filter(
          (obj, index, self) =>
            index === self.findIndex((o) => o.requestId === obj.requestId),
        );

        paramTable.push({
          MATNR: tReqNtiDtlDetail.materialId.toString(),
          MENGE: row.confirmQty,
          MEINH: tReqNtiDtlDetail.uom,
          WERKS: tReqNtiDtlHeader.plant,
          LGORT: tReqNtiDtlDetail.sloc,
          // UMLGO: '',
          SGTXT: listRequestId1.toString(), //tReqNtiDtlDetail.requestId,
          REASON_ID: tReqNtiDtlDetail.reasonId ?? 0,
          REASON_SCRAP: tReqNtiDtlDetail.scrapReason,
        });
      } // for

      const sapConn =
        await this.dynamicSapService.createDynamicSapConnection(sapConnConfig);

      const param: SapRfcObject = {
        I_BKTXT: value.createdName,
        T_ITEM: paramTable,
      };
      console.log('🚀 ~ NtiPromoExecutionService ~ execute ~ param:', param);

      const resultSap = await sapConn.execute<SapNtiPromoExecutionDto>(
        'ZFN_WEB_W_NTIPRM_TRF_POST',
        param,
      );
      // .catch((err) => console.log(err));

      console.log('resultSap: ', resultSap);

      if (
        (resultSap && resultSap?.E_ISERROR == 'X') ||
        (resultSap &&
          resultSap?.T_RETURN.length > 0 &&
          resultSap?.T_RETURN[0]?.['MESSAGE'])
      ) {
        return {
          status: 'error',
          message: resultSap?.T_RETURN[0]?.['MESSAGE'],
          data: resultSap?.T_RETURN,
        };
        // return resultSap?.T_RETURN;
      }

      let no = 0;
      const listNtiId: number[] = [];
      await this.dataSource.transaction(async (manager) => {
        for (const r of paramUpdate) {
          await this.tReqNtiHdrService.update(
            r.requestId,
            {
              initialStatus: 'P',
              updatedBy: value.createdBy,
              updatedName: value.createdName,
              updatedDate() {
                return 'NOW()';
              },
            },
            manager,
          );

          const allTreqNtiDtl = await this.tReqNtiDtlService.findAll(
            {
              materialId: r.materialId,
              ntiReason: r.ntiReason,
              scrapReason: r.scrapReason,
              category: r.category,
              uom: r.uom,
              remainingQty: Not(0),
              statusRequest: Not(In(['Done Execution', 'Done Complete'])),
              status: Not('Reject'),
              plant: value?.plant,
            },
            {
              requestId: 'ASC',
              ntiId: 'ASC',
            },
          );
          console.log(
            '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ allTreqNtiDtl:',
            allTreqNtiDtl,
          );

          let lastRemainingQty = 0;
          if (allTreqNtiDtl && allTreqNtiDtl.length > 0) {
            let dynamicQty = r.confirmQty;
            for (let l = 0; l < allTreqNtiDtl.length; l++) {
              if (allTreqNtiDtl[l].remainingQty <= dynamicQty) {
                console.log(
                  '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ dynamicQty1:',
                  dynamicQty,
                );
                // const confirmInputQty =
                //   r.confirmQty - allTreqNtiDtl[l].remainingQty;
                lastRemainingQty = 0;
                // dynamicQty = 0;
                await this.tReqNtiDtlService.update(
                  {
                    remainingQty: lastRemainingQty,
                    status: 'Done',
                    confirmQty:
                      allTreqNtiDtl[l].confirmQty +
                      allTreqNtiDtl[l].remainingQty,
                    confirmInputQty: allTreqNtiDtl[l].remainingQty,
                    matDoc: resultSap?.E_MBLNR,
                    matDocYear: resultSap?.E_MJAHR,
                    postingDate() {
                      return 'NOW()';
                    },
                  },
                  {
                    ntiId: allTreqNtiDtl[l].ntiId,
                  },
                  manager,
                );
                await this.tLogNtiService.create(
                  {
                    matDoc: resultSap?.E_MBLNR,
                    matDocYear: resultSap?.E_MJAHR,
                    postingDate() {
                      return 'NOW()';
                    },
                    reasonComplete: value.reason,
                    requestId: parseInt(allTreqNtiDtl[l].requestId),
                    status: lastRemainingQty <= 0 ? 'Done' : 'Partial',
                    lineItem: no,
                    plant: value.plant,
                    plantDescription: r.plantDescription,
                    materialId: r.materialId,
                    materialDescription: r.materialDescription,
                    submittedQty: allTreqNtiDtl[l].submittedQty,
                    remainingQty: lastRemainingQty,
                    confirmInputQty: r.confirmQty,
                    sloc: r.sloc,
                    uom: r.uom,
                    ntiReason: r.ntiReason,
                    scrapReason: r.scrapReason,
                    costCenter: r.costCenter,
                    costCenterDescription: r.costCenterDescription,
                    category: r.category,
                    confirmQty:
                      allTreqNtiDtl[l].confirmQty +
                      allTreqNtiDtl[l].remainingQty,
                    createdBy: value.createdBy,
                    createdName: value.createdName,
                    createdDate() {
                      return 'NOW()';
                    },
                  },
                  '',
                  manager,
                );
                dynamicQty = dynamicQty - allTreqNtiDtl[l].remainingQty;
                listNtiId.push(allTreqNtiDtl[l].ntiId);
              } else if (
                allTreqNtiDtl[l].remainingQty > dynamicQty &&
                dynamicQty > 0
              ) {
                console.log(
                  '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ dynamicQty 2 :',
                  dynamicQty,
                  l,
                );
                const remainingQty = allTreqNtiDtl[l].remainingQty - dynamicQty;
                let confimQty = remainingQty;
                confimQty = dynamicQty;

                dynamicQty = 0;
                await this.tReqNtiDtlService.update(
                  {
                    remainingQty: remainingQty,
                    status: 'Partial',
                    confirmQty: confimQty,
                    confirmInputQty: confimQty,
                    matDoc: resultSap?.E_MBLNR,
                    matDocYear: resultSap?.E_MJAHR,
                    postingDate() {
                      return 'NOW()';
                    },
                  },
                  {
                    ntiId: allTreqNtiDtl[l].ntiId,
                  },
                  manager,
                );
                await this.tLogNtiService.create(
                  {
                    matDoc: resultSap?.E_MBLNR,
                    matDocYear: resultSap?.E_MJAHR,
                    postingDate() {
                      return 'NOW()';
                    },
                    reasonComplete: value.reason,
                    requestId: parseInt(allTreqNtiDtl[l].requestId),
                    status: 'Partial',
                    lineItem: no,
                    plant: value.plant,
                    plantDescription: r.plantDescription,
                    materialId: r.materialId,
                    materialDescription: r.materialDescription,
                    submittedQty: allTreqNtiDtl[l].submittedQty,
                    remainingQty: remainingQty,
                    confirmQty: confimQty,
                    sloc: r.sloc,
                    uom: r.uom,
                    ntiReason: r.ntiReason,
                    scrapReason: r.scrapReason,
                    costCenter: r.costCenter,
                    costCenterDescription: r.costCenterDescription,
                    category: r.category,
                    createdBy: value.createdBy,
                    createdName: value.createdName,
                    createdDate() {
                      return 'NOW()';
                    },
                  },
                  '',
                  manager,
                );
                listNtiId.push(allTreqNtiDtl[l].ntiId);
              } else if (dynamicQty > 0) {
                console.log(
                  '🚀 ~ NtiPromoExecutionService ~ awaitthis.dataSource.transaction ~ dynamicQty3:',
                  dynamicQty,
                );
                lastRemainingQty = allTreqNtiDtl[l].remainingQty - dynamicQty;
                dynamicQty = lastRemainingQty;
                await this.tReqNtiDtlService.update(
                  {
                    remainingQty: lastRemainingQty,
                    status: lastRemainingQty <= 0 ? 'Done' : 'Partial',
                    confirmQty: r.confirmQtyFix,
                    confirmInputQty: dynamicQty,
                    matDoc: resultSap?.E_MBLNR,
                    matDocYear: resultSap?.E_MJAHR,
                    postingDate() {
                      return 'NOW()';
                    },
                  },
                  {
                    ntiId: allTreqNtiDtl[l].ntiId,
                  },
                  manager,
                );
                await this.tLogNtiService.create(
                  {
                    matDoc: resultSap?.E_MBLNR,
                    matDocYear: resultSap?.E_MJAHR,
                    postingDate() {
                      return 'NOW()';
                    },
                    reasonComplete: value.reason,
                    requestId: parseInt(allTreqNtiDtl[l].requestId),
                    status: lastRemainingQty <= 0 ? 'Done' : 'Partial',
                    lineItem: no,
                    plant: value.plant,
                    plantDescription: r.plantDescription,
                    materialId: r.materialId,
                    materialDescription: r.materialDescription,
                    submittedQty: allTreqNtiDtl[l].submittedQty,
                    remainingQty: lastRemainingQty,
                    confirmInputQty: r.confirmQty,
                    sloc: r.sloc,
                    uom: r.uom,
                    ntiReason: r.ntiReason,
                    scrapReason: r.scrapReason,
                    costCenter: r.costCenter,
                    costCenterDescription: r.costCenterDescription,
                    category: r.category,
                    confirmQty: r.confirmQtyFix,
                    createdBy: value.createdBy,
                    createdName: value.createdName,
                    createdDate() {
                      return 'NOW()';
                    },
                  },
                  '',
                  manager,
                );
                listNtiId.push(allTreqNtiDtl[l].ntiId);
              }
            }
          }

          const totalAll2 = await this.tReqNtiDtlService.totalExecuted({
            materialId: r.materialId,
            ntiReason: r.ntiReason,
            scrapReason: r.scrapReason,
            category: r.category,
            uom: r.uom,
            statusRequest: Not(In(['Done Execution', 'Done Complete'])),
            plant: value.plant,
          });
          console.log('totalAll2', totalAll2);

          no++;
        }
      });

      await this.dataSource.transaction(async (manager) => {
        for (const r of paramUpdate) {
          const totalAll = await this.tReqNtiDtlService.totalExecuted({
            materialId: r.materialId,
            ntiReason: r.ntiReason,
            scrapReason: r.scrapReason,
            category: r.category,
            uom: r.uom,
            // statusRequest: Not('Done Execution'),
            statusRequest: Not(In(['Done Execution', 'Done Complete'])),
            status: Not('Reject'),
          });
          console.log('totalAll', totalAll);

          // update status request untuk semua mid yang semua remaining qty nya 0
          if (totalAll.total <= 0) {
            await this.tReqNtiDtlService.update(
              {
                statusRequest: 'Done Execution',
              },
              {
                materialId: r.materialId,
                ntiReason: r.ntiReason,
                scrapReason: r.scrapReason,
                category: r.category,
                uom: r.uom,
                statusRequest: Not('Done Execution'),
              },
              manager,
            );
          }
        }
      });

      /** kirim ke email
       * menghilangkan duplikat data request id
       * supaya tidak kirim ke email 2x dengan request id yang sama
       */
      const paramKirimEmail = paramUpdate.filter(
        (obj, index, self) =>
          index === self.findIndex((o) => o.requestId === obj.requestId),
      );
      for (const row of paramKirimEmail) {
        const senEmailParam: SendEmailNtiPromoExecutionDto[] = [];
        const tReqNtiHdrData_: TReqNtiHdr =
          await this.tReqNtiHdrService.findOne({
            requestId: row.requestId,
          });

        const tReqDtlData_ = await this.tReqNtiDtlService.findAll({
          ntiId: In(listNtiId),
        });
        console.log(
          '🚀 ~ NtiPromoExecutionService ~ execute ~ tReqDtlData_:',
          tReqDtlData_,
        );

        if (tReqDtlData_ && tReqDtlData_.length > 0) {
          for (const r of tReqDtlData_) {
            senEmailParam.push({
              requestId: r.requestId,
              mid: r.materialId,
              materialDescription: r.materialDescription,
              uom: r.uom,
              matdoc: r.matDoc ? parseInt(r.matDoc) : 0,
              qty: r.confirmInputQty,
            });
          }
          if (tReqNtiHdrData_.requestId) {
            const mNtiUserData: MNtiUser = await this.mNtiUserService.findOne({
              plant: tReqNtiHdrData_.plant,
              jabatan: 'Kepala Gudang',
            });
            if (mNtiUserData.plant) {
              await this.emailService.actionSendEmailExecute({
                // to: 'nopezisaputra.pratama@wingscorp.com', //mNtiUserData.email,
                to: mNtiUserData.email,
                title: `NTI Promosi ${senEmailParam[0].requestId} is successfully created`,
                subTitle: 'The following documents successfully created',
                clickHere: `${process.env.URL_PORTAL}index.php/`,
                userCC: tReqNtiHdrData_.createdBy,
                type: 'execute',
                body: senEmailParam,
              });
            }
          }
        }
      }

      return {
        status: 'success',
        message: `Sukses Melakukan Execute NTI matdoc ${resultSap?.E_MBLNR} year: ${resultSap?.E_MJAHR}`,
        data: resultSap?.T_RETURN,
      };
    } catch (error) {
      throw error;
    }
  }

  async warehouseValidation(nik: string): Promise<any> {
    try {
      const ntiUserData = await this.mNtiUserService
        .findOne({
          // plant: plant,
          username: nik,
          jabatan: 'Kepala Gudang',
        })
        .catch(() => null);

      console.log('ntiUserData', ntiUserData);

      if (!ntiUserData) {
        return {
          status: 'error',
          message: `nik ${nik} tidak di temukan / bukan kepala gudang`,
        };
      } else {
        return {
          status: 'success',
          message: `plant ${ntiUserData?.plant} dengan nik ${nik} adalah kepala gudang`,
          plant: ntiUserData?.plant,
        };
      }
    } catch (error) {
      throw error;
    }
  }
}
