export class SendEmailNtiPromoExecutionDto {
  requestId: number;
  mid: number;
  materialDescription: string;
  qty: number;
  uom: string;
  matdoc: number;
}
