import { IsString } from 'class-validator';
import { BaseNtiPromoExecutionDto } from './base-ntipromoexecution.dto';

export class CompleteNtiPromoExecutionDto {
  @IsString()
  reason: string;

  @IsString()
  plant: string;

  @IsString()
  createdBy: string;

  @IsString()
  createdName: string;

  listRow: BaseNtiPromoExecutionDto[];
}
