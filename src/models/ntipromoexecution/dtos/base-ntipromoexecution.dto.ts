import { IsNumber, IsString } from 'class-validator';

export class BaseNtiPromoExecutionDto {
  @IsNumber()
  ntiId: number;

  @IsNumber()
  submittedQty: number;

  @IsNumber()
  confirmQty: number;

  @IsNumber()
  remainingQty: number;

  @IsString()
  materialId: string;

  @IsString()
  materialdescription: string;

  @IsString()
  ntiReason: string;

  @IsString()
  scrapReason: string;

  @IsString()
  category: string;

  @IsString()
  uom: string;
}
