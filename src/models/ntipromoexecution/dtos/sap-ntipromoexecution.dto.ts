import { SapRfcObject } from 'nestjs-sap-rfc';
import { RfcArray } from 'node-rfc';

export interface SapNtiPromoExecutionDto extends SapRfcObject {
  readonly I_BKTXT?: string;
  readonly T_ITEM?: RfcArray;
  readonly T_RETURN?: RfcArray;
  readonly E_MBLNR?: string;
  readonly E_MJAHR?: string;
  readonly E_ISERROR: string;
}
