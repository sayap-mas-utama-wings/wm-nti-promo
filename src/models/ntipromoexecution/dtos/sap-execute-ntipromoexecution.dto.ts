import { SapRfcObject } from 'nestjs-sap-rfc';

export interface SapExecuteNtiPromoExecutionDto extends SapRfcObject {
  MATNR: string;
  MENGE: number;
  MEINH: string;
  WERKS: string;
  LGORT: string;
  SGTXT: string;
  REASON_ID: number;
  REASON_SCRAP: string;
}
