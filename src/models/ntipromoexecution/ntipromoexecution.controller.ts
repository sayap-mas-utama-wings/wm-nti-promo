import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { NtiPromoExecutionService } from './ntipromoexecution.service';
import { CompleteNtiPromoExecutionDto } from './dtos/complete-ntipromoexecution.dto';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { TReqNtiDtlService } from '../treqntidtl/treqntidtl.service';
import { TReqNtiDtl } from '../treqntidtl/entities/treqntidtl.entity';
import { Response } from 'express';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';

@Controller('ntipromoexecution')
export class NtiPromoExecutionController {
  constructor(
    private readonly ntiPromoExecutionService: NtiPromoExecutionService,
    private readonly tReqNtiDtlService: TReqNtiDtlService,
  ) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.ntiPromoExecutionService.getListData(
        pagination,
        req?.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('warehouseValidation')
  async warehouseValidation(@Req() req) {
    try {
      return await this.ntiPromoExecutionService.warehouseValidation(
        req?.query.nik,
      );
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put('actionComplete')
  async actionComplete(
    @Body() value: CompleteNtiPromoExecutionDto,
    @Res({ passthrough: true }) response: Response,
  ): Promise<any> {
    try {
      if (value?.listRow.length < 0) {
        return responseError(
          'mohon lengkapi data',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      const rowError = [];
      for (const row of value.listRow) {
        const tReqNtiDtlData: TReqNtiDtl = await this.tReqNtiDtlService.findOne(
          {
            ntiId: row.ntiId,
          },
        );

        if (tReqNtiDtlData.ntiId && row.confirmQty > row.remainingQty) {
          rowError.push(row);
        }
      }

      if (rowError.length > 0) {
        response.status(HttpStatus.UNPROCESSABLE_ENTITY).send({
          status: 'error',
          message:
            'Mohon periksa kembali, Confirm Qty, tidak dapat melebihi Remaining Qty',
          data: rowError,
        });
        return;
      }

      if (!value?.plant) {
        response.status(HttpStatus.UNPROCESSABLE_ENTITY).send({
          status: 'error',
          message: 'Plant tidak boleh kosong',
          data: rowError,
        });
        return;
      }

      const result = await this.ntiPromoExecutionService.complete(value);

      response.status(HttpStatus.OK).send(result);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put('actionExecute')
  async actionExecute(
    @Body() value: CompleteNtiPromoExecutionDto,
    @Res({ passthrough: true }) response: Response,
  ): Promise<any> {
    try {
      if (value.listRow.length < 0) {
        return responseError(
          'mohon lengkapi data',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      const rowError = [];
      for (const row of value.listRow) {
        const tReqNtiDtlData: TReqNtiDtl = await this.tReqNtiDtlService.findOne(
          {
            ntiId: row.ntiId,
            // remainingQty: row.remainingQty,
            // remainingQty: Not(0),
            // materialId: parseInt(row.materialId) as number,
            // materialDescription: row.materialdescription,
            // ntiReason: row.ntiReason,
            // submittedQty:
          },
        );

        if (tReqNtiDtlData.ntiId && row.confirmQty > row.remainingQty) {
          row['message'] =
            `MID ${row.materialId} NTI Reason: ${row.ntiReason}, Scrap Reason: ${row.scrapReason} , Confirm Qty tidak dapat melebihi Submitted qty`;
          rowError.push(row);
        }
      }

      if (rowError.length > 0) {
        response.status(HttpStatus.UNPROCESSABLE_ENTITY).send({
          status: 'error',
          message:
            'Mohon periksa kembali, Confirm Qty, tidak dapat melebihi Remaining Qty',
          data: rowError,
        });
        return;
      }

      const result = await this.ntiPromoExecutionService.execute(value);

      if (result?.status == 'error') {
        response.status(HttpStatus.UNPROCESSABLE_ENTITY).send(result);
        return;
      }

      response.status(HttpStatus.OK).send(result);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
