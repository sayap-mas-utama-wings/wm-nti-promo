import { Module } from '@nestjs/common';
import { TReqNtiHdrModule } from '../treqntihdr/treqntihdr.module';
import { TReqntiDtlModule } from '../treqntidtl/treqntidtl.module';
import { TLogNtiModule } from '../tlognti/tlognti.module';
import { NtiPromoExecutionService } from './ntipromoexecution.service';
import { NtiPromoExecutionController } from './ntipromoexecution.controller';
import { DynamicSapModule } from 'src/providers/sap/dynamic-sap.module';
import { EmailModule } from '../email/email.module';
import { MNtiUserModule } from '../mntiuser/mntiuser.module';
import { MRfcSalesofficeModule } from '../general/mrfcsalesoffice/mrfcsalesoffice.module';
import { MPltGsberSlsoffModule } from '../general/mpltgsberslsoff/mpltgsberslsoff.module';

@Module({
  imports: [
    TReqNtiHdrModule,
    TReqntiDtlModule,
    TLogNtiModule,
    DynamicSapModule,
    EmailModule,
    MNtiUserModule,
    MRfcSalesofficeModule,
    MPltGsberSlsoffModule,
  ],
  controllers: [NtiPromoExecutionController],
  providers: [NtiPromoExecutionService],
})
export class NtiPromoExecutionModule {}
