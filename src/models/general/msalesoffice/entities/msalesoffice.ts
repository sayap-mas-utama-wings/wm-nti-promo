import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import { MPlantSalesOffice } from '../../mplantsalesoffice/entities/mplantsalesoffice';

@Entity('m_salesoffice')
export class MSalesOffice {
  @PrimaryColumn({
    name: 'vkbur',
  })
  vkbur: string;

  @Column({
    name: 'descr',
  })
  descr: string;

  @OneToOne(
    () => MPlantSalesOffice,
    (mPlantSalesOffice) => mPlantSalesOffice.vkbur,
  )
  @JoinColumn([
    {
      name: 'vkbur',
      referencedColumnName: 'vkbur',
    },
  ])
  mPlantSalesOffice: MPlantSalesOffice;
}
