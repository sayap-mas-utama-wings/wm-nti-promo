import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_sloc')
export class MSloc {
  @PrimaryColumn({
    name: 'werks_lgort',
  })
  werksLgort: string;

  @Column({
    name: 'description',
    length: 100,
  })
  description: string;
}
