import { Injectable, NotFoundException } from '@nestjs/common';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { MSloc } from './entities/msloc';
import { DataSource, FindOptionsWhere } from 'typeorm';

@Injectable()
export class MSlocService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findOne(whereCondition: FindOptionsWhere<MSloc>): Promise<MSloc> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MSloc);

      const data: MSloc = await mPlantTransplanRepo.findOne({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
