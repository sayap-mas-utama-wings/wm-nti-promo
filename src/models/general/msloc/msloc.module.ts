import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MSlocService } from './msloc.service';

@Module({
  imports: [DynamicDbModule],
  providers: [MSlocService],
  exports: [MSlocService],
})
export class MSlocModule {}
