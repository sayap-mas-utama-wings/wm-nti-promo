import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import { MPh3Cc } from '../../mph3cc/entities/mph3cc.entity';

@Entity('m_material')
export class MMaterial {
  @PrimaryColumn({
    name: 'matnr',
    type: 'varchar',
  })
  matnr: string;

  @PrimaryColumn({
    name: 'vkorg',
    type: 'varchar',
  })
  vkorg: string;

  @PrimaryColumn({
    name: 'vtweg',
    type: 'varchar',
  })
  vtweg: string;

  @Column({
    name: 'spart',
    length: 2,
    type: 'varchar',
  })
  spart: string;

  @Column({
    name: 'maktx',
    length: 40,
    type: 'varchar',
  })
  maktx: string;

  @Column({
    name: 'bismt',
    length: 18,
    type: 'varchar',
  })
  bismt: string;

  @Column({
    name: 'meins',
    length: 3,
    type: 'varchar',
  })
  meins: string;

  @Column({
    name: 'vrkme',
    length: 3,
    type: 'varchar',
  })
  vrkme: string;

  @Column({
    name: 'brgew',
    // length: 16.3,
    type: 'decimal',
  })
  brgew: string;

  @Column({
    name: 'gewei',
    length: 3,
    type: 'varchar',
  })
  gewei: string;

  @Column({
    name: 'volum',
    // length: 16.3,
    type: 'decimal',
  })
  volum: string;

  @Column({
    name: 'voleh',
    length: 3,
    type: 'varchar',
  })
  voleh: string;

  @Column({
    name: 'bonus',
    length: 2,
    type: 'varchar',
  })
  bonus: string;

  @Column({
    name: 'mvgr1',
    length: 3,
    type: 'varchar',
  })
  mvgr1: string;

  @Column({
    name: 'mvgr2',
    length: 3,
    type: 'varchar',
  })
  mvgr2: string;

  @Column({
    name: 'mvgr3',
    length: 3,
    type: 'varchar',
  })
  mvgr3: string;

  @Column({
    name: 'mvgr4',
    length: 3,
    type: 'varchar',
  })
  mvgr4: string;

  @Column({
    name: 'mvgr5',
    length: 3,
    type: 'varchar',
  })
  mvgr5: string;

  @Column({
    name: 'prodh',
    length: 10,
    type: 'varchar',
  })
  prodh: string;

  @Column({
    name: 'prodhier',
    length: 18,
    type: 'varchar',
  })
  prodhier: string;

  @Column({
    name: 'ean11',
    length: 20,
    type: 'varchar',
  })
  ean11: string;

  @Column({
    name: 'ean11_box',
    length: 20,
    type: 'varchar',
  })
  ean11Box: string;

  @OneToOne(() => MPh3Cc, (mPh3Cc) => mPh3Cc.mMaterial)
  @JoinColumn({ name: 'prodhier', referencedColumnName: 'prodh' })
  mPh3Cc: MPh3Cc;
}
