import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MMaterialService } from './mmaterial.service';

@Module({
  imports: [DynamicDbModule],
  controllers: [],
  providers: [MMaterialService],
  exports: [MMaterialService],
})
export class MMaterialModule {}
