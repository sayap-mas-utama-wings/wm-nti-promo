import { Injectable, NotFoundException } from '@nestjs/common';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { DataSource, FindOptionsRelations, FindOptionsWhere } from 'typeorm';
import { MMaterial } from './entities/mmaterial.entity';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';

@Injectable()
export class MMaterialService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findOne(
    whereCondition: FindOptionsWhere<MMaterial>,
    FindOptionsRelations?: FindOptionsRelations<MMaterial>,
  ): Promise<MMaterial> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mMaterialRepo = ds.getRepository(MMaterial);
      const data: MMaterial = await mMaterialRepo.findOne({
        relations: FindOptionsRelations,
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['matnr', 'vkorg', 'prodhier'],
          number: ['matnr', 'vkorg'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = [
          'mMaterial.matnr',
          // 'mMaterial.vkorg',
          // 'mMaterial.prodhier',
          'mMaterial.maktx',
        ];

        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mMaterialRepo = ds.getRepository(MMaterial);
      // const data: MMaterial[] = await mMaterialRepo.find({
      //   where: whereCondition,
      // });

      const data = await mMaterialRepo
        .createQueryBuilder('mMaterial')
        // .innerJoinAndSelect('mMaterial.mPh3Cc', 'mPh3Cc')
        .select([
          'mMaterial.matnr',
          'mMaterial.vkorg',
          'mMaterial.prodhier',
          'mMaterial.vtweg',
          'mMaterial.maktx',
        ])
        // .select('DISTINCT mPh3Cc.matnr', 'matnr')
        // .addSelect([
        // 'mMaterial.vkorg',
        // 'mMaterial.prodhier',
        // 'mMaterial.vtweg',
        // 'mMaterial.maktx',
        // ])
        // .distinctOn(['mMaterial.matnr'])
        .where(whereClause)
        .orderBy(orderClause)
        .skip((pagination.page - 1) * pagination.limit)
        .take(pagination.limit)
        .getManyAndCount();
      // if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
