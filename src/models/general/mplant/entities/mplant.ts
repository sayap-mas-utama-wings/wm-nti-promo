import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_plant')
export class MPlant {
  @PrimaryColumn({
    name: 'werks',
    type: 'char',
  })
  werks: string;

  @Column({
    name: 'name1',
    type: 'char',
    length: 30,
  })
  name1: string;
}
