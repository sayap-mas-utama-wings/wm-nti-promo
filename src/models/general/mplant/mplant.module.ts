import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MPlantService } from './mplant.service';

@Module({
  imports: [DynamicDbModule],
  controllers: [],
  providers: [MPlantService],
  exports: [MPlantService],
})
export class MPlantModule {}
