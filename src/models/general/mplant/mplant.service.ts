import { Injectable, NotFoundException } from '@nestjs/common';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { DataSource, FindOptionsWhere } from 'typeorm';
import { MPlant } from './entities/mplant';

@Injectable()
export class MPlantService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findList(whereCondition: FindOptionsWhere<MPlant>): Promise<MPlant[]> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MPlant);

      const data: MPlant[] = await mPlantTransplanRepo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findOne(whereCondition: FindOptionsWhere<MPlant>): Promise<MPlant> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MPlant);

      const data: MPlant = await mPlantTransplanRepo.findOne({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
