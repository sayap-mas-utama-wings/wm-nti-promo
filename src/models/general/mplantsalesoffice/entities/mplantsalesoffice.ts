import { Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import { MSalesOffice } from '../../msalesoffice/entities/msalesoffice';

@Entity('m_plant_salesoffice')
export class MPlantSalesOffice {
  @PrimaryColumn({
    name: 'vkbur',
  })
  vkbur: string;

  @PrimaryColumn({
    name: 'werks',
  })
  werks: string;

  @PrimaryColumn({
    name: 'lgort',
  })
  lgort: string;

  @OneToOne(() => MSalesOffice, (mSalesOffice) => mSalesOffice.vkbur)
  @JoinColumn([
    {
      name: 'vkbur',
      referencedColumnName: 'vkbur',
    },
  ])
  mSalesOffice: MSalesOffice;
}
