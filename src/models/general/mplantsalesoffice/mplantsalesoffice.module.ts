import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MPlantSalesOfficeService } from './mplantsalesoffice.service';
import { MPlantSalesOfficeController } from './mplantsalesoffice.controller';
import { MSlocModule } from '../msloc/msloc.module';

@Module({
  imports: [DynamicDbModule, MSlocModule],
  controllers: [MPlantSalesOfficeController],
  providers: [MPlantSalesOfficeService],
  exports: [MPlantSalesOfficeService],
})
export class MPlantSalesOfficeModule {}
