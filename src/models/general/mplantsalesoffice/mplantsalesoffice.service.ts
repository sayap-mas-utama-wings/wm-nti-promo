import { Injectable, NotFoundException } from '@nestjs/common';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import {
  DataSource,
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
} from 'typeorm';
import { MPlantSalesOffice } from './entities/mplantsalesoffice';

@Injectable()
export class MPlantSalesOfficeService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findOne(
    whereCondition: FindOptionsWhere<MPlantSalesOffice>,
    orderCondition?: FindOptionsOrder<MPlantSalesOffice>,
    FindOptionsRelations?: FindOptionsRelations<MPlantSalesOffice>,
  ): Promise<MPlantSalesOffice> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPh3CcRepo = ds.getRepository(MPlantSalesOffice);

      const data = await mPh3CcRepo.findOne({
        where: whereCondition,
        order: orderCondition,
        relations: FindOptionsRelations,
      });

      if (!data) throw new NotFoundException('Data t_flow not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['vkbur', 'werks', 'lgort'],
          //   number: ['vkbur', 'werks'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = ['vkbur', 'werks', 'lgort'];

        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantSalesOfficeRepo = ds.getRepository(MPlantSalesOffice);

      const data = await mPlantSalesOfficeRepo
        .createQueryBuilder('mPlantSalesOffice')
        // .select(['lgort'])
        .where(whereClause)
        .orderBy(orderClause)
        .skip((pagination.page - 1) * pagination.limit)
        .take(pagination.limit)
        .getManyAndCount();
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
