import { Controller, Get, HttpStatus, Req } from '@nestjs/common';
import { MPlantSalesOfficeService } from './mplantsalesoffice.service';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { MSlocService } from '../msloc/msloc.service';

@Controller('mplantsalesoffice')
export class MPlantSalesOfficeController {
  constructor(
    private readonly mPlantSalesOfficeService: MPlantSalesOfficeService,
    private readonly mSlocService: MSlocService,
  ) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] =
        await this.mPlantSalesOfficeService.findAndPaginate(
          pagination,
          req.query,
        );

      if (results && results.length > 0) {
        for (const r of results) {
          if (r.lgort && r.werks) {
            const mSlocData = await this.mSlocService
              .findOne({
                werksLgort: `${r.werks};${r.lgort}`,
              })
              .catch(() => null);
            r['plantDescription'] = mSlocData
              ? mSlocData?.description.split(';')[0]
              : '';
            r['slocDescription'] = mSlocData
              ? mSlocData?.description.split(';')[1]
              : '';
          }
        }
      }

      console.log(
        '🚀 ~ MPlantSalesOfficeController ~ findAndPaginate ~ results:',
        results,
      );

      // const uniqueArray = results.filter(
      //   (obj, index, self) =>
      //     index === self.findIndex((o) => o.lgort === obj.lgort),
      // );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
