import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_plant_transplan')
export class MPlantTransplan {
  @PrimaryColumn({
    name: 'werks_tplst',
    type: 'char',
    length: 10,
    default: '',
    nullable: false,
  })
  werksTplst: string;

  @PrimaryColumn({
    name: 'descr',
    type: 'char',
    length: 100,
    default: '',
    nullable: false,
  })
  descr: string;

  @Column({
    name: 'bukrs',
    type: 'char',
    length: 4,
    default: '',
    nullable: false,
  })
  bukrs: string;
}
