import { Injectable, NotFoundException } from '@nestjs/common';
import { FindOptionsWhere, DataSource } from 'typeorm';
import { MPlantTransplan } from './entities/mplanttransplan.entity';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';

@Injectable()
export class MPlantTransplanService {
  constructor(
    // @Inject('REPO')
    // private mPlantTransplanRepo: Repository<MPlantTransplan>,
    private dynamicDbService: DynamicDbService,
  ) {}

  async findList(
    whereCondition: FindOptionsWhere<MPlantTransplan>,
  ): Promise<MPlantTransplan[]> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MPlantTransplan);
      // const data: MPlantTransplan[] = await this.mPlantTransplanRepo.find({
      const data: MPlantTransplan[] = await mPlantTransplanRepo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  // async findOne(plant: string, transplan: string, cardCode: string): Promise<BaseMCardDto> {
  async findOne(
    whereCondition: FindOptionsWhere<MPlantTransplan>,
  ): Promise<MPlantTransplan> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MPlantTransplan);
      // const data: MPlantTransplan = await this.mPlantTransplanRepo.findOne({
      const data: MPlantTransplan = await mPlantTransplanRepo.findOne({
        // where: {plant, transplan, cardCode}
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
