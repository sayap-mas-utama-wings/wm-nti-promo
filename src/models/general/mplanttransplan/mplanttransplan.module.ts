import { Module } from '@nestjs/common';
import { MPlantTransplanService } from './mplanttransplan.service';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
// import { MPlantTransplan } from './entities/mplanttransplan.entity';
// import { DataSource } from 'typeorm';
// import { DynamicDbService } from 'src/providers/database/dynamic-db.service';

@Module({
  imports: [DynamicDbModule],
  providers: [
    MPlantTransplanService,
    // {
    //   provide: 'REPO',
    //   useFactory: async (dynamicDbService: DynamicDbService) => {
    //     const ds: DataSource =
    //       await dynamicDbService.createDynamicConnectionGeneral();
    //     await ds.initialize();
    //     return ds.getRepository(MPlantTransplan);
    //   },
    //   inject: [DynamicDbService],
    // },
  ],
  exports: [MPlantTransplanService],
})
export class MPlantTransplanModule {}
