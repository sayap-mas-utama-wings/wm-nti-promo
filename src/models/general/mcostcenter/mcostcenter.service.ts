import { Injectable, NotFoundException } from '@nestjs/common';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { MCostCenter } from './entities/mcostcenter';
import { DataSource, FindOptionsOrder, FindOptionsWhere } from 'typeorm';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';

@Injectable()
export class MCostCenterService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findOne(
    whereCondition: FindOptionsWhere<MCostCenter>,
    orderCondition?: FindOptionsOrder<MCostCenter>,
  ): Promise<MCostCenter> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mCostCenterRepo = ds.getRepository(MCostCenter);

      const data = await mCostCenterRepo.findOne({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data m_cost_center not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    whereCondition: FindOptionsWhere<MCostCenter>,
    orderCondition?: FindOptionsOrder<MCostCenter>,
  ): Promise<MCostCenter[]> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPh3CcRepo = ds.getRepository(MCostCenter);

      const data = await mPh3CcRepo.find({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data m_cost_center not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAndPagination(
    pagination: PaginationOptions,
    search?: any,
    select?: string[],
    orderColumns?: string[],
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      console.log('masok', pagination, search);
      if (search) {
        const objSearchAttr = {
          string: ['kokrs', 'kostl', 'bukrs', 'ltext', 'mctxt'],
          number: [],
          date: [],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = orderColumns ?? select;
        const columnIndex = search?.columnIndex ? search.columnIndex : 0;
        const sortOrder = search?.sortOrder ? search?.sortOrder : 'ASC';

        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPh3CcRepo = ds.getRepository(MCostCenter);

      const result = await mPh3CcRepo
        .createQueryBuilder('mPh3CcRepo')
        .select(select)
        .where(whereClause)
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .getManyAndCount();
      console.log('🚀 ~ MCostCenterService ~ result:', result);

      return result;
    } catch (error) {
      throw error;
    }
  }
}
