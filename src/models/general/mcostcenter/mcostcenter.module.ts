import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MCostCenterService } from './mcostcenter.service';

@Module({
  imports: [DynamicDbModule],
  controllers: [],
  providers: [MCostCenterService],
  exports: [MCostCenterService],
})
export class MCostCenterModule {}
