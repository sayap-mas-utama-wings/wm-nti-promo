import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import { MPh3Cc } from '../../mph3cc/entities/mph3cc.entity';

@Entity('m_cost_center')
export class MCostCenter {
  @PrimaryColumn({
    name: 'kokrs',
    type: 'varchar',
  })
  kokrs: string;

  @PrimaryColumn({
    name: 'kostl',
    type: 'varchar',
  })
  kostl: string;

  @Column({
    name: 'bukrs',
    type: 'varchar',
    length: 4,
  })
  bukrs: string;

  @Column({
    name: 'ltext',
    type: 'varchar',
    length: 40,
  })
  ltext: string;

  @Column({
    name: 'mctxt',
    type: 'varchar',
    length: 20,
  })
  mctxt: string;

  @OneToOne(() => MPh3Cc, (mPh3Cc) => mPh3Cc.mCostCenter)
  @JoinColumn({ name: 'kostl', referencedColumnName: 'kostl' })
  mPh3Cc: MPh3Cc;
}
