import { Injectable, NotFoundException } from '@nestjs/common';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { MPltGsberSlsoff } from './entities/mpltgsberslsoff';
import { DataSource, FindOptionsWhere } from 'typeorm';

@Injectable()
export class MPltGsberSlsoffService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findOne(
    whereCondition: FindOptionsWhere<MPltGsberSlsoff>,
  ): Promise<MPltGsberSlsoff> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPltGsberSlsoffRepo = ds.getRepository(MPltGsberSlsoff);
      const data: MPltGsberSlsoff = await mPltGsberSlsoffRepo.findOne({
        where: whereCondition,
      });

      if (!data) throw new NotFoundException('Data m_plt_gsber_slsoff not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
