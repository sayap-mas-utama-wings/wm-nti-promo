import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_plt_gsber_slsoff')
export class MPltGsberSlsoff {
  @PrimaryColumn({
    name: 'werks',
    type: 'varchar',
  })
  werks: string;

  @PrimaryColumn({
    name: 'gsber',
    type: 'varchar',
  })
  gsber: string;

  @Column({
    name: 'vkbur',
    type: 'varchar',
  })
  vkbur: string;
}
