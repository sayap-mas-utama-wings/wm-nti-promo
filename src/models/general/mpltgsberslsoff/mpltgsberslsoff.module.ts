import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MPltGsberSlsoffService } from './mpltgsberslsoff.service';

@Module({
  imports: [DynamicDbModule],
  providers: [MPltGsberSlsoffService],
  exports: [MPltGsberSlsoffService],
})
export class MPltGsberSlsoffModule {}
