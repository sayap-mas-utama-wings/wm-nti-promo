import { Module } from '@nestjs/common';
import { MFtpS3Service } from './mftps3.service';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
// import { MFtpS3 } from './entities/mftps3.entity';
// import { DataSource } from 'typeorm';
// import { DynamicDbService } from 'src/providers/database/dynamic-db.service';

@Module({
  imports: [DynamicDbModule],
  providers: [
    MFtpS3Service,
    // {
    //   provide: 'REPO',
    //   useFactory: async (dynamicDbService: DynamicDbService) => {
    //     const ds: DataSource =
    //       await dynamicDbService.createDynamicConnectionGeneral();
    //     await ds.initialize();
    //     return ds.getRepository(MFtpS3);
    //   },
    //   inject: [DynamicDbService],
    // },
  ],
  exports: [MFtpS3Service],
})
export class MFtpS3Module {}
