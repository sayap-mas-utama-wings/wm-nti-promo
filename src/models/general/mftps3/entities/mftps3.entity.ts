import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_ftp_s3')
export class MFtpS3 {
  @PrimaryColumn({
    name: 'app_id',
    type: 'varchar',
    length: 25,
    default: '',
    nullable: false,
  })
  appId: string;

  @Column({
    name: 'bucket',
    type: 'varchar',
    length: 80,
    default: '',
    nullable: false,
  })
  bucket: string;

  @Column({
    name: 'access_key_id',
    type: 'varchar',
    length: 20,
    default: '',
    nullable: false,
  })
  accessKeyId: string;

  @Column({
    name: 'secret_access_key',
    type: 'varchar',
    length: 40,
    default: '',
    nullable: false,
  })
  secretAccessKey: string;

  @Column({
    name: 'region',
    type: 'varchar',
    length: 50,
    default: '',
    nullable: false,
  })
  region: string;

  @Column({
    name: 'mainfolder',
    type: 'varchar',
    length: 45,
    default: '',
    nullable: false,
  })
  mainfolder: string;

  @Column({
    name: 'subfolder',
    type: 'varchar',
    length: 45,
    default: '',
    nullable: false,
  })
  subfolder: string;

  @Column({
    name: 'separator',
    type: 'varchar',
    length: 2,
    default: '',
    nullable: false,
  })
  separator: string;

  @Column({
    name: 'flag_encode',
    type: 'varchar',
    length: 1,
    default: '',
    nullable: false,
  })
  flagEncode: string;
}
