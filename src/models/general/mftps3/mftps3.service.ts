import { Injectable, NotFoundException } from '@nestjs/common';
import { FindOptionsWhere, DataSource } from 'typeorm';
import { MFtpS3 } from './entities/mftps3.entity';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';

@Injectable()
export class MFtpS3Service {
  constructor(
    // @Inject('REPO')
    // private mFtpS3Repo: Repository<MFtpS3>,
    private dynamicDbService: DynamicDbService,
  ) {}

  async findList(whereCondition: FindOptionsWhere<MFtpS3>): Promise<MFtpS3[]> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mFtpS3Repo = ds.getRepository(MFtpS3);
      // const data: MFtpS3[] = await this.mFtpS3Repo.find({
      const data: MFtpS3[] = await mFtpS3Repo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  // async findOne(plant: string, transplan: string, cardCode: string): Promise<BaseMCardDto> {
  async findOne(whereCondition: FindOptionsWhere<MFtpS3>): Promise<MFtpS3> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mFtpS3Repo = ds.getRepository(MFtpS3);
      // const data: MFtpS3 = await this.mFtpS3Repo.findOne({
      const data: MFtpS3 = await mFtpS3Repo.findOne({
        // where: {plant, transplan, cardCode}
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async findOneDecode(
    whereCondition: FindOptionsWhere<MFtpS3>,
  ): Promise<MFtpS3> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mFtpS3Repo = ds.getRepository(MFtpS3);
      // const data: MFtpS3 = await this.mFtpS3Repo.findOne({
      const data: MFtpS3 = await mFtpS3Repo.findOne({
        // where: {plant, transplan, cardCode}
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      if (data.flagEncode) {
        data.accessKeyId = Buffer.from(data.accessKeyId, 'base64').toString(
          'ascii',
        );

        data.secretAccessKey = Buffer.from(
          data.secretAccessKey,
          'base64',
        ).toString('ascii');
      }
      return data;
    } catch (error) {
      throw error;
    }
  }
}
