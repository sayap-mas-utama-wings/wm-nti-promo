import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MRfcSalesofficeService } from './mrfcsalesoffice.service';

@Module({
  imports: [DynamicDbModule],
  providers: [MRfcSalesofficeService],
  controllers: [],
  exports: [MRfcSalesofficeService],
})
export class MRfcSalesofficeModule {}
