import { Injectable, NotFoundException } from '@nestjs/common';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { MRfcSalesoffice } from './entities/mrfcsalesoffice';
import { DataSource, FindOptionsOrder, FindOptionsWhere } from 'typeorm';

@Injectable()
export class MRfcSalesofficeService {
  constructor(private dynamicDbService: DynamicDbService) {}

  async findOne(
    whereCondition: FindOptionsWhere<MRfcSalesoffice>,
    orderCondition?: FindOptionsOrder<MRfcSalesoffice>,
  ): Promise<MRfcSalesoffice> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MRfcSalesoffice);
      const data: MRfcSalesoffice = await mPlantTransplanRepo.findOne({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
