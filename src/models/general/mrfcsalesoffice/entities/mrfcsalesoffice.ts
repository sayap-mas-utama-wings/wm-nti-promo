import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_rfc_salesoffice')
export class MRfcSalesoffice {
  @PrimaryColumn({
    name: 'vkbur',
    type: 'varchar',
  })
  vkbur: string;

  @Column({
    name: 'module',
    type: 'varchar',
    length: 5,
  })
  module: string;

  @Column({
    name: 'rfcuser',
    type: 'varchar',
    length: 20,
  })
  rfcuser: string;

  @Column({
    name: 'password',
    type: 'varchar',
    length: 200,
  })
  password: string;
}
