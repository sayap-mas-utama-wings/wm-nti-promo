import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MPh3CcService } from './mph3cc.service';
import { MPh3CcController } from './mph3cc.controller';
import { DynamicSapModule } from 'src/providers/sap/dynamic-sap.module';

@Module({
  imports: [DynamicDbModule, DynamicSapModule],
  providers: [MPh3CcService],
  exports: [MPh3CcService],
  controllers: [MPh3CcController],
})
export class MPh3CCModule {}
