import { DynamicDbService } from 'src/providers/database/dynamic-db.service';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { CreateMPh3CcDto } from './dtos/create-mph3cc.dto';
import {
  DataSource,
  EntityManager,
  FindOptionsOrder,
  FindOptionsRelations,
  FindOptionsWhere,
  InsertResult,
} from 'typeorm';
import { MPh3Cc } from './entities/mph3cc.entity';
import { MConfigsap } from 'src/models/mconfigsap/entities/mconfigsap.entity';
import { SapRfcObject, SapService } from 'nestjs-sap-rfc';
import { Injectable, NotFoundException } from '@nestjs/common';
import { isBase64 } from 'class-validator';
import { DynamicSapService } from 'src/providers/sap/dynamic-sap.service';
import { RfcArray } from 'node-rfc';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';

interface SapMPh3CcObject extends SapRfcObject {
  readonly T_DATA?: RfcArray;
}

@Injectable()
export class MPh3CcService {
  constructor(
    private dynamicDbService: DynamicDbService,
    private dynamicSapService: DynamicSapService,
    // private readonly mConfigsapService: MConfigsapService,
  ) {}

  async job(): Promise<any> {
    try {
      const sapConnConfig: MConfigsap = new MConfigsap();
      sapConnConfig.host = process.env.SAP_HOST;
      sapConnConfig.client = process.env.SAP_CLIENT;
      sapConnConfig.username = process.env.SAP_USERNAME;
      sapConnConfig.password = process.env.SAP_PASSWORD;
      sapConnConfig.systemId = process.env.SAP_SYSTEM_ID;
      sapConnConfig.systemNumber = process.env.SAP_SYSTEM_NUMBER;
      sapConnConfig.lang = process.env.SAP_LANGUAGE;
      sapConnConfig.group = process.env.SAP_GROUP;
      sapConnConfig.msHost = process.env.SAP_MSHOST;

      const sapConn = await this.createDynamicSapConnection(sapConnConfig);

      const resultSap = await sapConn.execute<SapMPh3CcObject>(
        'ZFN_WEB_W_DL_PH3CC_CTGRY',
        {},
      );

      const resultSuccess = [];
      if (resultSap && resultSap?.T_DATA) {
        for (const element of resultSap?.T_DATA) {
          const cekExist = await this.findOne({
            prodh: element['PRODH'],
          });

          if (!cekExist) {
            resultSuccess.push(
              await this.create(
                {
                  prodh: element['PRODH'],
                  category: element['VTEXT'],
                  kostl: element['KOSTL'],
                },
                '',
              ),
            );
          }
        }

        if (resultSuccess.length <= 0) {
          resultSuccess.push({
            message:
              "belum ada data terbaru untuk job 'ZFN_WEB_W_DL_PH3CC_CTGRY'",
          });
        }
      } else {
        resultSuccess.push({
          message: "gagal mengeksekusi job 'ZFN_WEB_W_DL_PH3CC_CTGRY'",
        });
      }

      return resultSuccess;
    } catch (error) {
      throw error;
    }
  }

  async create(
    createDto: QueryDeepPartialEntity<CreateMPh3CcDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }

      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPh3CcRepo = ds.getRepository(MPh3Cc);

      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(MPh3Cc)
          .values(createDto)
          //   .returning(returning)
          .execute();
      } else {
        inserted = await mPh3CcRepo
          .createQueryBuilder()
          .insert()
          .values(createDto)
          //   .returning(returning)
          .execute();
      }
      return { message: 'Data saved', raw: inserted?.raw };
      return true;
    } catch (error) {
      throw error;
    }
  }

  async createDynamicSapConnection(config: MConfigsap): Promise<SapService> {
    try {
      if (isBase64(config.password)) {
        config.password = Buffer.from(config.password, 'base64').toString(
          'ascii',
        );
      }
      return this.dynamicSapService.createDynamicConnection({
        ashost: config.msHost ? '' : config.host,
        client: config.client,
        user: config.username,
        passwd: config.password,
        sysid: config.systemId,
        sysnr: config.systemNumber,
        lang: config.lang,
        group: config.group,
        mshost: config.msHost,
      });
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MPh3Cc>,
    orderCondition?: FindOptionsOrder<MPh3Cc>,
    FindOptionsRelations?: FindOptionsRelations<MPh3Cc>,
  ): Promise<MPh3Cc> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPh3CcRepo = ds.getRepository(MPh3Cc);

      const data = await mPh3CcRepo.findOne({
        where: whereCondition,
        order: orderCondition,
        relations: FindOptionsRelations,
      });

      if (!data) throw new NotFoundException('Data t_flow not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAndPagination(
    pagination: PaginationOptions,
    search?: any,
    select?: string[],
    orderColumns?: string[],
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['requestId', 'plant', 'plantDescription'],
          number: [],
          date: [],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = orderColumns ?? select;
        const columnIndex = search?.columnIndex ? search.columnIndex : 0;
        const sortOrder = search?.sortOrder ? search?.sortOrder : 'ASC';

        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPh3CcRepo = ds.getRepository(MPh3Cc);

      const result = await mPh3CcRepo
        .createQueryBuilder('mPh3CcRepo')
        .distinct(true)
        .select(select)
        .where(whereClause)
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .getManyAndCount();

      return result;
    } catch (error) {
      throw error;
    }
  }
}
