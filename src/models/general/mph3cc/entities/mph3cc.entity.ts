import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import { MMaterial } from '../../mmaterial/entities/mmaterial.entity';
import { MCostCenter } from '../../mcostcenter/entities/mcostcenter';

@Entity('m_ph3_cc')
export class MPh3Cc {
  @PrimaryColumn({
    name: 'prodh',
    type: 'varchar',
    length: 18,
  })
  prodh: string;

  @Column({
    name: 'category',
    type: 'varchar',
    length: 40,
  })
  category: string;

  @Column({
    name: 'kostl',
    type: 'varchar',
    length: 10,
  })
  kostl: string;

  @OneToOne(() => MMaterial, (mMaterial) => mMaterial.mPh3Cc)
  @JoinColumn({ name: 'prodh', referencedColumnName: 'prodhier' })
  mMaterial: MMaterial;

  @OneToOne(() => MCostCenter, (mCostCenter) => mCostCenter.mPh3Cc)
  @JoinColumn({ name: 'kostl', referencedColumnName: 'kostl' })
  mCostCenter: MCostCenter;
}
