import { IsString } from 'class-validator';

export class BaseMph3CcDto {
  @IsString()
  prodh: string;

  @IsString()
  category: string;

  @IsString()
  kostl: string;
}
