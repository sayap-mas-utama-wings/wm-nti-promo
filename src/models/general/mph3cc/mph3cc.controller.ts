import { Controller, Get, HttpStatus, Req } from '@nestjs/common';
import { MPh3CcService } from './mph3cc.service';
import { responseError, responsePage } from 'src/common/helpers/response.helper';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';

@Controller('mph3cc')
export class MPh3CcController {
  constructor(private readonly mPh3CcService: MPh3CcService) {}

  @Get()
  async findAndPaginate(@Req() req): Promise<any> {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mPh3CcService.findAndPagination(
        pagination,
        req.query,
        ['mPh3CcRepo.prodh', 'mPh3CcRepo.category', 'mPh3CcRepo.kostl'],
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('job')
  async job() {
    try {
      return await this.mPh3CcService.job();
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
