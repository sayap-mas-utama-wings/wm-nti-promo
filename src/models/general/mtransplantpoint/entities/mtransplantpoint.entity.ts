import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_transplant_point')
export class MTransplantPoint {
  @PrimaryColumn({
    name: 'tplst',
    type: 'varchar',
    length: 4,
    default: null,
    nullable: false,
  })
  tplst: string;

  @Column({
    name: 'bezei',
    type: 'varchar',
    length: 35,
    default: null,
    nullable: true,
  })
  bezei: string;
}
