import { Module } from '@nestjs/common';
import { DynamicDbModule } from 'src/providers/database/dynamic-db.module';
import { MTransplantPointService } from './mtransplantpoint.service';
// import { MPlantTransplan } from './entities/mplanttransplan.entity';
// import { DataSource } from 'typeorm';
// import { DynamicDbService } from 'src/providers/database/dynamic-db.service';

@Module({
  imports: [DynamicDbModule],
  providers: [
    MTransplantPointService,
    // {
    //   provide: 'REPO',
    //   useFactory: async (dynamicDbService: DynamicDbService) => {
    //     const ds: DataSource =
    //       await dynamicDbService.createDynamicConnectionGeneral();
    //     await ds.initialize();
    //     return ds.getRepository(MPlantTransplan);
    //   },
    //   inject: [DynamicDbService],
    // },
  ],
  exports: [MTransplantPointService],
})
export class MTransplantPointModule {}
