import { Injectable, NotFoundException } from '@nestjs/common';
import { FindOptionsWhere, DataSource } from 'typeorm';
import { MTransplantPoint } from './entities/mtransplantpoint.entity';
import { DynamicDbService } from 'src/providers/database/dynamic-db.service';

@Injectable()
export class MTransplantPointService {
  constructor(
    // @Inject('REPO')
    // private mPlantTransplanRepo: Repository<MTransplantPoint>,
    private dynamicDbService: DynamicDbService,
  ) {}

  async findList(
    whereCondition: FindOptionsWhere<MTransplantPoint>,
  ): Promise<MTransplantPoint[]> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MTransplantPoint);
      // const data: MTransplantPoint[] = await this.mPlantTransplanRepo.find({
      const data: MTransplantPoint[] = await mPlantTransplanRepo.find({
        where: whereCondition,
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  // async findOne(plant: string, transplan: string, cardCode: string): Promise<BaseMCardDto> {
  async findOne(
    whereCondition: FindOptionsWhere<MTransplantPoint>,
  ): Promise<MTransplantPoint> {
    try {
      const ds: DataSource =
        await this.dynamicDbService.createDynamicConnectionGeneral();
      const mPlantTransplanRepo = ds.getRepository(MTransplantPoint);
      // const data: MTransplantPoint = await this.mPlantTransplanRepo.findOne({
      const data: MTransplantPoint = await mPlantTransplanRepo.findOne({
        // where: {plant, transplan, cardCode}
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }
}
