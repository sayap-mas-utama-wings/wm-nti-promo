import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Req,
  HttpStatus,
  UseInterceptors,
} from '@nestjs/common';
import { TransformInterceptor } from 'src/common/interceptors/transform-interceptor';
import { MMaterialService } from './mmaterial.service';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { CreateMMaterialDto } from './dtos/create-mmaterial.dto';
import { UpdateMMaterialDto } from './dtos/update-mmaterial.dto';

@Controller('mmaterial')
@UseInterceptors(TransformInterceptor)
export class MMaterialController {
  constructor(private readonly mMaterialService: MMaterialService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mMaterialService.findAndPaginate(
        pagination,
        req.query,
      );
      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post()
  async create(@Body() createValue: CreateMMaterialDto) {
    try {
      return await this.mMaterialService.create(createValue);
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put()
  async update(@Body() updateValue: UpdateMMaterialDto) {
    try {
      return await this.mMaterialService.update(updateValue);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete(':plant/:transplan/:materialId')
  async delete(
    @Param('plant') plant: string,
    @Param('transplan') transplan: string,
    @Param('materialId') materialId: string,
  ) {
    try {
      return await this.mMaterialService.delete(plant, transplan, materialId);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
