import { Module } from '@nestjs/common';
import { MMaterialService } from './mmaterial.service';
import { MMaterialController } from './mmaterial.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MMaterial } from './entities/mmaterial';

@Module({
  imports: [TypeOrmModule.forFeature([MMaterial])],
  controllers: [MMaterialController],
  providers: [MMaterialService],
  exports: [MMaterialService],
})
export class MMaterialModule {}
