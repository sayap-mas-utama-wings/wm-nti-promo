import { PartialType } from '@nestjs/mapped-types';
import { BaseMMaterialDto } from './base-mmaterial.dto';
import { IsString } from 'class-validator';

export class UpdateMMaterialDto extends PartialType(BaseMMaterialDto) {
  changedOn: string;

  @IsString()
  changedBy: string;
}
