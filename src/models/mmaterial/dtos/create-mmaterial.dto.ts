import { IsString } from 'class-validator';
import { BaseMMaterialDto } from './base-mmaterial.dto';

export class CreateMMaterialDto extends BaseMMaterialDto {
  createdOn: string;

  @IsString()
  createdBy: string;
}
