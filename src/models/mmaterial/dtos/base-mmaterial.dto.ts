import { IsString } from "class-validator";

export class BaseMMaterialDto {
  @IsString()
  plant: string;

  @IsString()
  transplan: string;

  @IsString()
  materialId: string;
}
