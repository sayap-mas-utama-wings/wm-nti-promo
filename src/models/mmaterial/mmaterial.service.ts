import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MMaterial } from './entities/mmaterial';
import { FindOptionsWhere, Repository } from 'typeorm';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { CreateMMaterialDto } from './dtos/create-mmaterial.dto';
import { UpdateMMaterialDto } from './dtos/update-mmaterial.dto';

@Injectable()
export class MMaterialService {
  constructor(
    @InjectRepository(MMaterial) private mMaterialRepo: Repository<MMaterial>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};
      if (search) {
        const objSearchAttr = {
          string: ['plant', 'transplan', 'materialId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);

        const columnNames = ['plant', 'transplan', 'materialId'];
        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.mMaterialRepo.findAndCount({
        where: whereClause,
        order: orderClause,
        take: pagination.limit,
        skip: (pagination.page - 1) * pagination.limit,
      });
      return result;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MMaterial>,
  ): Promise<MMaterial> {
    try {
      const data: MMaterial = await this.mMaterialRepo.findOne({
        where: whereCondition,
      });
      if (!data) throw new NotFoundException('Data not found!');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async create(createValueDto: CreateMMaterialDto): Promise<any> {
    try {
      const inserted = await this.mMaterialRepo
        .createQueryBuilder()
        .insert()
        .values(createValueDto)
        .returning('*')
        .execute();
      const returned = { message: 'Data saved', raw: inserted?.generatedMaps };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async update(updateValueDto: UpdateMMaterialDto): Promise<any> {
    try {
      const updated = await this.mMaterialRepo
        .createQueryBuilder()
        .update(MMaterial)
        .set({
          materialId: updateValueDto.materialId,
          changedBy: updateValueDto.changedBy,
        })
        .where('plant = :plant AND transplan = :transplan', {
          plant: updateValueDto.plant,
          transplan: updateValueDto.transplan,
        })
        .returning('*')
        .updateEntity(true)
        .execute();
      if (!updated?.affected) throw new NotFoundException('Update failed!');
      const returned = {
        message: 'Data updated',
        raw: updated?.generatedMaps,
        affected: updated?.affected,
      };
      return returned;
    } catch (error) {
      throw error;
    }
  }

  async delete(
    plant: string,
    transplan: string,
    materialId: string,
  ): Promise<any> {
    try {
      const deleted = await this.mMaterialRepo
        .createQueryBuilder()
        .delete()
        .from(MMaterial)
        .where(
          'plant = :plant AND transplan = :transplan AND material_id = :materialId',
          {
            plant,
            transplan,
            materialId,
          },
        )
        .execute();
      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      Object.assign(deleted, { message: 'Data deleted' });
      return deleted;
    } catch (error) {
      throw error;
    }
  }
}
