import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MPicmarketing } from './entities/mpicmarketing';
import {
  DeleteResult,
  EntityManager,
  FindOptionsOrder,
  FindOptionsWhere,
  InsertResult,
  Repository,
  UpdateResult,
} from 'typeorm';
import { generateWhereClauseByAttr } from 'src/common/helpers/global.helper';
import { PaginationOptions } from 'src/common/helpers/pagination.helper';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { CreateMPicMarketingDto } from './dtos/create-mpicmarketing.dto';
import { UpdateMPicMarketingDto } from './dtos/update-mpicmarketing.dto';

@Injectable()
export class MPicMarketingService {
  constructor(
    @InjectRepository(MPicmarketing)
    private mPicMarketingRepo: Repository<MPicmarketing>,
  ) {}

  async findAndPaginate(
    pagination: PaginationOptions,
    search?: any,
  ): Promise<any> {
    try {
      let whereClause: any;
      const orderClause = {};

      if (search) {
        const objSearchAttr = {
          string: ['settingName', 'item', 'value', 'description'],
          number: ['userId'],
        };

        whereClause = generateWhereClauseByAttr(search, objSearchAttr);
        Object.assign(whereClause);

        const columnNames = ['setting_name', 'item', 'value', 'description'];

        const columnIndex = search?.columnIndex ?? 0;
        const sortOrder = search?.sortOrder ?? 'ASC';
        Object.assign(orderClause, {
          [columnNames[columnIndex]]: sortOrder,
        });
      }

      const result = await this.mPicMarketingRepo
        .createQueryBuilder('m_pic_marketing')
        .where(whereClause)
        .orderBy(orderClause)
        .take(pagination.limit)
        .skip((pagination.page - 1) * pagination.limit)
        .getManyAndCount();

      return result;
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    whereCondition: FindOptionsWhere<MPicmarketing>,
    orderCondition?: FindOptionsOrder<MPicmarketing>,
  ) {
    try {
      const data = await this.mPicMarketingRepo.findOne({
        where: whereCondition,
        order: orderCondition,
      });

      if (!data) throw new NotFoundException('Data not found!');

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findAll(
    whereCondition: FindOptionsWhere<MPicmarketing>,
  ): Promise<MPicmarketing[]> {
    try {
      return await this.mPicMarketingRepo.find({ where: whereCondition });
    } catch (error) {
      throw error;
    }
  }

  async create(
    createDto: QueryDeepPartialEntity<CreateMPicMarketingDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      if (returning === null || returning === undefined) {
        returning = '*';
      }

      let inserted: InsertResult;
      if (transactionManager) {
        inserted = await transactionManager
          .createQueryBuilder()
          .insert()
          .into(MPicmarketing)
          .values(createDto)
          .returning(returning)
          .execute();
      } else {
        inserted = await this.mPicMarketingRepo
          .createQueryBuilder()
          .insert()
          .values(createDto)
          .returning(returning)
          .execute();
      }
      return { message: 'Data saved', raw: inserted?.generatedMaps };
    } catch (error) {
      throw error;
    }
  }

  async update(
    updateDto: QueryDeepPartialEntity<UpdateMPicMarketingDto>,
    returning?: string,
    transactionManager?: EntityManager,
  ): Promise<any> {
    let updated: UpdateResult;
    if (transactionManager) {
      updated = await transactionManager
        .createQueryBuilder()
        .update(MPicmarketing)
        .set({
          settingName: updateDto.settingName,
          item: updateDto.item,
          value: updateDto.value,
          description: updateDto.description,
          updatedBy: updateDto.updatedBy,
          updatedDate() {
            return 'now()';
          },
          updatedName: updateDto.updatedName,
        })
        .where('user_id = :userId', { userId: updateDto.userId })
        .returning(returning)
        .execute();
    } else {
      updated = await this.mPicMarketingRepo
        .createQueryBuilder()
        .update(MPicmarketing)
        .set({
          settingName: updateDto.settingName,
          item: updateDto.item,
          value: updateDto.value,
          description: updateDto.description,
          updatedBy: updateDto.updatedBy,
          updatedDate() {
            return 'now()';
          },
          updatedName: updateDto.updatedName,
        })
        .where('user_id = :userId', { userId: updateDto.userId })
        .returning(returning)
        .updateEntity(true)
        .execute();
    }

    if (!updated?.affected) throw new NotFoundException('Update failed!');

    return {
      message: 'Data updated',
      raw: updated?.generatedMaps,
      affected: updated?.affected,
    };
  }

  async delete(
    whereCondition: FindOptionsWhere<MPicmarketing>,
    transactionManager?: EntityManager,
  ): Promise<any> {
    try {
      let deleted: DeleteResult;
      if (transactionManager) {
        deleted = await transactionManager
          .createQueryBuilder()
          .delete()
          .from(MPicmarketing)
          .where(whereCondition)
          .execute();
      } else {
        deleted = await this.mPicMarketingRepo
          .createQueryBuilder()
          .delete()
          .from(MPicmarketing)
          .where(whereCondition)
          .execute();
      }

      if (!deleted?.affected) throw new NotFoundException('Delete failed!');
      return {
        message: 'Data deleted',
        raw: deleted?.raw,
        affected: deleted?.affected,
      };
    } catch (error) {
      throw error;
    }
  }
}
