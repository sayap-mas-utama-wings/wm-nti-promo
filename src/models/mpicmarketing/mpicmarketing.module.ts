import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MPicmarketing } from './entities/mpicmarketing';
import { MPicMarketingService } from './mpicmarketing.service';
import { MPicMarketingController } from './mpicmarketing.controller';

@Module({
  imports: [TypeOrmModule.forFeature([MPicmarketing])],
  controllers: [MPicMarketingController],
  providers: [MPicMarketingService],
  exports: [MPicMarketingService],
})
export class MPicMarketingModule {}
