import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('m_pic_marketing')
export class MPicmarketing {
  @PrimaryColumn({
    name: 'user_id',
    type: 'int4',
  })
  userId: string;

  @Column({
    name: 'setting_name',
    type: 'varchar',
    length: 50,
  })
  settingName: string;

  @Column({
    name: 'item',
    type: 'varchar',
    length: 10,
  })
  item: string;

  @Column({
    name: 'value',
    type: 'varchar',
    length: 15,
  })
  value: string;

  @Column({
    name: 'description',
    type: 'varchar',
    length: 50,
  })
  description: string;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 15,
  })
  createdBy: string;

  @Column({
    name: 'created_name',
    type: 'varchar',
    length: 50,
  })
  createdName: string;

  @Column({
    name: 'created_date',
    type: 'timestamp',
    default: null,
    nullable: true,
  })
  createdDate: string;

  @Column({
    name: 'updated_by',
    type: 'varchar',
    length: 15,
  })
  updatedBy: string;

  @Column({
    name: 'updated_name',
    type: 'varchar',
    length: 50,
  })
  updatedName: string;

  @Column({
    name: 'updated_date',
    type: 'timestamp',
    onUpdate: 'NOW()',
  })
  updatedDate: string;
}
