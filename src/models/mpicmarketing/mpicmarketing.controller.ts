import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { MPicMarketingService } from './mpicmarketing.service';
import { createPaginationOptions } from 'src/common/helpers/pagination.helper';
import {
  responseError,
  responsePage,
} from 'src/common/helpers/response.helper';
import { CreateMPicMarketingDto } from './dtos/create-mpicmarketing.dto';
import { UpdateMPicMarketingDto } from './dtos/update-mpicmarketing.dto';

@Controller('mpicmarketing')
export class MPicMarketingController {
  constructor(private readonly mPicMarketingService: MPicMarketingService) {}

  @Get()
  async findAndPaginate(@Req() req) {
    try {
      const pagination = createPaginationOptions(req);
      const [results, total] = await this.mPicMarketingService.findAndPaginate(
        pagination,
        req.query,
      );

      return responsePage(results, total, pagination);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Get('findOne/:userId')
  async findOne(@Param('userId') userId: string) {
    try {
      if (!userId) {
        return responseError(
          'userId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }
      return await this.mPicMarketingService.findOne({ userId: userId });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Post('createData')
  async create(@Body() createValue: CreateMPicMarketingDto) {
    try {
      if (!createValue.settingName) {
        return responseError(
          'setting name harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!createValue.item) {
        return responseError(
          'item harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!createValue.value) {
        return responseError(
          'value harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!createValue.description) {
        return responseError(
          'description harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.mPicMarketingService.create({
        ...createValue,
        createdDate() {
          return 'NOW()';
        },
      });
    } catch (error) {
      let message = error.message;
      if (error?.code == '23505') {
        message = 'Data already exists!';
      }
      return responseError(
        message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Put('updateData')
  async update(@Body() updateValue: UpdateMPicMarketingDto) {
    try {
      if (!updateValue.userId) {
        return responseError(
          'userId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.settingName) {
        return responseError(
          'setting name harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.item) {
        return responseError(
          'item harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.value) {
        return responseError(
          'value harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      } else if (!updateValue.description) {
        return responseError(
          'description harus diisi',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }

      return await this.mPicMarketingService.update(updateValue);
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  @Delete('deleteData/:userId')
  async delete(@Param('userId') userId: string) {
    try {
      if (!userId) {
        return responseError(
          'userId is required',
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      }
      return this.mPicMarketingService.delete({
        userId: userId,
      });
    } catch (error) {
      return responseError(
        error.message,
        error?.status ?? HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }
}
