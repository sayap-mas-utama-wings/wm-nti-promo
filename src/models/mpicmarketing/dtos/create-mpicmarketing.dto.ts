import { IsString } from 'class-validator';
import { BaseMPicMarketingDto } from './base-mpicmarketing.dto';

export class CreateMPicMarketingDto extends BaseMPicMarketingDto {
  @IsString()
  createdBy: string;

  @IsString()
  createdName: string;

  @IsString()
  createdDate: string;
}
