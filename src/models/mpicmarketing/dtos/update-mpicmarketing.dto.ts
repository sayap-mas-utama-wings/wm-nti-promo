import { IsString } from 'class-validator';
import { BaseMPicMarketingDto } from './base-mpicmarketing.dto';

export class UpdateMPicMarketingDto extends BaseMPicMarketingDto {
  @IsString()
  userId: string;

  @IsString()
  updatedBy: string;

  @IsString()
  updatedName: string;

  @IsString()
  updatedDate: string;
}
