import { IsString } from 'class-validator';

export class BaseMPicMarketingDto {
  @IsString()
  settingName: string;

  @IsString()
  item: string;

  @IsString()
  value: string;

  @IsString()
  description: string;
}
