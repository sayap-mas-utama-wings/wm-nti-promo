import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PgWmDatabaseProviderModule } from './providers/database/postgres/provider.module';
import { MConfigdatabaseModule } from './models/mconfigdatabase/mconfigdatabase.module';
import { MConfigsapModule } from './models/mconfigsap/mconfigsap.module';
import { XRayMiddleware } from './common/middlewares/xray.middleware';
import { MNtiUserModule } from './models/mntiuser/mntiuser.module';
import { MSettingModule } from './models/msetting/msetting.module';
import { MNtiReasonModule } from './models/mntireason/mntireason.module';
import { MPicMarketingModule } from './models/mpicmarketing/mpicmarketing.module';
import { MPh3CCModule } from './models/general/mph3cc/mph3cc.module';
import { SmuUserModule } from './models/joomlaportal/smuusers/smuusers.module';
import { TReqNtiHdrModule } from './models/treqntihdr/treqntihdr.module';
import { TReqntiDtlModule } from './models/treqntidtl/treqntidtl.module';
import { CreateNtiModule } from './models/createnti/createnti.module';
import { MMaterialModule } from './models/general/mmaterial/mmaterial.module';
import { MPlantSalesOfficeModule } from './models/general/mplantsalesoffice/mplantsalesoffice.module';
import { MCostCenterModule } from './models/general/mcostcenter/mcostcenter.module';
import { MPlantModule } from './models/general/mplant/mplant.module';
import { TLogNtiModule } from './models/tlognti/tlognti.module';
import { TReqNtiWorkflowModule } from './models/treqntiworkflow/treqntiworkflow.module';
import { TInboxV2Module } from './models/inbox/tinboxv2/tinboxv2.module';
import { ApprovalMarketingModule } from './models/approvalmarketing/approvalmarketing.module';
import { NtiPromoExecutionModule } from './models/ntipromoexecution/ntipromoexecution.module';
import { ReportOutStandingNtiModule } from './models/reportoutstandingnti/reportoutstandingnti.module';
import { ReportDetailsModule } from './models/reportdetails/reportdetails.module';
import { EmailModule } from './models/email/email.module';

@Module({
  imports: [
    PgWmDatabaseProviderModule,
    MConfigdatabaseModule,
    MConfigsapModule,
    MSettingModule,
    MNtiUserModule,
    MNtiReasonModule,
    MPicMarketingModule,
    MPh3CCModule,
    SmuUserModule,
    TReqNtiHdrModule,
    TReqntiDtlModule,
    CreateNtiModule,
    MMaterialModule,
    MPlantSalesOfficeModule,
    MCostCenterModule,
    MPlantModule,
    TLogNtiModule,
    TReqNtiWorkflowModule,
    TInboxV2Module,
    ApprovalMarketingModule,
    NtiPromoExecutionModule,
    ReportOutStandingNtiModule,
    ReportDetailsModule,
    EmailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(XRayMiddleware).forRoutes('*');
  }
}
