deploy-dev:
	@npm run start
	@npm run build
	@mv dist /var/www/html

deploy-prod:
	@npm run start
	@npm run build
	@cp -a dist/* /var/www/html/

dev:
	@yarn start:dev
